package com.alibaba.fastjson;

class JSONStreamContext {

    final static int START_OBJECT = 1001;
    final static int PROPERTY_KEY = 1002;
    final static int PROPERTY_VALUE = 1003;
    final static int START_ARRAY = 1004;
    final static int ARRAY_VALUE = 1005;

    protected final JSONStreamContext parent;

    protected int                     state;

    public JSONStreamContext(JSONStreamContext parent, int state){
        this.parent = parent;
        this.state = state;
    }
}
