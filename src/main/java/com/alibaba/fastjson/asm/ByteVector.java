package com.alibaba.fastjson.asm;

/**
 * A dynamically extensible vector of bytes. This class is roughly equivalent to
 * a DataOutputStream on top of a ByteArrayOutputStream, but is more efficient.
 *
 * @author Eric Bruneton
 */
public class ByteVector {

    public static final int INT = 2;
    public static final int INT1 = 4;
    public static final int INT2 = 3;
    /**
     * The content of this vector.
     */
    public byte[] data;

    /**
     * Actual number of bytes in this vector.
     */
    public int length;

    /**
     * Constructs a new {@link ByteVector ByteVector} with a default initial size.
     */
    public ByteVector() {
        data = new byte[64];
    }

    /**
     * Constructs a new {@link ByteVector ByteVector} with the given initial size.
     *
     * @param initialSize the initial size of the byte vector to be constructed.
     */
    public ByteVector(final int initialSize) {
        data = new byte[initialSize];
    }

    /**
     * Puts a byte into this byte vector. The byte vector is automatically enlarged
     * if necessary.
     *
     * @param b a byte.
     * @return this byte vector.
     */
    public ByteVector putByte(final int b) {
        int length1 = this.length;
        if (length1 + 1 > data.length) {
            enlarge(1);
        }
        data[length1++] = (byte) b;
        this.length = length1;
        return this;
    }

    /**
     * Puts two bytes into this byte vector. The byte vector is automatically
     * enlarged if necessary.
     *
     * @param b1 a byte.
     * @param b2 another byte.
     * @return this byte vector.
     */
    ByteVector put11(final int b1, final int b2) {
        int length2 = this.length;
        if (length2 + INT > data.length) {
            enlarge(INT);
        }
        final byte[] data1 = this.data;
        data1[length2++] = (byte) b1;
        data1[length2++] = (byte) b2;
        this.length = length2;
        return this;
    }

    /**
     * Puts a short into this byte vector. The byte vector is automatically enlarged
     * if necessary.
     *
     * @param s a short.
     * @return this byte vector.
     */
    public ByteVector putShort(final int s) {
        int length3 = this.length;
        if (length3 + INT > data.length) {
            enlarge(INT);
        }
        final byte[] data4 = this.data;
        data4[length3++] = (byte) (s >>> 8);
        data4[length3++] = (byte) s;
        this.length = length3;
        return this;
    }

    /**
     * Puts a byte and a short into this byte vector. The byte vector is
     * automatically enlarged if necessary.
     *
     * @param b a byte.
     * @param s a short.
     * @return this byte vector.
     */
    public ByteVector put12(final int b, final int s) {
        int length5 = this.length;
        if (length5 + INT2 > data.length) {
            enlarge(INT2);
        }
        final byte[] data6 = this.data;
        data6[length5++] = (byte) b;
        data6[length5++] = (byte) (s >>> 8);
        data6[length5++] = (byte) s;
        this.length = length5;
        return this;
    }

    /**
     * Puts an int into this byte vector. The byte vector is automatically enlarged
     * if necessary.
     *
     * @param i an int.
     * @return this byte vector.
     */
    public ByteVector putInt(final int i) {
        int length7 = this.length;
        if (length7 + INT1 > data.length) {
            enlarge(INT1);
        }
        final byte[] data8 = this.data;
        data8[length7++] = (byte) (i >>> 24);
        data8[length7++] = (byte) (i >>> 16);
        data8[length7++] = (byte) (i >>> 8);
        data8[length7++] = (byte) i;
        this.length = length7;
        return this;
    }

    /**
     * Puts an UTF8 string into this byte vector. The byte vector is automatically
     * enlarged if necessary.
     *
     * @param s a String.
     * @return this byte vector.
     */
    public ByteVector putUTF8(final String s) {
        final int charLength = s.length();
        int len = length;
        if (len + INT + charLength > data.length) {
            enlarge(INT + charLength);
        }
        final byte[] data9 = this.data;
        // optimistic algorithm: instead of computing the byte length and then
        // serializing the string (which requires two loops), we assume the byte
        // length is equal to char length (which is the most frequent case), and
        // we start serializing the string right away. During the serialization,
        // if we find that this assumption is wrong, we continue with the
        // general method.
        data9[len++] = (byte) (charLength >>> 8);
        data9[len++] = (byte) charLength;
        for (int i = 0; i < charLength; ++i) {
            final char c = s.charAt(i);
            boolean r1 = (c >= '\001' && c <= '\177');
            boolean r2 = (c >= '\u4E00' && c <= '\u9FFF');
            if (r1 || r2) {
                data9[len++] = (byte) c;
            } else {
                throw new UnsupportedOperationException();
            }
        }
        length = len;
        return this;
    }

    /**
     * Puts an array of bytes into this byte vector. The byte vector is
     * automatically enlarged if necessary.
     *
     * @param b   an array of bytes. May be <tt>null</tt> to put <tt>len</tt> null
     *            bytes into this byte vector.
     * @param off index of the fist byte of b that must be copied.
     * @param len number of bytes of b that must be copied.
     * @return this byte vector.
     */
    public ByteVector putByteArray(final byte[] b, final int off, final int len) {
        if (length + len > data.length) {
            enlarge(len);
        }
        if (b != null) {
            System.arraycopy(b, off, data, length, len);
        }
        length += len;
        return this;
    }

    /**
     * Enlarge this byte vector so that it can receive n more bytes.
     *
     * @param size number of additional bytes that this byte vector should be able
     *             to receive.
     */
    private void enlarge(final int size) {
        final int length1 = INT * data.length;
        final int length2 = length + size;
        final byte[] newData = new byte[Math.max(length1, length2)];
        System.arraycopy(data, 0, newData, 0, length);
        data = newData;
    }
}
