package com.alibaba.fastjson.asm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by wenshao on 05/08/2017.
 * @author xwm
 */
public class ClassReader {
    public static final int INT = 2;
    public static final int INT1 = 8;
    public final byte[] b;
    private final int[] items;
    private final String[] strings;
    private final int maxStringLength;
    public final int header;
    private boolean readAnnotations;

    public ClassReader(InputStream is, boolean readAnnotations) throws IOException {
        this.readAnnotations = readAnnotations;

        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            for (; ; ) {
                int len = is.read(buf);
                if (len == -1) {
                    break;
                }

                if (len > 0) {
                    out.write(buf, 0, len);
                }
            }
            is.close();
            this.b = out.toByteArray();
        }

        // parses the constant pool
        items = new int[readUnsignedShort(INT1)];
        int n = items.length;
        strings = new String[n];
        int max = 0;
        int index = 10;
        for (int i = 1; i < n; ++i) {
            items[i] = index + 1;
            int size;
            switch (b[index]) {
                // FIELD:
                // METH:
                // IMETH:
                // INT:
                // FLOAT:
                // INVOKEDYN:
                // NAME_TYPE:
                case 9:
                case 10:
                case 11:
                case 3:
                case 4:
                case 18:
                case 12:
                    size = 5;
                    break;
                // LONG:
                // DOUBLE:
                // MHANDLE:
                // UTF8:
                case 5:
                case 6:
                    size = 9;
                    ++i;
                    break;
                case 15:
                    size = 4;
                    break;
                case 1:
                    size = 3 + readUnsignedShort(index + 1);
                    if (size > max) {
                        max = size;
                    }
                    break;
                // case HamConstants.CLASS:
                // case HamConstants.STR:
                default:
                    size = 3;
                    break;
            }
            index += size;
        }
        maxStringLength = max;
        // the class header information starts just after the constant pool
        header = index;
    }

    public void accept(final TypeCollector classVisitor) {
        // buffer used to read strings
        char[] c = new char[maxStringLength];
        // loop variables
        int i, j;
        // indexes in b
        int u, v;
        int anns = 0;

        //read annotations
        if (readAnnotations) {
            u = getAttributes();
            for (i = readUnsignedShort(u); i > 0; --i) {
                String attrName = readUTF8(u + INT, c);
                if ("RuntimeVisibleAnnotations".equals(attrName)) {
                    anns = u + INT1;
                    break;
                }
                u += 6 + readInt(u + 4);
            }
        }

        // visits the header
        u = header;
        v = items[readUnsignedShort(u + 4)];
        int len = readUnsignedShort(u + 6);
        u += INT1;
        for (i = 0; i < len; ++i) {
            u += INT;
        }
        v = u;
        i = readUnsignedShort(v);
        v += INT;
        for (; i > 0; --i) {
            j = readUnsignedShort(v + 6);
            v += INT1;
            for (; j > 0; --j) {
                v += 6 + readInt(v + INT);
            }
        }
        i = readUnsignedShort(v);
        v += INT;
        for (; i > 0; --i) {
            j = readUnsignedShort(v + 6);
            v += INT1;
            for (; j > 0; --j) {
                v += 6 + readInt(v + INT);
            }
        }

        i = readUnsignedShort(v);
        v += INT;
        for (; i > 0; --i) {
            v += 6 + readInt(v + INT);
        }

        if (anns != 0) {
            for (i = readUnsignedShort(anns), v = anns + INT; i > 0; --i) {
                String name = readUTF8(v, c);
                classVisitor.visitAnnotation(name);
            }
        }

        // visits the fields
        i = readUnsignedShort(u);
        u += INT;
        for (; i > 0; --i) {
            j = readUnsignedShort(u + 6);
            u += INT1;
            for (; j > 0; --j) {
                u += 6 + readInt(u + INT);
            }
        }

        // visits the methods
        i = readUnsignedShort(u);
        u += INT;
        for (; i > 0; --i) {
            // inlined in original ASM source, now a method call
            u = readMethod(classVisitor, c, u);
        }
    }

    private int getAttributes() {
        // skips the header
        int u = header + INT1 + readUnsignedShort(header + 6) * INT;
        // skips fields and methods
        for (int i = readUnsignedShort(u); i > 0; --i) {
            for (int j = readUnsignedShort(u + INT1); j > 0; --j) {
                u += 6 + readInt(u + 12);
            }
            u += INT1;
        }
        u += INT;
        for (int i = readUnsignedShort(u); i > 0; --i) {
            for (int j = readUnsignedShort(u + INT1); j > 0; --j) {
                u += 6 + readInt(u + 12);
            }
            u += INT1;
        }
        // the attribute_info structure starts just after the methods
        return u + INT;
    }

    private int readMethod(TypeCollector classVisitor, char[] c, int u) {
        int v;
        int w;
        int j;
        String attrName;
        int k;
        int access = readUnsignedShort(u);
        String name = readUTF8(u + INT, c);
        String desc = readUTF8(u + 4, c);
        v = 0;
        w = 0;

        // looks for Code and Exceptions attributes
        j = readUnsignedShort(u + 6);
        u += INT1;
        for (; j > 0; --j) {
            attrName = readUTF8(u, c);
            int attrSize = readInt(u + INT);
            u += 6;
            // tests are sorted in decreasing frequency order
            // (based on frequencies observed on typical classes)
            if ("Code".equals(attrName)) {
                v = u;
            }
            u += attrSize;
        }
        // reads declared exceptions
        if (w == 0) {
        } else {
            w += INT;
            for (j = 0; j < readUnsignedShort(w); ++j) {
                w += INT;
            }
        }

        // visits the method's code, if any
        MethodCollector mv = classVisitor.visitMethod(access, name, desc);

        if (mv != null && v != 0) {
            int codeLength = readInt(v + 4);
            v += INT1;

            int codeStart = v;
            int codeEnd = v + codeLength;
            v = codeEnd;

            j = readUnsignedShort(v);
            v += INT;
            for (; j > 0; --j) {
                v += INT1;
            }
            // parses the local variable, line number tables, and code
            // attributes
            int varTable = 0;
            int varTypeTable = 0;
            j = readUnsignedShort(v);
            v += INT;
            for (; j > 0; --j) {
                attrName = readUTF8(v, c);
                if ("LocalVariableTable".equals(attrName)) {
                    varTable = v + 6;
                } else if ("LocalVariableTypeTable".equals(attrName)) {
                    varTypeTable = v + 6;
                }
                v += 6 + readInt(v + INT);
            }

            v = codeStart;
            // visits the local variable tables
            if (varTable != 0) {
                if (varTypeTable != 0) {
                    k = readUnsignedShort(varTypeTable) * 3;
                    w = varTypeTable + INT;
                    int[] typeTable = new int[k];
                    while (k > 0) {
                        // signature
                        typeTable[--k] = w + 6;
                        // index
                        typeTable[--k] = readUnsignedShort(w + INT1);
                        // start
                        typeTable[--k] = readUnsignedShort(w);
                        w += 10;
                    }
                }
                k = readUnsignedShort(varTable);
                w = varTable + INT;
                for (; k > 0; --k) {
                    int index = readUnsignedShort(w + INT1);
                    mv.visitLocalVariable(readUTF8(w + 4, c), index);
                    w += 10;
                }
            }
        }
        return u;
    }

    private int readUnsignedShort(final int index) {
        byte[] b = this.b;
        return ((b[index] & 0xFF) << INT1) | (b[index + 1] & 0xFF);
    }

    private int readInt(final int index) {
        byte[] b = this.b;
        return ((b[index] & 0xFF) << 24) | ((b[index + 1] & 0xFF) << 16)
                | ((b[index + INT] & 0xFF) << INT1) | (b[index + 3] & 0xFF);
    }

    private String readUTF8(int index, final char[] buf) {
        int item = readUnsignedShort(index);
        String s = strings[item];
        if (s != null) {
            return s;
        }
        index = items[item];
        return strings[item] = readUTF(index + INT, readUnsignedShort(index), buf);
    }

    private String readUTF(int index, final int utfLen, final char[] buf) {
        int endIndex = index + utfLen;
        byte[] b = this.b;
        int strLen = 0;
        int c;
        int st = 0;
        char cc = 0;
        while (index < endIndex) {
            c = b[index++];
            switch (st) {
                case 0:
                    c = c & 0xFF;
                    if (c < 0x80) {
                        // 0xxxxxxx
                        buf[strLen++] = (char) c;
                    } else if (c < 0xE0 && c > 0xBF) {
                        // 110x xxxx 10xx xxxx
                        cc = (char) (c & 0x1F);
                        st = 1;
                    } else {
                        // 1110 xxxx 10xx xxxx 10xx xxxx
                        cc = (char) (c & 0x0F);
                        st = INT;
                    }
                    break;

                case 1:
                    // byte 2 of 2-byte char or byte 3 of 3-byte char
                    buf[strLen++] = (char) ((cc << 6) | (c & 0x3F));
                    st = 0;
                    break;

                case INT:
                    // byte 2 of 3-byte char
                    cc = (char) ((cc << 6) | (c & 0x3F));
                    st = 1;
                    break;
                default:
                    st = 0;
            }
        }
        return new String(buf, 0, strLen);
    }
}
