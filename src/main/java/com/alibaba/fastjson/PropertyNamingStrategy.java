package com.alibaba.fastjson;

/**
 * @since 1.2.15
 * @author xwm
 */
public enum PropertyNamingStrategy {

    /**
     * CamelCase
     * PascalCase
     * SnakeCase
     */

    CamelCase,
    PascalCase,
    SnakeCase,
    KebabCase,
    NoChange;

    public static final char CHAR = 'z';
    public static final char CHAR1 = 'a';
    public static final char CHAR2 = 'A';
    public static final char CHAR3 = 'Z';

    public String translate(String propertyName) {
        switch (this) {
            case SnakeCase: {
                StringBuilder buf = new StringBuilder();
                for (int i = 0; i < propertyName.length(); ++i) {
                    char ch = propertyName.charAt(i);
                    if (ch >= CHAR2 && ch <= CHAR3) {
                        char chUcase = (char) (ch + 32);
                        if (i > 0) {
                            buf.append('_');
                        }
                        buf.append(chUcase);
                    } else {
                        buf.append(ch);
                    }
                }
                return buf.toString();
            }
            case KebabCase: {
                StringBuilder buf = new StringBuilder();
                for (int i = 0; i < propertyName.length(); ++i) {
                    char ch = propertyName.charAt(i);
                    if (ch >= CHAR2 && ch <= CHAR3) {
                        char chUcase = (char) (ch + 32);
                        if (i > 0) {
                            buf.append('-');
                        }
                        buf.append(chUcase);
                    } else {
                        buf.append(ch);
                    }
                }
                return buf.toString();
            }
            case PascalCase: {
                char ch = propertyName.charAt(0);
                if (ch >= CHAR1 && ch <= CHAR) {
                    char[] chars = propertyName.toCharArray();
                    chars[0] -= 32;
                    return new String(chars);
                }

                return propertyName;
            }
            case CamelCase: {
                char ch = propertyName.charAt(0);
                if (ch >= CHAR2 && ch <= CHAR3) {
                    char[] chars = propertyName.toCharArray();
                    chars[0] += 32;
                    return new String(chars);
                }

                return propertyName;
            }
            case NoChange:
            default:
                return propertyName;
        }
    }
}
