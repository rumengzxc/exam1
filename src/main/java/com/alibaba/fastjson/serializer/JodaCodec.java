package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Locale;
import java.util.TimeZone;

import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import org.joda.time.*;
import org.joda.time.format.*;

/**
 * @author xwm
 */
public class JodaCodec implements ObjectSerializer, ContextObjectSerializer, ObjectDeserializer {
    public final static JodaCodec INSTANCE = new JodaCodec();

    private final static String DEFAULT_PATTTERN = "yyyy-MM-dd HH:mm:ss";
    private final static DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormat.forPattern(DEFAULT_PATTTERN);
    private final static DateTimeFormatter DEFAULT_FORMATTER_23 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private final static DateTimeFormatter FORMATTER_DT_19_TW = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_CN = DateTimeFormat.forPattern("yyyy年M月d日 HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_CN_1 = DateTimeFormat.forPattern("yyyy年M月d日 H时m分s秒");
    private final static DateTimeFormatter FORMATTER_DT_19_KR = DateTimeFormat.forPattern("yyyy년M월d일 HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_US = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_EUR = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_DE = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss");
    private final static DateTimeFormatter FORMATTER_DT_19_IN = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss");

    private final static DateTimeFormatter FORMATTER_D_8 = DateTimeFormat.forPattern("yyyyMMdd");
    private final static DateTimeFormatter FORMATTER_D_10_TW = DateTimeFormat.forPattern("yyyy/MM/dd");
    private final static DateTimeFormatter FORMATTER_D_10_CN = DateTimeFormat.forPattern("yyyy年M月d日");
    private final static DateTimeFormatter FORMATTER_D_10_KR = DateTimeFormat.forPattern("yyyy년M월d일");
    private final static DateTimeFormatter FORMATTER_D_10_US = DateTimeFormat.forPattern("MM/dd/yyyy");
    private final static DateTimeFormatter FORMATTER_D_10_EUR = DateTimeFormat.forPattern("dd/MM/yyyy");
    private final static DateTimeFormatter FORMATTER_D_10_DE = DateTimeFormat.forPattern("dd.MM.yyyy");
    private final static DateTimeFormatter FORMATTER_D_10_IN = DateTimeFormat.forPattern("dd-MM-yyyy");

    private final static DateTimeFormatter ISO_FIXED_FORMAT =
            DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.getDefault());

    private final static String FORMATTER_ISO_8601_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    private final static String FORMATTER_ISO_8601_PATTERN_23 = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private final static String FORMATTER_ISO_8601_PATTERN_29 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS";
    private final static DateTimeFormatter FORMATTER_ISO_8601 = DateTimeFormat.forPattern(FORMATTER_ISO_8601_PATTERN);
    public static final int INT = 10;
    public static final int INT1 = 8;
    public static final int INT2 = 23;
    public static final int INT3 = 19;
    public static final char CHAR = ':';
    public static final char CHAR1 = '-';
    public static final char CHAR2 = 'T';
    public static final char CHAR3 = ' ';
    public static final char CHAR4 = '/';
    public static final int INT4 = 12;
    public static final String STRING = "US";
    public static final String STRING1 = "BR";
    public static final char CHAR5 = '.';
    public static final int INT5 = 17;
    public static final char CHAR6 = '年';
    public static final char CHAR7 = '秒';
    public static final char CHAR8 = '년';
    public static final int INT6 = 9;


    @Override
    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        return deserialze(parser, type, fieldName, null, 0);
    }

    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName, String format, int feature) {
        JSONLexer lexer = parser.lexer;
        if (lexer.token() == JSONToken.NULL){
            lexer.nextToken();
            return null;
        }

        if (lexer.token() == JSONToken.LITERAL_STRING) {
            String text = lexer.stringVal();
            lexer.nextToken();

            DateTimeFormatter formatter = null;
            if (format != null) {
                if (DEFAULT_PATTTERN.equals(format)) {
                    formatter = DEFAULT_FORMATTER;
                } else {
                    formatter = DateTimeFormat.forPattern(format);
                }
            }

            if ("".equals(text)) {
                return null;
            }

            if (type == LocalDateTime.class) {
                LocalDateTime localDateTime;
                if (text.length() == INT || text.length() == INT1) {
                    LocalDate localDate = parseLocalDate(text, format, formatter);
                    localDateTime = localDate.toLocalDateTime(LocalTime.MIDNIGHT);
                } else {
                    localDateTime = parseDateTime(text, formatter);
                }
                return (T) localDateTime;
            } else if (type == LocalDate.class) {
                LocalDate localDate;
                if (text.length() == INT2) {
                    LocalDateTime localDateTime = LocalDateTime.parse(text);
                    localDate = localDateTime.toLocalDate();
                } else {
                    localDate = parseLocalDate(text, format, formatter);
                }

                return (T) localDate;
            } else if (type == LocalTime.class) {
                LocalTime localDate;
                if (text.length() == INT2) {
                    LocalDateTime localDateTime = LocalDateTime.parse(text);
                    localDate = localDateTime.toLocalTime();
                } else {
                    localDate = LocalTime.parse(text);
                }
                return (T) localDate;
            } else if (type == DateTime.class) {
                if (formatter == DEFAULT_FORMATTER) {
                    formatter = ISO_FIXED_FORMAT;
                }

                DateTime zonedDateTime = parseZonedDateTime(text, formatter);

                return (T) zonedDateTime;
            } else if (type == DateTimeZone.class) {
                DateTimeZone offsetTime = DateTimeZone.forID(text);

                return (T) offsetTime;
            } else if (type == Period.class) {
                Period period = Period.parse(text);

                return (T) period;
            } else if (type == Duration.class) {
                Duration duration = Duration.parse(text);

                return (T) duration;
            } else if (type == Instant.class) {
                boolean digit = true;
                for (int i = 0; i < text.length(); ++i) {
                    char ch = text.charAt(i);
                    if (ch < '0' || ch > '9') {
                        digit = false;
                        break;
                    }
                }
                if (digit && text.length() > INT1 && text.length() < INT3) {
                    long epochMillis = Long.parseLong(text);
                    return (T) new Instant(epochMillis);
                }

                Instant instant = Instant.parse(text);

                return (T) instant;
            } else if (type == DateTimeFormatter.class) {
                return (T) DateTimeFormat.forPattern(text);
            }
        } else if (lexer.token() == JSONToken.LITERAL_INT) {
            long millis = lexer.longValue();
            lexer.nextToken();

            TimeZone timeZone = JSON.defaultTimeZone;
            if (timeZone == null) {
                timeZone = TimeZone.getDefault();
            }

            if (type == DateTime.class) {
                return (T) new DateTime(millis, DateTimeZone.forTimeZone(timeZone));
            }

            LocalDateTime localDateTime =  new LocalDateTime(millis, DateTimeZone.forTimeZone(timeZone));
            if (type == LocalDateTime.class) {
                return (T) localDateTime;
            }

            if (type == LocalDate.class) {
                return (T) localDateTime.toLocalDate();
            }

            if (type == LocalTime.class) {
                return (T) localDateTime.toLocalTime();
            }

            if (type == Instant.class) {
                Instant instant = new Instant(millis);

                return (T) instant;
            }

            throw new UnsupportedOperationException();
        } else {
            throw new UnsupportedOperationException();
        }
        return null;
    }

    protected LocalDateTime parseDateTime(String text, DateTimeFormatter formatter) {
        if (formatter == null) {
            if (text.length() == INT3) {
                char c4 = text.charAt(4);
                char c7 = text.charAt(7);
                char c10 = text.charAt(INT);
                char c13 = text.charAt(13);
                char c16 = text.charAt(16);
                if (c13 == CHAR && c16 == CHAR) {
                    if (c4 == CHAR1 && c7 == CHAR1) {
                        // yyyy-MM-dd  or  yyyy-MM-dd'T'
                        if (c10 == CHAR2) {
                            formatter = FORMATTER_ISO_8601;
                        } else if (c10 == CHAR3) {
                            formatter = DEFAULT_FORMATTER;
                        }
                    } else if (c4 == CHAR4 && c7 == CHAR4) {
                        // tw yyyy/mm/dd
                        formatter = FORMATTER_DT_19_TW;
                    } else {
                        char c0 = text.charAt(0);
                        char c1 = text.charAt(1);
                        char c2 = text.charAt(2);
                        char c3 = text.charAt(3);
                        char c5 = text.charAt(5);
                        if (c2 == CHAR4 && c5 == CHAR4) {
                            // mm/dd/yyyy or mm/dd/yyyy
                            int v0 = (c0 - '0') * INT + (c1 - '0');
                            int v1 = (c3 - '0') * INT + (c4 - '0');
                            if (v0 > INT4) {
                                formatter = FORMATTER_DT_19_EUR;
                            } else if (v1 > INT4) {
                                formatter = FORMATTER_DT_19_US;
                            } else {
                                String country = Locale.getDefault().getCountry();

                                if (STRING.equals(country)) {
                                    formatter = FORMATTER_DT_19_US;
                                } else if (STRING1.equals(country)
                                        || "AU".equals(country)) {
                                    formatter = FORMATTER_DT_19_EUR;
                                }
                            }
                        } else if (c2 == CHAR5 && c5 == CHAR5) {
                            // dd.mm.yyyy
                            formatter = FORMATTER_DT_19_DE;
                        } else if (c2 == CHAR1 && c5 == CHAR1) {
                            // dd-mm-yyyy
                            formatter = FORMATTER_DT_19_IN;
                        }
                    }
                }
            } else if (text.length() == INT2) {
                char c4 = text.charAt(4);
                char c7 = text.charAt(7);
                char c10 = text.charAt(INT);
                char c13 = text.charAt(13);
                char c16 = text.charAt(16);
                char c19 = text.charAt(INT3);

                if (c13 == CHAR
                        && c16 == CHAR
                        && c4 == CHAR1
                        && c7 == CHAR1
                        && c10 == CHAR3
                        && c19 == CHAR5
                ) {
                    formatter = DEFAULT_FORMATTER_23;
                }
            }

            if (text.length() >= INT5) {
                char c4 = text.charAt(4);
                if (c4 == CHAR6) {
                    if (text.charAt(text.length() - 1) == CHAR7) {
                        formatter = FORMATTER_DT_19_CN_1;
                    } else {
                        formatter = FORMATTER_DT_19_CN;
                    }
                } else if (c4 == CHAR8) {
                    formatter = FORMATTER_DT_19_KR;
                }
            }

            boolean digit = true;
            for (int i = 0; i < text.length(); ++i) {
                char ch = text.charAt(i);
                if (ch < '0' || ch > '9') {
                    digit = false;
                    break;
                }
            }
            if (digit && text.length() > INT1 && text.length() < INT3) {
                long epochMillis = Long.parseLong(text);
                return new LocalDateTime(epochMillis, DateTimeZone.forTimeZone(JSON.defaultTimeZone));
            }
        }

        return formatter == null ?
                LocalDateTime.parse(text)
                : LocalDateTime.parse(text, formatter);
    }

    protected LocalDate parseLocalDate(String text, String format, DateTimeFormatter formatter) {
        if (formatter == null) {
            if (text.length() == INT1) {
                formatter = FORMATTER_D_8;
            }

            if (text.length() == INT) {
                char c4 = text.charAt(4);
                char c7 = text.charAt(7);
                if (c4 == CHAR4 && c7 == CHAR4) {
                    // tw yyyy/mm/dd
                    formatter = FORMATTER_D_10_TW;
                }

                char c0 = text.charAt(0);
                char c1 = text.charAt(1);
                char c2 = text.charAt(2);
                char c3 = text.charAt(3);
                char c5 = text.charAt(5);
                if (c2 == CHAR4 && c5 == CHAR4) {
                    // mm/dd/yyyy or mm/dd/yyyy
                    int v0 = (c0 - '0') * INT + (c1 - '0');
                    int v1 = (c3 - '0') * INT + (c4 - '0');
                    if (v0 > INT4) {
                        formatter = FORMATTER_D_10_EUR;
                    } else if (v1 > INT4) {
                        formatter = FORMATTER_D_10_US;
                    } else {
                        String country = Locale.getDefault().getCountry();

                        if (STRING.equals(country)) {
                            formatter = FORMATTER_D_10_US;
                        } else if (STRING1.equals(country)
                                || "AU".equals(country)) {
                            formatter = FORMATTER_D_10_EUR;
                        }
                    }
                } else if (c2 == CHAR5 && c5 == CHAR5) {
                    // dd.mm.yyyy
                    formatter = FORMATTER_D_10_DE;
                } else if (c2 == CHAR1 && c5 == CHAR1) {
                    // dd-mm-yyyy
                    formatter = FORMATTER_D_10_IN;
                }
            }

            if (text.length() >= INT6) {
                char c4 = text.charAt(4);
                if (c4 == CHAR6) {
                    formatter = FORMATTER_D_10_CN;
                } else if (c4 == CHAR8) {
                    formatter = FORMATTER_D_10_KR;
                }
            }

            boolean digit = true;
            for (int i = 0; i < text.length(); ++i) {
                char ch = text.charAt(i);
                if (ch < '0' || ch > '9') {
                    digit = false;
                    break;
                }
            }
            if (digit && text.length() > INT1 && text.length() < INT3) {
                long epochMillis = Long.parseLong(text);
                return new LocalDateTime(epochMillis, DateTimeZone.forTimeZone(JSON.defaultTimeZone))
                        .toLocalDate();
            }
        }

        return formatter == null ?
                LocalDate.parse(text)
                : LocalDate.parse(text, formatter);
    }

    protected DateTime parseZonedDateTime(String text, DateTimeFormatter formatter) {
        if (formatter == null) {
            if (text.length() == INT3) {
                char c4 = text.charAt(4);
                char c7 = text.charAt(7);
                char c10 = text.charAt(INT);
                char c13 = text.charAt(13);
                char c16 = text.charAt(16);
                if (c13 == CHAR && c16 == CHAR) {
                    if (c4 == CHAR1 && c7 == CHAR1) {
                        // yyyy-MM-dd  or  yyyy-MM-dd'T'
                        if (c10 == CHAR2) {
                            formatter = FORMATTER_ISO_8601;
                        } else if (c10 == CHAR3) {
                            formatter = DEFAULT_FORMATTER;
                        }
                    } else if (c4 == CHAR4 && c7 == CHAR4) {
                        // tw yyyy/mm/dd
                        formatter = FORMATTER_DT_19_TW;
                    } else {
                        char c0 = text.charAt(0);
                        char c1 = text.charAt(1);
                        char c2 = text.charAt(2);
                        char c3 = text.charAt(3);
                        char c5 = text.charAt(5);
                        if (c2 == CHAR4 && c5 == CHAR4) {
                            // mm/dd/yyyy or mm/dd/yyyy
                            int v0 = (c0 - '0') * INT + (c1 - '0');
                            int v1 = (c3 - '0') * INT + (c4 - '0');
                            if (v0 > INT4) {
                                formatter = FORMATTER_DT_19_EUR;
                            } else if (v1 > INT4) {
                                formatter = FORMATTER_DT_19_US;
                            } else {
                                String country = Locale.getDefault().getCountry();

                                if (STRING.equals(country)) {
                                    formatter = FORMATTER_DT_19_US;
                                } else if (STRING1.equals(country)
                                        || "AU".equals(country)) {
                                    formatter = FORMATTER_DT_19_EUR;
                                }
                            }
                        } else if (c2 == CHAR5 && c5 == CHAR5) {
                            // dd.mm.yyyy
                            formatter = FORMATTER_DT_19_DE;
                        } else if (c2 == CHAR1 && c5 == CHAR1) {
                            // dd-mm-yyyy
                            formatter = FORMATTER_DT_19_IN;
                        }
                    }
                }
            }

            if (text.length() >= INT5) {
                char c4 = text.charAt(4);
                if (c4 == CHAR6) {
                    if (text.charAt(text.length() - 1) == CHAR7) {
                        formatter = FORMATTER_DT_19_CN_1;
                    } else {
                        formatter = FORMATTER_DT_19_CN;
                    }
                } else if (c4 == CHAR8) {
                    formatter = FORMATTER_DT_19_KR;
                }
            }
        }

        return formatter == null ?
                DateTime.parse(text)
                : DateTime.parse(text, formatter);
    }

    @Override
    public int getFastMatchToken() {
        return JSONToken.LITERAL_STRING;
    }

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType,
                      int features) throws IOException {
        SerializeWriter out = serializer.out;
        if (object == null) {
            out.writeNull();
        } else {
            if (fieldType == null) {
                fieldType = object.getClass();
            }

            if (fieldType == LocalDateTime.class) {
                final int mask = SerializerFeature.UseISO8601DateFormat.getMask();
                LocalDateTime dateTime = (LocalDateTime) object;
                String format = serializer.getDateFormatPattern();

                if (format == null) {
                    if ((features & mask) != 0 || serializer.isEnabled(SerializerFeature.UseISO8601DateFormat)) {
                        format = FORMATTER_ISO_8601_PATTERN;
                    } else {
                        int millis = dateTime.getMillisOfSecond();
                        if (millis == 0) {
                            format = FORMATTER_ISO_8601_PATTERN_23;
                        } else {
                            format = FORMATTER_ISO_8601_PATTERN_29;
                        }
                    }
                }

                if (format != null) {
                    write(out, dateTime, format);
                } else if (out.isEnabled(SerializerFeature.WriteDateUseDateFormat)) {
                    //使用固定格式转化时间
                    write(out, dateTime, JSON.DEFFAULT_DATE_FORMAT);
                } else {
                    out.writeLong(dateTime.toDateTime(DateTimeZone.forTimeZone(JSON.defaultTimeZone)).toInstant().getMillis());
                }
            } else {
                out.writeString(object.toString());
            }
        }
    }

    @Override
    public void write(JSONSerializer serializer, Object object, BeanContext context) throws IOException {
        SerializeWriter out = serializer.out;
        String format = context.getFormat();
        write(out, (ReadablePartial) object, format);
    }

    private void write(SerializeWriter out, ReadablePartial object, String format) {
        DateTimeFormatter formatter;
        if (format.equals(FORMATTER_ISO_8601_PATTERN)) {
            formatter = FORMATTER_ISO_8601;
        } else {
            formatter = DateTimeFormat.forPattern(format);
        }

        String text = formatter.print(object);
        out.writeString(text);
    }
}
