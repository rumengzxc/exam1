package com.alibaba.fastjson.serializer;

import java.io.IOException;

/**
 * @author xwm
 */
public interface ContextObjectSerializer extends ObjectSerializer {

    /**
     * write
     * @param serializer
     * @param object
     * @param context
     * @return
     */
    void write(JSONSerializer serializer, //
               Object object, //
               BeanContext context) throws IOException;
}
