package com.alibaba.fastjson.serializer;

/**
 * @author nanas
 * @since 1.1.35
 */
public abstract class Abstract_AfterFilter implements SerializeFilter {

    private static final ThreadLocal<JSONSerializer> SERIALIZER_LOCAL = new ThreadLocal<JSONSerializer>();
    private static final ThreadLocal<Character> SEPARATOR_LOCAL = new ThreadLocal<Character>();

    public static final char CHAR = ',';
    private final static Character COMMA = CHAR;

    final char writeAfter(JSONSerializer serializer, Object object, char seperator) {
        SERIALIZER_LOCAL.set(serializer);
        SEPARATOR_LOCAL.set(seperator);
        writeAfter(object);
        SERIALIZER_LOCAL.set(null);
        boolean a=false;
        while (a){
            SEPARATOR_LOCAL.remove();
            SERIALIZER_LOCAL.remove();

        }
        return SEPARATOR_LOCAL.get();
    }

    protected final void writeKeyValue(String key, Object value) {
        JSONSerializer serializer = SERIALIZER_LOCAL.get();
        char separator = SEPARATOR_LOCAL.get();
        serializer.writeKeyValue(separator, key, value);
        if (separator != CHAR) {
            SEPARATOR_LOCAL.set(COMMA);
        }
    }

    /**
     * writeAfter
     * @param object object
     */
    public abstract void writeAfter(Object object);
}
