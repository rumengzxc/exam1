package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.asm.*;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.ASMClassLoader;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;

import java.io.Serializable;
import java.lang.reflect.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.alibaba.fastjson.util.ASMUtils.desc;
import static com.alibaba.fastjson.util.ASMUtils.type;
/**
 * @author xwm
 */
public class ASMSerializerFactory implements Opcodes {

    public static final int INT = 256;
    public static final int INT1 = 3;
    public static final String TRIM = "trim";
    protected final ASMClassLoader classLoader = new ASMClassLoader();

    private final AtomicLong seed = new AtomicLong();

    static final String JSON_SERIALIZER = type(JSONSerializer.class);
    static final String OBJECT_SERIALIZER = type(ObjectSerializer.class);
    static final String OBJECT_SERIALIZER_DESC = "L" + OBJECT_SERIALIZER + ";";
    static final String SERIALIZE_WRITER = type(SerializeWriter.class);
    static final String SERIALIZE_WRITER_DESC = "L" + SERIALIZE_WRITER + ";";
    static final String JAVA_BEAN_SERIALIZER = type(JavaBeanSerializer.class);
    static final String JAVA_BEAN_SERIALIZER_DESC = "L" + type(JavaBeanSerializer.class) + ";";
    static final String SERIAL_CONTEXT_DESC = desc(SerialContext.class);
    static final String SERIALIZE_FILTERABLE_DESC = desc(Abstract_SerializeFilterable.class);

    static class Context {

        static final int SERIALIZER = 1;
        static final int OBJ = 2;
        static final int PARAM_FIELD_NAME = INT1;
        static final int PARAM_FIELD_TYPE = 4;
        static final int FEATURES = 5;
        static int fieldName = 6;
        static int original = 7;
        static int processValue = 8;

        private final FieldInfo[] getters;
        private final String className;
        private final SerializeBeanInfo beanInfo;
        private final boolean writeDirect;

        private Map<String, Integer> variants = new HashMap<String, Integer>();
        private int variantIndex = 9;
        private final boolean nonContext;

        public Context(FieldInfo[] getters, //
                       SerializeBeanInfo beanInfo, //
                       String className, //
                       boolean writeDirect, //
                       boolean nonContext) {
            this.getters = getters;
            this.className = className;
            this.beanInfo = beanInfo;
            this.writeDirect = writeDirect;
            this.nonContext = nonContext || beanInfo.beanType.isEnum();
        }

        public int var(String name) {
            Integer i = variants.get(name);
            if (i == null) {
                variants.put(name, variantIndex++);
            }
            i = variants.get(name);
            return i.intValue();
        }

        public int var(String name, int increment) {
            Integer i = variants.get(name);
            if (i == null) {
                variants.put(name, variantIndex);
                variantIndex += increment;
            }
            i = variants.get(name);
            return i.intValue();
        }

        public int getFieldOrinal(String name) {
            int fieldIndex = -1;
            for (int i = 0, size = getters.length; i < size; ++i) {
                FieldInfo item = getters[i];
                if (item.name.equals(name)) {
                    fieldIndex = i;
                    break;
                }
            }
            return fieldIndex;
        }
    }

    public JavaBeanSerializer createJavaBeanSerializer(SerializeBeanInfo beanInfo) throws Exception {
        Class<?> clazz = beanInfo.beanType;
        if (clazz.isPrimitive()) {
            throw new JSONException("unsupportd class " + clazz.getName());
        }

        JSONType jsonType = TypeUtils.getAnnotation(clazz, JSONType.class);

        FieldInfo[] unsortedGetters = beanInfo.fields;

        for (FieldInfo fieldInfo : unsortedGetters) {
            if (fieldInfo.field == null
                    && fieldInfo.method != null
                    && fieldInfo.method.getDeclaringClass().isInterface()) {
                return new JavaBeanSerializer(beanInfo);
            }
        }

        FieldInfo[] getters = beanInfo.sortedFields;

        boolean nativeSorted = beanInfo.sortedFields == beanInfo.fields;

        if (getters.length > INT) {
            return new JavaBeanSerializer(beanInfo);
        }

        for (FieldInfo getter : getters) {
            if (!ASMUtils.checkName(getter.getMember().getName())) {
                return new JavaBeanSerializer(beanInfo);
            }
        }

        String className = "ASMSerializer_" + seed.incrementAndGet() + "_" + clazz.getSimpleName();
        String classNameType;
        String classNameFull;
        Package pkg = ASMSerializerFactory.class.getPackage();
        if (pkg != null) {
            String packageName = pkg.getName();
            classNameType = packageName.replace('.', '/') + "/" + className;
            classNameFull = packageName + "." + className;
        } else {
            classNameType = className;
            classNameFull = className;
        }

        ClassWriter cw = new ClassWriter();
        cw.visit(V1_5
                , ACC_PUBLIC + ACC_SUPER
                , classNameType
                , JAVA_BEAN_SERIALIZER
                , new String[]{OBJECT_SERIALIZER}
        );

        for (FieldInfo fieldInfo : getters) {
            if (fieldInfo.fieldClass.isPrimitive()
                    || fieldInfo.fieldClass == String.class) {
                continue;
            }

            new FieldWriter(cw, ACC_PUBLIC, fieldInfo.name + "_asm_fieldType", "Ljava/lang/reflect/Type;")
                    .visitEnd();

            if (List.class.isAssignableFrom(fieldInfo.fieldClass)) {
                new FieldWriter(cw, ACC_PUBLIC, fieldInfo.name + "_asm_list_item_ser_",
                        OBJECT_SERIALIZER_DESC)
                        .visitEnd();
            }

            new FieldWriter(cw, ACC_PUBLIC, fieldInfo.name + "_asm_ser_", OBJECT_SERIALIZER_DESC)
                    .visitEnd();
        }

        MethodVisitor mw = new MethodWriter(cw, ACC_PUBLIC, "<init>", "(" + desc(SerializeBeanInfo.class) + ")V", null, null);
        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, 1);
        mw.visitMethodInsn(INVOKESPECIAL, JAVA_BEAN_SERIALIZER, "<init>", "(" + desc(SerializeBeanInfo.class) + ")V");

        // init _asm_fieldType
        for (int i = 0; i < getters.length; ++i) {
            FieldInfo fieldInfo = getters[i];
            if (fieldInfo.fieldClass.isPrimitive()
                    || fieldInfo.fieldClass == String.class) {
                continue;
            }

            mw.visitVarInsn(ALOAD, 0);

            if (fieldInfo.method != null) {
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldInfo.declaringClass)));
                mw.visitLdcInsn(fieldInfo.method.getName());
                mw.visitMethodInsn(INVOKESTATIC, type(ASMUtils.class), "getMethodType",
                        "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Type;");

            } else {
                mw.visitVarInsn(ALOAD, 0);
                mw.visitLdcInsn(i);
                mw.visitMethodInsn(INVOKESPECIAL, JAVA_BEAN_SERIALIZER, "getFieldType", "(I)Ljava/lang/reflect/Type;");
            }

            mw.visitFieldInsn(PUTFIELD, classNameType, fieldInfo.name + "_asm_fieldType", "Ljava/lang/reflect/Type;");
        }

        mw.visitInsn(RETURN);
        mw.visitMaxs(4, 4);
        mw.visitEnd();

        boolean DisableCircularReferenceDetect = false;
        if (jsonType != null) {
            for (SerializerFeature featrues : jsonType.serialzeFeatures()) {
                if (featrues == SerializerFeature.DisableCircularReferenceDetect) {
                    DisableCircularReferenceDetect = true;
                    break;
                }
            }
        }

        // 0 write
        // 1 writeNormal
        // 2 writeNonContext
        for (int i = 0; i < INT1; ++i) {
            String methodName;
            boolean nonContext = DisableCircularReferenceDetect;
            boolean writeDirect = false;
            if (i == 0) {
                methodName = "write";
                writeDirect = true;
            } else if (i == 1) {
                methodName = "writeNormal";
            } else {
                writeDirect = true;
                nonContext = true;
                methodName = "writeDirectNonContext";
            }

            Context context = new Context(getters, beanInfo, classNameType, writeDirect,
                    nonContext);

            mw = new MethodWriter(cw,
                    ACC_PUBLIC,
                    methodName,
                    "(L" + JSON_SERIALIZER
                            + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V",
                    null,
                    new String[]{"java/io/IOException"}
            );

            {
                Label endIf_ = new Label();
                mw.visitVarInsn(ALOAD, Context.OBJ);
                mw.visitJumpInsn(IFNONNULL, endIf_);
                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER,
                        "writeNull", "()V");

                mw.visitInsn(RETURN);
                mw.visitLabel(endIf_);
            }

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitFieldInsn(GETFIELD, JSON_SERIALIZER, "out", SERIALIZE_WRITER_DESC);
            mw.visitVarInsn(ASTORE, context.var("out"));

            if ((!nativeSorted)
                    && !context.writeDirect) {

                if (jsonType == null || jsonType.alphabetic()) {
                    Label else1 = new Label();

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isSortField", "()Z");

                    mw.visitJumpInsn(IFNE, else1);
                    mw.visitVarInsn(ALOAD, 0);
                    mw.visitVarInsn(ALOAD, 1);
                    mw.visitVarInsn(ALOAD, 2);
                    mw.visitVarInsn(ALOAD, INT1);
                    mw.visitVarInsn(ALOAD, 4);
                    mw.visitVarInsn(ILOAD, 5);
                    mw.visitMethodInsn(INVOKEVIRTUAL, classNameType,
                            "writeUnsorted", "(L" + JSON_SERIALIZER
                                    + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                    mw.visitInsn(RETURN);

                    mw.visitLabel(else1);
                }
            }

            // isWriteDoubleQuoteDirect
            if (context.writeDirect && !nonContext) {
                Label direct = new Label();
                Label directElse = new Label();

                mw.visitVarInsn(ALOAD, 0);
                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeDirect", "(L" + JSON_SERIALIZER + ";)Z");
                mw.visitJumpInsn(IFNE, directElse);

                mw.visitVarInsn(ALOAD, 0);
                mw.visitVarInsn(ALOAD, 1);
                mw.visitVarInsn(ALOAD, 2);
                mw.visitVarInsn(ALOAD, INT1);
                mw.visitVarInsn(ALOAD, 4);
                mw.visitVarInsn(ILOAD, 5);
                mw.visitMethodInsn(INVOKEVIRTUAL, classNameType,
                        "writeNormal", "(L" + JSON_SERIALIZER
                                + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                mw.visitInsn(RETURN);

                mw.visitLabel(directElse);
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitLdcInsn(SerializerFeature.DisableCircularReferenceDetect.mask);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
                mw.visitJumpInsn(IFEQ, direct);

                mw.visitVarInsn(ALOAD, 0);
                mw.visitVarInsn(ALOAD, 1);
                mw.visitVarInsn(ALOAD, 2);
                mw.visitVarInsn(ALOAD, INT1);
                mw.visitVarInsn(ALOAD, 4);
                mw.visitVarInsn(ILOAD, 5);
                mw.visitMethodInsn(INVOKEVIRTUAL, classNameType, "writeDirectNonContext",
                        "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                mw.visitInsn(RETURN);

                mw.visitLabel(direct);
            }

            // obj
            mw.visitVarInsn(ALOAD, Context.OBJ);
            // serializer
            mw.visitTypeInsn(CHECKCAST, type(clazz));
            // obj
            mw.visitVarInsn(ASTORE, context.var("entity"));
            generateWriteMethod(clazz, mw, getters, context);
            mw.visitInsn(RETURN);
            mw.visitMaxs(7, context.variantIndex + 2);
            mw.visitEnd();
        }

        if (!nativeSorted) {
            // sortField support
            Context context = new Context(getters, beanInfo, classNameType, false,
                    DisableCircularReferenceDetect);

            mw = new MethodWriter(cw, ACC_PUBLIC, "writeUnsorted",
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V",
                    null, new String[]{"java/io/IOException"});

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitFieldInsn(GETFIELD, JSON_SERIALIZER, "out", SERIALIZE_WRITER_DESC);
            mw.visitVarInsn(ASTORE, context.var("out"));

            // obj
            mw.visitVarInsn(ALOAD, Context.OBJ);
            // serializer
            mw.visitTypeInsn(CHECKCAST, type(clazz));
            // obj
            mw.visitVarInsn(ASTORE, context.var("entity"));

            generateWriteMethod(clazz, mw, unsortedGetters, context);

            mw.visitInsn(RETURN);
            mw.visitMaxs(7, context.variantIndex + 2);
            mw.visitEnd();
        }

        // 0 writeAsArray
        // 1 writeAsArrayNormal
        // 2 writeAsArrayNonContext
        for (int i = 0; i < INT1; ++i) {
            String methodName;
            boolean nonContext = DisableCircularReferenceDetect;
            boolean writeDirect = false;
            if (i == 0) {
                methodName = "writeAsArray";
                writeDirect = true;
            } else if (i == 1) {
                methodName = "writeAsArrayNormal";
            } else {
                writeDirect = true;
                nonContext = true;
                methodName = "writeAsArrayNonContext";
            }

            Context context = new Context(getters, beanInfo, classNameType, writeDirect,
                    nonContext);

            mw = new MethodWriter(cw, ACC_PUBLIC, methodName,
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V",
                    null, new String[]{"java/io/IOException"});

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitFieldInsn(GETFIELD, JSON_SERIALIZER, "out", SERIALIZE_WRITER_DESC);
            mw.visitVarInsn(ASTORE, context.var("out"));

            // obj
            mw.visitVarInsn(ALOAD, Context.OBJ);
            // serializer
            mw.visitTypeInsn(CHECKCAST, type(clazz));
            // obj
            mw.visitVarInsn(ASTORE, context.var("entity"));
            generateWriteAsArray(clazz, mw, getters, context);
            mw.visitInsn(RETURN);
            mw.visitMaxs(7, context.variantIndex + 2);
            mw.visitEnd();
        }

        byte[] code = cw.toByteArray();

        Class<?> serializerClass = classLoader.defineClassPublic(classNameFull, code, 0, code.length);
        Constructor<?> constructor = serializerClass.getConstructor(SerializeBeanInfo.class);
        Object instance = constructor.newInstance(beanInfo);

        return (JavaBeanSerializer) instance;
    }

    private void generateWriteAsArray(Class<?> clazz, MethodVisitor mw, FieldInfo[] getters,
                                      Context context) throws Exception {

        Label nonPropertyFilters_ = new Label();
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, 0);
        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "hasPropertyFilters", "(" + SERIALIZE_FILTERABLE_DESC + ")Z");
        mw.visitJumpInsn(IFNE, nonPropertyFilters_);
        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, 1);
        mw.visitVarInsn(ALOAD, 2);
        mw.visitVarInsn(ALOAD, INT1);
        mw.visitVarInsn(ALOAD, 4);
        mw.visitVarInsn(ILOAD, 5);
        mw.visitMethodInsn(INVOKESPECIAL, JAVA_BEAN_SERIALIZER,
                "writeNoneASM", "(L" + JSON_SERIALIZER
                        + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
        mw.visitInsn(RETURN);

        mw.visitLabel(nonPropertyFilters_);
        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(BIPUSH, '[');
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        int size = getters.length;

        if (size == 0) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(BIPUSH, ']');
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            return;
        }

        for (int i = 0; i < size; ++i) {
            final char seperator = (i == size - 1) ? ']' : ',';

            FieldInfo fieldInfo = getters[i];
            Class<?> fieldClass = fieldInfo.fieldClass;

            mw.visitLdcInsn(fieldInfo.name);
            mw.visitVarInsn(ASTORE, Context.fieldName);

            if (fieldClass == byte.class
                    || fieldClass == short.class
                    || fieldClass == int.class) {

                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeInt", "(I)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (fieldClass == long.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeLong", "(J)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (fieldClass == float.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitInsn(ICONST_1);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFloat", "(FZ)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (fieldClass == double.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitInsn(ICONST_1);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeDouble", "(DZ)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (fieldClass == boolean.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(Z)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (fieldClass == char.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                // Character.toString(value)
                get1(mw, context, fieldInfo);
                mw.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "toString", "(C)Ljava/lang/String;");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeString", "(Ljava/lang/String;C)V");

            } else if (fieldClass == String.class) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                get1(mw, context, fieldInfo);
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeString", "(Ljava/lang/String;C)V");
            } else if (fieldClass.isEnum()) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitInsn(DUP);
                get1(mw, context, fieldInfo);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeEnum", "(Ljava/lang/Enum;)V");
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else if (List.class.isAssignableFrom(fieldClass)) {
                Type fieldType = fieldInfo.fieldType;

                Type elementType;
                if (fieldType instanceof Class) {
                    elementType = Object.class;
                } else {
                    elementType = ((ParameterizedType) fieldType).getActualTypeArguments()[0];
                }

                Class<?> elementClass = null;
                if (elementType instanceof Class<?>) {
                    elementClass = (Class<?>) elementType;

                    if (elementClass == Object.class) {
                        elementClass = null;
                    }
                }

                get1(mw, context, fieldInfo);
                mw.visitTypeInsn(CHECKCAST, "java/util/List");
                mw.visitVarInsn(ASTORE, context.var("list"));

                if (elementClass == String.class
                        && context.writeDirect) {
                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitVarInsn(ALOAD, context.var("list"));
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(Ljava/util/List;)V");
                } else {
                    Label nullEnd_ = new Label(), nullElse_ = new Label();

                    mw.visitVarInsn(ALOAD, context.var("list"));
                    mw.visitJumpInsn(IFNONNULL, nullElse_);

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeNull", "()V");
                    mw.visitJumpInsn(GOTO, nullEnd_);

                    mw.visitLabel(nullElse_);

                    mw.visitVarInsn(ALOAD, context.var("list"));
                    mw.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I");
                    mw.visitVarInsn(ISTORE, context.var("size"));

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitVarInsn(BIPUSH, '[');
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

                    Label for_ = new Label(), forFirst_ = new Label(), forEnd_ = new Label();

                    mw.visitInsn(ICONST_0);
                    mw.visitVarInsn(ISTORE, context.var("i"));

                    // for (; i < list.size() -1; ++i) {
                    mw.visitLabel(for_);
                    mw.visitVarInsn(ILOAD, context.var("i"));
                    mw.visitVarInsn(ILOAD, context.var("size"));
                    // i < list.size - 1
                    mw.visitJumpInsn(IF_ICMPGE, forEnd_);

                    mw.visitVarInsn(ILOAD, context.var("i"));
                    // i < list.size - 1
                    mw.visitJumpInsn(IFEQ, forFirst_);

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitVarInsn(BIPUSH, ',');
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

                    mw.visitLabel(forFirst_);

                    mw.visitVarInsn(ALOAD, context.var("list"));
                    mw.visitVarInsn(ILOAD, context.var("i"));
                    mw.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;");
                    mw.visitVarInsn(ASTORE, context.var("list_item"));

                    Label forItemNullEnd_ = new Label(), forItemNullElse_ = new Label();

                    mw.visitVarInsn(ALOAD, context.var("list_item"));
                    mw.visitJumpInsn(IFNONNULL, forItemNullElse_);

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeNull", "()V");
                    mw.visitJumpInsn(GOTO, forItemNullEnd_);

                    mw.visitLabel(forItemNullElse_);

                    Label forItemClassIfEnd_ = new Label(), forItemClassIfElse_ = new Label();
                    if (elementClass != null && Modifier.isPublic(elementClass.getModifiers())) {
                        mw.visitVarInsn(ALOAD, context.var("list_item"));
                        mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
                        mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                        mw.visitJumpInsn(IF_ACMPNE, forItemClassIfElse_);

                        getListFieldItemSer(context, mw, fieldInfo, elementClass);
                        mw.visitVarInsn(ASTORE, context.var("list_item_desc"));

                        Label instanceOfElse_ = new Label(), instanceOfEnd_ = new Label();

                        if (context.writeDirect) {
                            mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                            mw.visitTypeInsn(INSTANCEOF, JAVA_BEAN_SERIALIZER);
                            mw.visitJumpInsn(IFEQ, instanceOfElse_);

                            mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                            mw.visitTypeInsn(CHECKCAST, JAVA_BEAN_SERIALIZER);
                            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                            mw.visitVarInsn(ALOAD, context.var("list_item"));
                            if (context.nonContext) {
                                // fieldName
                                mw.visitInsn(ACONST_NULL);
                            } else {
                                mw.visitVarInsn(ILOAD, context.var("i"));
                                mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf",
                                        "(I)Ljava/lang/Integer;");
                            }
                            mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                            mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                            mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeAsArrayNonContext",
                                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                            mw.visitJumpInsn(GOTO, instanceOfEnd_);

                            mw.visitLabel(instanceOfElse_);
                        }

                        mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                        // object
                        mw.visitVarInsn(ALOAD, context.var("list_item"));
                        if (context.nonContext) {
                            // fieldName
                            mw.visitInsn(ACONST_NULL);
                        } else {
                            mw.visitVarInsn(ILOAD, context.var("i"));
                            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                        }
                        // fieldType
                        mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                        // features
                        mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                        mw.visitMethodInsn(INVOKEINTERFACE, OBJECT_SERIALIZER, "write",
                                "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                        mw.visitLabel(instanceOfEnd_);
                        mw.visitJumpInsn(GOTO, forItemClassIfEnd_);
                    }

                    mw.visitLabel(forItemClassIfElse_);
                    mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                    mw.visitVarInsn(ALOAD, context.var("list_item"));
                    if (context.nonContext) {
                        mw.visitInsn(ACONST_NULL);
                    } else {
                        mw.visitVarInsn(ILOAD, context.var("i"));
                        mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                    }
                    if (elementClass != null && Modifier.isPublic(elementClass.getModifiers())) {
                        mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc((Class<?>) elementType)));
                        mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                                "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                    } else {
                        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                                "(Ljava/lang/Object;Ljava/lang/Object;)V");
                    }
                    mw.visitLabel(forItemClassIfEnd_);
                    mw.visitLabel(forItemNullEnd_);

                    mw.visitIincInsn(context.var("i"), 1);
                    mw.visitJumpInsn(GOTO, for_);

                    mw.visitLabel(forEnd_);

                    mw.visitVarInsn(ALOAD, context.var("out"));
                    mw.visitVarInsn(BIPUSH, ']');
                    mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

                    mw.visitLabel(nullEnd_);
                }

                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            } else {
                Label notNullEnd_ = new Label(), notNullElse_ = new Label();

                get1(mw, context, fieldInfo);
                mw.visitInsn(DUP);
                mw.visitVarInsn(ASTORE, context.var("field_" + fieldInfo.fieldClass.getName()));
                mw.visitJumpInsn(IFNONNULL, notNullElse_);

                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeNull", "()V");
                mw.visitJumpInsn(GOTO, notNullEnd_);

                mw.visitLabel(notNullElse_);

                Label classIfEnd_ = new Label(), classIfElse_ = new Label();
                mw.visitVarInsn(ALOAD, context.var("field_" + fieldInfo.fieldClass.getName()));
                mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldClass)));
                mw.visitJumpInsn(IF_ACMPNE, classIfElse_);

                getFieldSer(context, mw, fieldInfo);
                mw.visitVarInsn(ASTORE, context.var("fied_ser"));

                Label instanceOfElse_ = new Label(), instanceOfEnd_ = new Label();
                if (context.writeDirect && Modifier.isPublic(fieldClass.getModifiers())) {
                    mw.visitVarInsn(ALOAD, context.var("fied_ser"));
                    mw.visitTypeInsn(INSTANCEOF, JAVA_BEAN_SERIALIZER);
                    mw.visitJumpInsn(IFEQ, instanceOfElse_);

                    mw.visitVarInsn(ALOAD, context.var("fied_ser"));
                    mw.visitTypeInsn(CHECKCAST, JAVA_BEAN_SERIALIZER);
                    mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                    mw.visitVarInsn(ALOAD, context.var("field_" + fieldInfo.fieldClass.getName()));
                    mw.visitVarInsn(ALOAD, Context.fieldName);
                    mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldClass)));
                    mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                    mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeAsArrayNonContext",
                            "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                    mw.visitJumpInsn(GOTO, instanceOfEnd_);

                    mw.visitLabel(instanceOfElse_);
                }
                mw.visitVarInsn(ALOAD, context.var("fied_ser"));
                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitVarInsn(ALOAD, context.var("field_" + fieldInfo.fieldClass.getName()));
                mw.visitVarInsn(ALOAD, Context.fieldName);
                // fieldType
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldClass)));
                // features
                mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                mw.visitMethodInsn(INVOKEINTERFACE, OBJECT_SERIALIZER, "write",
                        "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                mw.visitLabel(instanceOfEnd_);
                mw.visitJumpInsn(GOTO, classIfEnd_);

                mw.visitLabel(classIfElse_);
                String format = fieldInfo.getFormat();

                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitVarInsn(ALOAD, context.var("field_" + fieldInfo.fieldClass.getName()));
                if (format != null) {
                    mw.visitLdcInsn(format);
                    mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFormat",
                            "(Ljava/lang/Object;Ljava/lang/String;)V");
                } else {
                    mw.visitVarInsn(ALOAD, Context.fieldName);
                    if (fieldInfo.fieldType instanceof Class<?>
                            && ((Class<?>) fieldInfo.fieldType).isPrimitive()) {
                        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                                "(Ljava/lang/Object;Ljava/lang/Object;)V");
                    } else {
                        mw.visitVarInsn(ALOAD, 0);
                        mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_fieldType",
                                "Ljava/lang/reflect/Type;");
                        mw.visitLdcInsn(fieldInfo.serialzeFeatures);

                        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                                "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                    }
                }
                mw.visitLabel(classIfEnd_);
                mw.visitLabel(notNullEnd_);


                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitVarInsn(BIPUSH, seperator);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
            }
        }
    }

    private void generateWriteMethod(Class<?> clazz, MethodVisitor mw, FieldInfo[] getters,
                                     Context context) throws Exception {

        // if (serializer.containsReference(object)) {
        Label end = new Label();

        int size = getters.length;

        if (!context.writeDirect) {
            // pretty format not byte code optimized
            Label endSupper_ = new Label();
            Label supper_ = new Label();
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitLdcInsn(SerializerFeature.PrettyFormat.mask);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
            mw.visitJumpInsn(IFNE, supper_);

            boolean hasMethod = false;
            for (FieldInfo getter : getters) {
                if (getter.method != null) {
                    hasMethod = true;
                }
            }

            if (hasMethod) {
                mw.visitVarInsn(ALOAD, context.var("out"));
                mw.visitLdcInsn(SerializerFeature.IgnoreErrorGetter.mask);
                mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
                mw.visitJumpInsn(IFEQ, endSupper_);
            } else {
                mw.visitJumpInsn(GOTO, endSupper_);
            }

            mw.visitLabel(supper_);
            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, 1);
            mw.visitVarInsn(ALOAD, 2);
            mw.visitVarInsn(ALOAD, INT1);
            mw.visitVarInsn(ALOAD, 4);
            mw.visitVarInsn(ILOAD, 5);
            mw.visitMethodInsn(INVOKESPECIAL, JAVA_BEAN_SERIALIZER,
                    "write", "(L" + JSON_SERIALIZER
                            + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            mw.visitInsn(RETURN);

            mw.visitLabel(endSupper_);
        }

        if (!context.nonContext) {
            Label endRef_ = new Label();

            // /////
            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, Context.OBJ);
            mw.visitVarInsn(ILOAD, Context.FEATURES);
            mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeReference",
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;I)Z");

            mw.visitJumpInsn(IFEQ, endRef_);

            mw.visitInsn(RETURN);

            mw.visitLabel(endRef_);
        }

        final String writeAsArrayMethodName;

        if (context.writeDirect) {
            if (context.nonContext) {
                writeAsArrayMethodName = "writeAsArrayNonContext";
            } else {
                writeAsArrayMethodName = "writeAsArray";
            }
        } else {
            writeAsArrayMethodName = "writeAsArrayNormal";
        }

        if ((context.beanInfo.features & SerializerFeature.BeanToArray.mask) == 0) {
            Label endWriteAsArray_ = new Label();

            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitLdcInsn(SerializerFeature.BeanToArray.mask);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
            mw.visitJumpInsn(IFEQ, endWriteAsArray_);

            // /////
            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, 2);
            mw.visitVarInsn(ALOAD, INT1);
            mw.visitVarInsn(ALOAD, 4);
            mw.visitVarInsn(ILOAD, 5);
            mw.visitMethodInsn(INVOKEVIRTUAL,
                    context.className,
                    writeAsArrayMethodName,
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");

            mw.visitInsn(RETURN);

            mw.visitLabel(endWriteAsArray_);
        } else {
            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, 2);
            mw.visitVarInsn(ALOAD, INT1);
            mw.visitVarInsn(ALOAD, 4);
            mw.visitVarInsn(ILOAD, 5);
            mw.visitMethodInsn(INVOKEVIRTUAL,
                    context.className,
                    writeAsArrayMethodName,
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            mw.visitInsn(RETURN);
        }

        if (!context.nonContext) {
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "getContext", "()" + SERIAL_CONTEXT_DESC);
            mw.visitVarInsn(ASTORE, context.var("parent"));

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("parent"));
            mw.visitVarInsn(ALOAD, Context.OBJ);
            mw.visitVarInsn(ALOAD, Context.PARAM_FIELD_NAME);
            mw.visitLdcInsn(context.beanInfo.features);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "setContext",
                    "(" + SERIAL_CONTEXT_DESC + "Ljava/lang/Object;Ljava/lang/Object;I)V");
        }

        boolean writeClasName = (context.beanInfo.features & SerializerFeature.WriteClassName.mask) != 0;

        // SEPERATO
        if (writeClasName || !context.writeDirect) {
            Label end_ = new Label();
            Label else_ = new Label();
            Label writeClass_ = new Label();

            if (!writeClasName) {
                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitVarInsn(ALOAD, Context.PARAM_FIELD_TYPE);
                mw.visitVarInsn(ALOAD, Context.OBJ);
                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "isWriteClassName",
                        "(Ljava/lang/reflect/Type;Ljava/lang/Object;)Z");
                mw.visitJumpInsn(IFEQ, else_);
            }

            // IFNULL
            mw.visitVarInsn(ALOAD, Context.PARAM_FIELD_TYPE);
            mw.visitVarInsn(ALOAD, Context.OBJ);
            mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
            mw.visitJumpInsn(IF_ACMPEQ, else_);

            mw.visitLabel(writeClass_);
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(BIPUSH, '{');
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            if (context.beanInfo.typeKey != null) {
                mw.visitLdcInsn(context.beanInfo.typeKey);
            } else {
                mw.visitInsn(ACONST_NULL);
            }
            mw.visitVarInsn(ALOAD, Context.OBJ);

            mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeClassName", "(L" + JSON_SERIALIZER + ";Ljava/lang/String;Ljava/lang/Object;)V");
            mw.visitVarInsn(BIPUSH, ',');
            mw.visitJumpInsn(GOTO, end_);

            mw.visitLabel(else_);
            mw.visitVarInsn(BIPUSH, '{');

            mw.visitLabel(end_);
        } else {
            mw.visitVarInsn(BIPUSH, '{');
        }

        mw.visitVarInsn(ISTORE, context.var("seperator"));

        if (!context.writeDirect) {
            before(mw, context);
        }

        if (!context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isNotWriteDefaultValue", "()Z");
            mw.visitVarInsn(ISTORE, context.var("notWriteDefaultValue"));

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, 0);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "checkValue", "(" + SERIALIZE_FILTERABLE_DESC + ")Z");
            mw.visitVarInsn(ISTORE, context.var("checkValue"));

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, 0);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "hasNameFilters", "(" + SERIALIZE_FILTERABLE_DESC + ")Z");
            mw.visitVarInsn(ISTORE, context.var("hasNameFilters"));
        }

        for (int i = 0; i < size; ++i) {
            FieldInfo property = getters[i];
            Class<?> propertyClass = property.fieldClass;

            mw.visitLdcInsn(property.name);
            mw.visitVarInsn(ASTORE, Context.fieldName);

            if (propertyClass == byte.class
                    || propertyClass == short.class
                    || propertyClass == int.class) {
                int1(clazz, mw, property, context, context.var(propertyClass.getName()), 'I');
            } else if (propertyClass == long.class) {
                long1(clazz, mw, property, context);
            } else if (propertyClass == float.class) {
                float1(clazz, mw, property, context);
            } else if (propertyClass == double.class) {
                double1(clazz, mw, property, context);
            } else if (propertyClass == boolean.class) {
                int1(clazz, mw, property, context, context.var("boolean"), 'Z');
            } else if (propertyClass == char.class) {
                int1(clazz, mw, property, context, context.var("char"), 'C');
            } else if (propertyClass == String.class) {
                string1(clazz, mw, property, context);
            } else if (propertyClass == BigDecimal.class) {
                decimal1(clazz, mw, property, context);
            } else if (List.class.isAssignableFrom(propertyClass)) {
                list1(clazz, mw, property, context);
            } else if (propertyClass.isEnum()) {
                enum1(clazz, mw, property, context);
            } else {
                object1(clazz, mw, property, context);
            }
        }

        if (!context.writeDirect) {
            after(mw, context);
        }

        Label else1 = new Label();
        Label endIf = new Label();

        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitIntInsn(BIPUSH, '{');
        mw.visitJumpInsn(IF_ICMPNE, else1);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(BIPUSH, '{');
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        mw.visitLabel(else1);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(BIPUSH, '}');
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        mw.visitLabel(endIf);
        mw.visitLabel(end);

        if (!context.nonContext) {
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("parent"));
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "setContext", "(" + SERIAL_CONTEXT_DESC + ")V");
        }

    }

    private void object1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end = new Label();

        nameApply(mw, property, context, end);
        get1(mw, context, property);
        mw.visitVarInsn(ASTORE, context.var("object"));

        filters(mw, property, context, end);

        writeObject(mw, property, context, end);

        mw.visitLabel(end);
    }

    private void enum1(Class<?> clazz, MethodVisitor mw, FieldInfo fieldInfo, Context context) {
        Label notNull = new Label();
        Label endIf = new Label();
        Label end1 = new Label();

        nameApply(mw, fieldInfo, context, end1);
        get1(mw, context, fieldInfo);
        mw.visitTypeInsn(CHECKCAST, "java/lang/Enum");
        mw.visitVarInsn(ASTORE, context.var("enum"));

        filters(mw, fieldInfo, context, end1);

        mw.visitVarInsn(ALOAD, context.var("enum"));
        mw.visitJumpInsn(IFNONNULL, notNull);
        ifWriteNull(mw, fieldInfo, context);
        mw.visitJumpInsn(GOTO, endIf);

        mw.visitLabel(notNull);

        if (context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ILOAD, context.var("seperator"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitVarInsn(ALOAD, context.var("enum"));
            mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Enum", "name", "()Ljava/lang/String;");
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValueStringWithDoubleQuote",
                    "(CLjava/lang/String;Ljava/lang/String;)V");
        } else {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ILOAD, context.var("seperator"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitInsn(ICONST_0);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldName", "(Ljava/lang/String;Z)V");

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("enum"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc((Class<?>) fieldInfo.fieldClass)));
            mw.visitLdcInsn(fieldInfo.serialzeFeatures);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                    "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
        }

        seperator(mw, context);

        mw.visitLabel(endIf);
        mw.visitLabel(end1);
    }

    private void int1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context, int var, char type) {
        Label end_ = new Label();

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(ISTORE, var);

        filters(mw, property, context, end_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitVarInsn(ALOAD, Context.fieldName);
        mw.visitVarInsn(ILOAD, var);

        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue", "(CLjava/lang/String;" + type + ")V");

        seperator(mw, context);

        mw.visitLabel(end_);
    }

    private void long1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end_ = new Label();

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(LSTORE, context.var("long", 2));

        filters(mw, property, context, end_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitVarInsn(ALOAD, Context.fieldName);
        mw.visitVarInsn(LLOAD, context.var("long", 2));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue", "(CLjava/lang/String;J)V");

        seperator(mw, context);

        mw.visitLabel(end_);
    }

    private void float1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end_ = new Label();

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(FSTORE, context.var("float"));

        filters(mw, property, context, end_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitVarInsn(ALOAD, Context.fieldName);
        mw.visitVarInsn(FLOAD, context.var("float"));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue", "(CLjava/lang/String;F)V");

        seperator(mw, context);

        mw.visitLabel(end_);
    }

    private void double1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end_ = new Label();

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(DSTORE, context.var("double", 2));

        filters(mw, property, context, end_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitVarInsn(ALOAD, Context.fieldName);
        mw.visitVarInsn(DLOAD, context.var("double", 2));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue", "(CLjava/lang/String;D)V");

        seperator(mw, context);

        mw.visitLabel(end_);
    }

    private void get1(MethodVisitor mw, Context context, FieldInfo fieldInfo) {
        Method method = fieldInfo.method;
        if (method != null) {
            mw.visitVarInsn(ALOAD, context.var("entity"));
            Class<?> declaringClass = method.getDeclaringClass();
            mw.visitMethodInsn(declaringClass.isInterface() ? INVOKEINTERFACE : INVOKEVIRTUAL, type(declaringClass), method.getName(), desc(method));
            if (!method.getReturnType().equals(fieldInfo.fieldClass)) {
                mw.visitTypeInsn(CHECKCAST, type(fieldInfo.fieldClass));
            }
        } else {
            mw.visitVarInsn(ALOAD, context.var("entity"));
            Field field = fieldInfo.field;
            mw.visitFieldInsn(GETFIELD, type(fieldInfo.declaringClass), field.getName(),
                    desc(field.getType()));
            if (!field.getType().equals(fieldInfo.fieldClass)) {
                mw.visitTypeInsn(CHECKCAST, type(fieldInfo.fieldClass));
            }
        }
    }

    private void decimal1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end_ = new Label();

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(ASTORE, context.var("decimal"));

        filters(mw, property, context, end_);

        Label if_ = new Label();
        Label else_ = new Label();
        Label endIf_ = new Label();

        mw.visitLabel(if_);

        // if (decimalValue == null) {
        mw.visitVarInsn(ALOAD, context.var("decimal"));
        mw.visitJumpInsn(IFNONNULL, else_);
        ifWriteNull(mw, property, context);
        mw.visitJumpInsn(GOTO, endIf_);

        mw.visitLabel(else_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitVarInsn(ALOAD, Context.fieldName);
        mw.visitVarInsn(ALOAD, context.var("decimal"));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue",
                "(CLjava/lang/String;Ljava/math/BigDecimal;)V");

        seperator(mw, context);
        mw.visitJumpInsn(GOTO, endIf_);

        mw.visitLabel(endIf_);

        mw.visitLabel(end_);
    }

    private void string1(Class<?> clazz, MethodVisitor mw, FieldInfo property, Context context) {
        Label end_ = new Label();

        if (property.name.equals(context.beanInfo.typeKey)) {
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, Context.PARAM_FIELD_TYPE);
            mw.visitVarInsn(ALOAD, Context.OBJ);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "isWriteClassName",
                    "(Ljava/lang/reflect/Type;Ljava/lang/Object;)Z");
            mw.visitJumpInsn(IFNE, end_);
        }

        nameApply(mw, property, context, end_);
        get1(mw, context, property);
        mw.visitVarInsn(ASTORE, context.var("string"));

        filters(mw, property, context, end_);

        Label else_ = new Label();
        Label endIf_ = new Label();

        // if (value == null) {
        mw.visitVarInsn(ALOAD, context.var("string"));
        mw.visitJumpInsn(IFNONNULL, else_);

        ifWriteNull(mw, property, context);

        mw.visitJumpInsn(GOTO, endIf_);

        mw.visitLabel(else_);


        if (TRIM.equals(property.format)) {
            mw.visitVarInsn(ALOAD, context.var("string"));
            mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", TRIM, "()Ljava/lang/String;");
            mw.visitVarInsn(ASTORE, context.var("string"));
        }

        if (context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ILOAD, context.var("seperator"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitVarInsn(ALOAD, context.var("string"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValueStringWithDoubleQuoteCheck",
                    "(CLjava/lang/String;Ljava/lang/String;)V");
        } else {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ILOAD, context.var("seperator"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitVarInsn(ALOAD, context.var("string"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldValue",
                    "(CLjava/lang/String;Ljava/lang/String;)V");
        }
        seperator(mw, context);

        mw.visitLabel(endIf_);

        mw.visitLabel(end_);
    }

    private void list1(Class<?> clazz, MethodVisitor mw, FieldInfo fieldInfo, Context context) {
        Type propertyType = fieldInfo.fieldType;

        Type elementType = TypeUtils.getCollectionItemType(propertyType);

        Class<?> elementClass = null;
        if (elementType instanceof Class<?>) {
            elementClass = (Class<?>) elementType;
        }

        if (elementClass == Object.class
                || elementClass == Serializable.class) {
            elementClass = null;
        }

        Label end_ = new Label(), else_ = new Label(), endIf_ = new Label();

        nameApply(mw, fieldInfo, context, end_);
        get1(mw, context, fieldInfo);
        mw.visitTypeInsn(CHECKCAST, "java/util/List");
        mw.visitVarInsn(ASTORE, context.var("list"));

        filters(mw, fieldInfo, context, end_);

        mw.visitVarInsn(ALOAD, context.var("list"));
        mw.visitJumpInsn(IFNONNULL, else_);
        ifWriteNull(mw, fieldInfo, context);
        mw.visitJumpInsn(GOTO, endIf_);

        mw.visitLabel(else_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        writeFieldName(mw, context);

        //
        mw.visitVarInsn(ALOAD, context.var("list"));
        mw.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I");
        mw.visitVarInsn(ISTORE, context.var("size"));

        Label else3 = new Label();
        Label endIf3 = new Label();

        mw.visitVarInsn(ILOAD, context.var("size"));
        mw.visitInsn(ICONST_0);
        mw.visitJumpInsn(IF_ICMPNE, else3);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitLdcInsn("[]");
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(Ljava/lang/String;)V");

        mw.visitJumpInsn(GOTO, endIf3);

        mw.visitLabel(else3);

        if (!context.nonContext) {
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("list"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "setContext", "(Ljava/lang/Object;Ljava/lang/Object;)V");
        }

        if (elementType == String.class
                && context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ALOAD, context.var("list"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(Ljava/util/List;)V");
        } else {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(BIPUSH, '[');
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

            Label for_ = new Label(), forFirst_ = new Label(), forEnd_ = new Label();

            mw.visitInsn(ICONST_0);
            mw.visitVarInsn(ISTORE, context.var("i"));

            // for (; i < list.size() -1; ++i) {
            mw.visitLabel(for_);
            mw.visitVarInsn(ILOAD, context.var("i"));
            mw.visitVarInsn(ILOAD, context.var("size"));
            // i < list.size - 1
            mw.visitJumpInsn(IF_ICMPGE, forEnd_);

            mw.visitVarInsn(ILOAD, context.var("i"));
            // i < list.size - 1
            mw.visitJumpInsn(IFEQ, forFirst_);

            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(BIPUSH, ',');
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

            mw.visitLabel(forFirst_);

            mw.visitVarInsn(ALOAD, context.var("list"));
            mw.visitVarInsn(ILOAD, context.var("i"));
            mw.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;");
            mw.visitVarInsn(ASTORE, context.var("list_item"));

            Label forItemNullEnd_ = new Label(), forItemNullElse_ = new Label();

            mw.visitVarInsn(ALOAD, context.var("list_item"));
            mw.visitJumpInsn(IFNONNULL, forItemNullElse_);

            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeNull", "()V");
            mw.visitJumpInsn(GOTO, forItemNullEnd_);

            mw.visitLabel(forItemNullElse_);

            Label forItemClassIfEnd_ = new Label(), forItemClassIfElse_ = new Label();
            if (elementClass != null && Modifier.isPublic(elementClass.getModifiers())) {
                mw.visitVarInsn(ALOAD, context.var("list_item"));
                mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                mw.visitJumpInsn(IF_ACMPNE, forItemClassIfElse_);

                getListFieldItemSer(context, mw, fieldInfo, elementClass);
                mw.visitVarInsn(ASTORE, context.var("list_item_desc"));

                Label instanceOfElse_ = new Label(), instanceOfEnd_ = new Label();

                if (context.writeDirect) {
                    String writeMethodName = context.nonContext && context.writeDirect ?
                            "writeDirectNonContext"
                            : "write";
                    mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                    mw.visitTypeInsn(INSTANCEOF, JAVA_BEAN_SERIALIZER);
                    mw.visitJumpInsn(IFEQ, instanceOfElse_);

                    mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                    mw.visitTypeInsn(CHECKCAST, JAVA_BEAN_SERIALIZER);
                    mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                    mw.visitVarInsn(ALOAD, context.var("list_item"));
                    if (context.nonContext) {
                        // fieldName
                        mw.visitInsn(ACONST_NULL);
                    } else {
                        mw.visitVarInsn(ILOAD, context.var("i"));
                        mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                    }
                    mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                    mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                    mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, writeMethodName,
                            "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                    mw.visitJumpInsn(GOTO, instanceOfEnd_);

                    mw.visitLabel(instanceOfElse_);
                }
                mw.visitVarInsn(ALOAD, context.var("list_item_desc"));
                mw.visitVarInsn(ALOAD, Context.SERIALIZER);
                mw.visitVarInsn(ALOAD, context.var("list_item"));
                if (context.nonContext) {
                    // fieldName
                    mw.visitInsn(ACONST_NULL);
                } else {
                    mw.visitVarInsn(ILOAD, context.var("i"));
                    mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                }
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(elementClass)));
                mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                mw.visitMethodInsn(INVOKEINTERFACE, OBJECT_SERIALIZER, "write",
                        "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");

                mw.visitLabel(instanceOfEnd_);
                mw.visitJumpInsn(GOTO, forItemClassIfEnd_);
            }

            mw.visitLabel(forItemClassIfElse_);

            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("list_item"));
            if (context.nonContext) {
                mw.visitInsn(ACONST_NULL);
            } else {
                mw.visitVarInsn(ILOAD, context.var("i"));
                mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
            }

            if (elementClass != null && Modifier.isPublic(elementClass.getModifiers())) {
                mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc((Class<?>) elementType)));
                mw.visitLdcInsn(fieldInfo.serialzeFeatures);
                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            } else {
                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                        "(Ljava/lang/Object;Ljava/lang/Object;)V");
            }

            mw.visitLabel(forItemClassIfEnd_);
            mw.visitLabel(forItemNullEnd_);

            mw.visitIincInsn(context.var("i"), 1);
            mw.visitJumpInsn(GOTO, for_);

            mw.visitLabel(forEnd_);

            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(BIPUSH, ']');
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");
        }

        {
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "popContext", "()V");
        }

        mw.visitLabel(endIf3);

        seperator(mw, context);

        mw.visitLabel(endIf_);

        mw.visitLabel(end_);
    }

    private void filters(MethodVisitor mw, FieldInfo property, Context context, Label end1) {
        if (property.fieldTransient) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitLdcInsn(SerializerFeature.SkipTransientField.mask);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
            mw.visitJumpInsn(IFNE, end1);
        }

        notWriteDefault(mw, property, context, end1);

        if (context.writeDirect) {
            return;
        }

        apply(mw, property, context);
        mw.visitJumpInsn(IFEQ, end1);

        processKey(mw, property, context);

        processValue(mw, property, context, end1);
    }

    private void nameApply(MethodVisitor mw, FieldInfo property, Context context, Label end1) {
        if (!context.writeDirect) {
            mw.visitVarInsn(ALOAD, 0);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, Context.OBJ);
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "applyName",
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/String;)Z");
            mw.visitJumpInsn(IFEQ, end1);

            labelApply(mw, property, context, end1);
        }

        if (property.field == null) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitLdcInsn(SerializerFeature.IgnoreNonFieldGetter.mask);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");

            // if true
            mw.visitJumpInsn(IFNE, end1);
        }
    }

    private void labelApply(MethodVisitor mw, FieldInfo property, Context context, Label end1) {
        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitLdcInsn(property.label);
        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "applyLabel",
                "(L" + JSON_SERIALIZER + ";Ljava/lang/String;)Z");
        mw.visitJumpInsn(IFEQ, end1);
    }

    private void writeObject(MethodVisitor mw, FieldInfo fieldInfo, Context context, Label end1) {
        String format = fieldInfo.getFormat();
        Class<?> fieldClass = fieldInfo.fieldClass;

        Label notNull_ = new Label();

        // if (obj == null)
        if (context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("object"));
        } else {
            mw.visitVarInsn(ALOAD, Context.processValue);
        }
        mw.visitInsn(DUP);
        mw.visitVarInsn(ASTORE, context.var("object"));
        mw.visitJumpInsn(IFNONNULL, notNull_);
        ifWriteNull(mw, fieldInfo, context);
        mw.visitJumpInsn(GOTO, end1);

        mw.visitLabel(notNull_);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        writeFieldName(mw, context);

        Label classIfEnd_ = new Label(), classIfElse_ = new Label();
        if (Modifier.isPublic(fieldClass.getModifiers())
                && !ParserConfig.isPrimitive2(fieldClass)
        ) {
            mw.visitVarInsn(ALOAD, context.var("object"));
            mw.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
            mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldClass)));
            mw.visitJumpInsn(IF_ACMPNE, classIfElse_);

            getFieldSer(context, mw, fieldInfo);
            mw.visitVarInsn(ASTORE, context.var("fied_ser"));

            Label instanceOfElse_ = new Label(), instanceOfEnd_ = new Label();
            mw.visitVarInsn(ALOAD, context.var("fied_ser"));
            mw.visitTypeInsn(INSTANCEOF, JAVA_BEAN_SERIALIZER);
            mw.visitJumpInsn(IFEQ, instanceOfElse_);

            boolean disableCircularReferenceDetect = (fieldInfo.serialzeFeatures & SerializerFeature.DisableCircularReferenceDetect.mask) != 0;
            boolean fieldBeanToArray = (fieldInfo.serialzeFeatures & SerializerFeature.BeanToArray.mask) != 0;
            String writeMethodName;
            boolean r1 = (context.nonContext && context.writeDirect);
            if (disableCircularReferenceDetect || r1) {
                writeMethodName = fieldBeanToArray ? "writeAsArrayNonContext" : "writeDirectNonContext";
            } else {
                writeMethodName = fieldBeanToArray ? "writeAsArray" : "write";
            }

            mw.visitVarInsn(ALOAD, context.var("fied_ser"));
            mw.visitTypeInsn(CHECKCAST, JAVA_BEAN_SERIALIZER);
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("object"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitVarInsn(ALOAD, 0);
            mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_fieldType",
                    "Ljava/lang/reflect/Type;");
            mw.visitLdcInsn(fieldInfo.serialzeFeatures);
            mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, writeMethodName,
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            mw.visitJumpInsn(GOTO, instanceOfEnd_);

            mw.visitLabel(instanceOfElse_);

            mw.visitVarInsn(ALOAD, context.var("fied_ser"));
            mw.visitVarInsn(ALOAD, Context.SERIALIZER);
            mw.visitVarInsn(ALOAD, context.var("object"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitVarInsn(ALOAD, 0);
            mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_fieldType",
                    "Ljava/lang/reflect/Type;");
            mw.visitLdcInsn(fieldInfo.serialzeFeatures);
            mw.visitMethodInsn(INVOKEINTERFACE, OBJECT_SERIALIZER, "write",
                    "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");

            mw.visitLabel(instanceOfEnd_);
            mw.visitJumpInsn(GOTO, classIfEnd_);
        }

        mw.visitLabel(classIfElse_);

        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        if (context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("object"));
        } else {
            mw.visitVarInsn(ALOAD, Context.processValue);
        }
        if (format != null) {
            mw.visitLdcInsn(format);
            mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFormat",
                    "(Ljava/lang/Object;Ljava/lang/String;)V");
        } else {
            mw.visitVarInsn(ALOAD, Context.fieldName);
            if (fieldInfo.fieldType instanceof Class<?>
                    && ((Class<?>) fieldInfo.fieldType).isPrimitive()) {
                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                        "(Ljava/lang/Object;Ljava/lang/Object;)V");
            } else {
                if (fieldInfo.fieldClass == String.class) {
                    mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(String.class)));
                } else {
                    mw.visitVarInsn(ALOAD, 0);
                    mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_fieldType",
                            "Ljava/lang/reflect/Type;");
                }
                mw.visitLdcInsn(fieldInfo.serialzeFeatures);

                mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "writeWithFieldName",
                        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            }
        }
        mw.visitLabel(classIfEnd_);

        seperator(mw, context);
    }

    private void before(MethodVisitor mw, Context context) {
        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, Context.OBJ);
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeBefore",
                "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;C)C");
        mw.visitVarInsn(ISTORE, context.var("seperator"));
    }

    private void after(MethodVisitor mw, Context context) {
        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, 2);
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "writeAfter",
                "(L" + JSON_SERIALIZER + ";Ljava/lang/Object;C)C");
        mw.visitVarInsn(ISTORE, context.var("seperator"));
    }

    private void notWriteDefault(MethodVisitor mw, FieldInfo property, Context context, Label end1) {
        if (context.writeDirect) {
            return;
        }

        Label elseLabel = new Label();

        mw.visitVarInsn(ILOAD, context.var("notWriteDefaultValue"));
        mw.visitJumpInsn(IFEQ, elseLabel);

        Class<?> propertyClass = property.fieldClass;
        if (propertyClass == boolean.class) {
            mw.visitVarInsn(ILOAD, context.var("boolean"));
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == byte.class) {
            mw.visitVarInsn(ILOAD, context.var("byte"));
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == short.class) {
            mw.visitVarInsn(ILOAD, context.var("short"));
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == int.class) {
            mw.visitVarInsn(ILOAD, context.var("int"));
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == long.class) {
            mw.visitVarInsn(LLOAD, context.var("long"));
            mw.visitInsn(LCONST_0);
            mw.visitInsn(LCMP);
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == float.class) {
            mw.visitVarInsn(FLOAD, context.var("float"));
            mw.visitInsn(FCONST_0);
            mw.visitInsn(FCMPL);
            mw.visitJumpInsn(IFEQ, end1);
        } else if (propertyClass == double.class) {
            mw.visitVarInsn(DLOAD, context.var("double"));
            mw.visitInsn(DCONST_0);
            mw.visitInsn(DCMPL);
            mw.visitJumpInsn(IFEQ, end1);
        }

        mw.visitLabel(elseLabel);
    }

    private void apply(MethodVisitor mw, FieldInfo property, Context context) {
        Class<?> propertyClass = property.fieldClass;

        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, Context.OBJ);
        mw.visitVarInsn(ALOAD, Context.fieldName);

        if (propertyClass == byte.class) {
            mw.visitVarInsn(ILOAD, context.var("byte"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
        } else if (propertyClass == short.class) {
            mw.visitVarInsn(ILOAD, context.var("short"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
        } else if (propertyClass == int.class) {
            mw.visitVarInsn(ILOAD, context.var("int"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
        } else if (propertyClass == char.class) {
            mw.visitVarInsn(ILOAD, context.var("char"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
        } else if (propertyClass == long.class) {
            mw.visitVarInsn(LLOAD, context.var("long", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
        } else if (propertyClass == float.class) {
            mw.visitVarInsn(FLOAD, context.var("float"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
        } else if (propertyClass == double.class) {
            mw.visitVarInsn(DLOAD, context.var("double", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
        } else if (propertyClass == boolean.class) {
            mw.visitVarInsn(ILOAD, context.var("boolean"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
        } else if (propertyClass == BigDecimal.class) {
            mw.visitVarInsn(ALOAD, context.var("decimal"));
        } else if (propertyClass == String.class) {
            mw.visitVarInsn(ALOAD, context.var("string"));
        } else if (propertyClass.isEnum()) {
            mw.visitVarInsn(ALOAD, context.var("enum"));
        } else if (List.class.isAssignableFrom(propertyClass)) {
            mw.visitVarInsn(ALOAD, context.var("list"));
        } else {
            mw.visitVarInsn(ALOAD, context.var("object"));
        }
        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER,
                "apply", "(L" + JSON_SERIALIZER
                        + ";Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
    }

    private void processValue(MethodVisitor mw, FieldInfo fieldInfo, Context context, Label end1) {
        Label processKeyElse_ = new Label();

        Class<?> fieldClass = fieldInfo.fieldClass;

        if (fieldClass.isPrimitive()) {
            Label checkValueEnd_ = new Label();
            mw.visitVarInsn(ILOAD, context.var("checkValue"));
            mw.visitJumpInsn(IFNE, checkValueEnd_);

            mw.visitInsn(ACONST_NULL);
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ASTORE, Context.processValue);
            mw.visitJumpInsn(GOTO, processKeyElse_);

            mw.visitLabel(checkValueEnd_);
        }

        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, 0);
        mw.visitLdcInsn(context.getFieldOrinal(fieldInfo.name));
        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "getBeanContext", "(I)" + desc(BeanContext.class));
        mw.visitVarInsn(ALOAD, Context.OBJ);
        mw.visitVarInsn(ALOAD, Context.fieldName);

        String valueDesc = "Ljava/lang/Object;";
        if (fieldClass == byte.class) {
            mw.visitVarInsn(ILOAD, context.var("byte"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == short.class) {
            mw.visitVarInsn(ILOAD, context.var("short"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == int.class) {
            mw.visitVarInsn(ILOAD, context.var("int"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == char.class) {
            mw.visitVarInsn(ILOAD, context.var("char"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == long.class) {
            mw.visitVarInsn(LLOAD, context.var("long", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == float.class) {
            mw.visitVarInsn(FLOAD, context.var("float"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == double.class) {
            mw.visitVarInsn(DLOAD, context.var("double", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == boolean.class) {
            mw.visitVarInsn(ILOAD, context.var("boolean"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
            mw.visitInsn(DUP);
            mw.visitVarInsn(ASTORE, Context.original);
        } else if (fieldClass == BigDecimal.class) {
            mw.visitVarInsn(ALOAD, context.var("decimal"));
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ALOAD, Context.original);
        } else if (fieldClass == String.class) {
            mw.visitVarInsn(ALOAD, context.var("string"));
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ALOAD, Context.original);
        } else if (fieldClass.isEnum()) {
            mw.visitVarInsn(ALOAD, context.var("enum"));
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ALOAD, Context.original);
        } else if (List.class.isAssignableFrom(fieldClass)) {
            mw.visitVarInsn(ALOAD, context.var("list"));
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ALOAD, Context.original);
        } else {
            mw.visitVarInsn(ALOAD, context.var("object"));
            mw.visitVarInsn(ASTORE, Context.original);
            mw.visitVarInsn(ALOAD, Context.original);
        }

        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER, "processValue",
                "(L" + JSON_SERIALIZER + ";"
                        + desc(BeanContext.class)
                        + "Ljava/lang/Object;Ljava/lang/String;"
                        + valueDesc + ")Ljava/lang/Object;");

        mw.visitVarInsn(ASTORE, Context.processValue);

        mw.visitVarInsn(ALOAD, Context.original);
        mw.visitVarInsn(ALOAD, Context.processValue);
        mw.visitJumpInsn(IF_ACMPEQ, processKeyElse_);
        writeObject(mw, fieldInfo, context, end1);
        mw.visitJumpInsn(GOTO, end1);

        mw.visitLabel(processKeyElse_);
    }

    private void processKey(MethodVisitor mw, FieldInfo property, Context context) {
        Label elseProcessKey = new Label();

        mw.visitVarInsn(ILOAD, context.var("hasNameFilters"));
        mw.visitJumpInsn(IFEQ, elseProcessKey);

        Class<?> propertyClass = property.fieldClass;

        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitVarInsn(ALOAD, Context.OBJ);
        mw.visitVarInsn(ALOAD, Context.fieldName);

        if (propertyClass == byte.class) {
            mw.visitVarInsn(ILOAD, context.var("byte"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
        } else if (propertyClass == short.class) {
            mw.visitVarInsn(ILOAD, context.var("short"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
        } else if (propertyClass == int.class) {
            mw.visitVarInsn(ILOAD, context.var("int"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
        } else if (propertyClass == char.class) {
            mw.visitVarInsn(ILOAD, context.var("char"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
        } else if (propertyClass == long.class) {
            mw.visitVarInsn(LLOAD, context.var("long", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
        } else if (propertyClass == float.class) {
            mw.visitVarInsn(FLOAD, context.var("float"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
        } else if (propertyClass == double.class) {
            mw.visitVarInsn(DLOAD, context.var("double", 2));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
        } else if (propertyClass == boolean.class) {
            mw.visitVarInsn(ILOAD, context.var("boolean"));
            mw.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
        } else if (propertyClass == BigDecimal.class) {
            mw.visitVarInsn(ALOAD, context.var("decimal"));
        } else if (propertyClass == String.class) {
            mw.visitVarInsn(ALOAD, context.var("string"));
        } else if (propertyClass.isEnum()) {
            mw.visitVarInsn(ALOAD, context.var("enum"));
        } else if (List.class.isAssignableFrom(propertyClass)) {
            mw.visitVarInsn(ALOAD, context.var("list"));
        } else {
            mw.visitVarInsn(ALOAD, context.var("object"));
        }

        mw.visitMethodInsn(INVOKEVIRTUAL, JAVA_BEAN_SERIALIZER,
                "processKey", "(L" + JSON_SERIALIZER
                        + ";Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");

        mw.visitVarInsn(ASTORE, Context.fieldName);

        mw.visitLabel(elseProcessKey);
    }

    private void ifWriteNull(MethodVisitor mw, FieldInfo fieldInfo, Context context) {
        Class<?> propertyClass = fieldInfo.fieldClass;

        Label if1 = new Label();
        Label else1 = new Label();
        Label writeNull = new Label();
        Label endIf = new Label();

        mw.visitLabel(if1);

        JSONField annotation = fieldInfo.getAnnotation();
        int features = 0;
        if (annotation != null) {
            features = SerializerFeature.of(annotation.serialzeFeatures());
        }
        JSONType jsonType = context.beanInfo.jsonType;
        if (jsonType != null) {
            features |= SerializerFeature.of(jsonType.serialzeFeatures());
        }

        int writeNullFeatures;
        if (propertyClass == String.class) {
            writeNullFeatures = SerializerFeature.WriteMapNullValue.getMask()
                    | SerializerFeature.WriteNullStringAsEmpty.getMask();
        } else if (Number.class.isAssignableFrom(propertyClass)) {
            writeNullFeatures = SerializerFeature.WriteMapNullValue.getMask()
                    | SerializerFeature.WriteNullNumberAsZero.getMask();
        } else if (Collection.class.isAssignableFrom(propertyClass)) {
            writeNullFeatures = SerializerFeature.WriteMapNullValue.getMask()
                    | SerializerFeature.WriteNullListAsEmpty.getMask();
        } else if (Boolean.class == propertyClass) {
            writeNullFeatures = SerializerFeature.WriteMapNullValue.getMask()
                    | SerializerFeature.WriteNullBooleanAsFalse.getMask();
        } else {
            writeNullFeatures = SerializerFeature.WRITE_MAP_NULL_FEATURES;
        }

        if ((features & writeNullFeatures) == 0) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitLdcInsn(writeNullFeatures);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "isEnabled", "(I)Z");
            mw.visitJumpInsn(IFEQ, else1);
        }

        mw.visitLabel(writeNull);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitVarInsn(ILOAD, context.var("seperator"));
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "write", "(I)V");

        writeFieldName(mw, context);

        mw.visitVarInsn(ALOAD, context.var("out"));
        mw.visitLdcInsn(features);
        // features

        if (propertyClass == String.class || propertyClass == Character.class) {
            mw.visitLdcInsn(SerializerFeature.WriteNullStringAsEmpty.mask);
        } else if (Number.class.isAssignableFrom(propertyClass)) {
            mw.visitLdcInsn(SerializerFeature.WriteNullNumberAsZero.mask);
        } else if (propertyClass == Boolean.class) {
            mw.visitLdcInsn(SerializerFeature.WriteNullBooleanAsFalse.mask);
        } else if (Collection.class.isAssignableFrom(propertyClass) || propertyClass.isArray()) {
            mw.visitLdcInsn(SerializerFeature.WriteNullListAsEmpty.mask);
        } else {
            mw.visitLdcInsn(0);
        }
        mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeNull", "(II)V");

        // seperator = ',';
        seperator(mw, context);

        mw.visitJumpInsn(GOTO, endIf);

        mw.visitLabel(else1);

        mw.visitLabel(endIf);
    }

    private void writeFieldName(MethodVisitor mw, Context context) {
        if (context.writeDirect) {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldNameDirect", "(Ljava/lang/String;)V");
        } else {
            mw.visitVarInsn(ALOAD, context.var("out"));
            mw.visitVarInsn(ALOAD, Context.fieldName);
            mw.visitInsn(ICONST_0);
            mw.visitMethodInsn(INVOKEVIRTUAL, SERIALIZE_WRITER, "writeFieldName", "(Ljava/lang/String;Z)V");
        }
    }

    private void seperator(MethodVisitor mw, Context context) {
        mw.visitVarInsn(BIPUSH, ',');
        mw.visitVarInsn(ISTORE, context.var("seperator"));
    }

    private void getListFieldItemSer(Context context, MethodVisitor mw, FieldInfo fieldInfo, Class<?> itemType) {
        Label notNull_ = new Label();
        mw.visitVarInsn(ALOAD, 0);
        mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_list_item_ser_",
                OBJECT_SERIALIZER_DESC);
        mw.visitJumpInsn(IFNONNULL, notNull_);

        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(itemType)));
        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "getObjectWriter",
                "(Ljava/lang/Class;)" + OBJECT_SERIALIZER_DESC);

        mw.visitFieldInsn(PUTFIELD, context.className, fieldInfo.name + "_asm_list_item_ser_",
                OBJECT_SERIALIZER_DESC);

        mw.visitLabel(notNull_);

        mw.visitVarInsn(ALOAD, 0);
        mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_list_item_ser_",
                OBJECT_SERIALIZER_DESC);
    }

    private void getFieldSer(Context context, MethodVisitor mw, FieldInfo fieldInfo) {
        Label notNull_ = new Label();
        mw.visitVarInsn(ALOAD, 0);
        mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_ser_", OBJECT_SERIALIZER_DESC);
        mw.visitJumpInsn(IFNONNULL, notNull_);

        mw.visitVarInsn(ALOAD, 0);
        mw.visitVarInsn(ALOAD, Context.SERIALIZER);
        mw.visitLdcInsn(com.alibaba.fastjson.asm.Type.getType(desc(fieldInfo.fieldClass)));
        mw.visitMethodInsn(INVOKEVIRTUAL, JSON_SERIALIZER, "getObjectWriter",
                "(Ljava/lang/Class;)" + OBJECT_SERIALIZER_DESC);

        mw.visitFieldInsn(PUTFIELD, context.className, fieldInfo.name + "_asm_ser_", OBJECT_SERIALIZER_DESC);

        mw.visitLabel(notNull_);

        mw.visitVarInsn(ALOAD, 0);
        mw.visitFieldInsn(GETFIELD, context.className, fieldInfo.name + "_asm_ser_", OBJECT_SERIALIZER_DESC);
    }
}
