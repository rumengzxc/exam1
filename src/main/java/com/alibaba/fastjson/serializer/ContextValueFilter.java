package com.alibaba.fastjson.serializer;

/**
 * @since 1.2.9
 * @author xwm
 */

public interface ContextValueFilter extends SerializeFilter {
    /**
     * process
     * @param context
     * @param object
     * @param name
     * @param value
     * @return
     */
    Object process(BeanContext context, Object object, String name, Object value);
}
