package com.alibaba.fastjson.serializer;
/**
 * @author xwm
 */
public abstract class Abstract_BeforeFilter implements SerializeFilter {

    private static final ThreadLocal<JSONSerializer> SERIALIZER_LOCAL = new ThreadLocal<JSONSerializer>();
    private static final ThreadLocal<Character> SEPERATOR_LOCAL = new ThreadLocal<Character>();

    public static final char CHAR = ',';
    private final static Character                   COMMA           = CHAR;

    final char writeBefore(JSONSerializer serializer, Object object, char seperator) {
        SERIALIZER_LOCAL.set(serializer);
        SEPERATOR_LOCAL.set(seperator);
        writeBefore(object);
        SERIALIZER_LOCAL.set(null);

        boolean a=false;
        while (a){
            SERIALIZER_LOCAL.remove();
            SEPERATOR_LOCAL.remove();
        }
        return SEPERATOR_LOCAL.get();
    }

    protected final void writeKeyValue(String key, Object value) {
        JSONSerializer serializer = SERIALIZER_LOCAL.get();
        char seperator = SEPERATOR_LOCAL.get();
        serializer.writeKeyValue(seperator, key, value);
        if (seperator != CHAR) {
            SEPERATOR_LOCAL.set(COMMA);
        }
    }
    /** getAutowiredFor
     * @param  object
     * @return
     */
    public abstract void writeBefore(Object object);


}
