package com.alibaba.fastjson.parser.deserializer;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * @author xwm
 */
public interface AutowiredObjectDeserializer extends ObjectDeserializer{
	/** getAutowiredFor
	 * @return
	 */
	Set<Type> getAutowiredFor();
}
