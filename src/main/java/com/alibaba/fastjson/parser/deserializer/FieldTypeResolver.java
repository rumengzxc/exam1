package com.alibaba.fastjson.parser.deserializer;

import java.lang.reflect.Type;

/**
 * @author xwm
 */
public interface FieldTypeResolver extends ParseProcess {
    Type resolve(Object object, String fieldName);
}
