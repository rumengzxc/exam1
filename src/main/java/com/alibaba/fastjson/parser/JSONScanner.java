/*
 * Copyright 1999-2017 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.IOUtils;

import java.math.BigDecimal;
import java.util.*;

//这个类，为了性能优化做了很多特别处理，一切都是为了性能！！！

/**
 * @author wenshao[szujobs@hotmail.com]
 */
public final class JSONScanner extends Abstract_JSONLexerBase {

    public static final int INT = 65279;
    public static final int INT1 = 2;
    public static final char CHAR = 'L';
    public static final char CHAR1 = 'S';
    public static final char CHAR2 = 'B';
    public static final char CHAR3 = 'F';
    public static final char CHAR4 = 'D';
    public static final int INT2 = 8;
    public static final int INT3 = 13;
    public static final char CHAR5 = '/';
    public static final char CHAR6 = 'a';
    public static final char CHAR7 = 't';
    public static final char CHAR8 = 'e';
    public static final char CHAR9 = '(';
    public static final char CHAR10 = ')';
    public static final char CHAR11 = '-';
    public static final char CHAR12 = 'T';
    public static final char CHAR13 = ':';
    public static final int INT4 = 16;
    public static final char CHAR14 = 'Z';
    public static final int INT5 = 17;
    public static final char CHAR15 = '0';
    public static final char CHAR16 = '9';
    public static final int INT6 = 9;
    public static final char CHAR17 = ' ';
    public static final String STRING = "\"@type\":\"";
    public static final char CHAR18 = ',';
    public static final char CHAR19 = ']';
    public static final char CHAR20 = 'n';
    public static final char CHAR21 = '.';
    public static final char CHAR22 = '年';
    public static final char CHAR23 = '년';
    public static final char CHAR24 = '月';
    public static final char CHAR25 = '월';
    public static final char CHAR26 = '日';
    public static final char CHAR27 = '일';
    public static final int INT7 = 10;
    public static final int INT8 = 15;
    public static final char CHAR28 = '"';
    public static final char CHAR29 = '+';
    public static final int INT9 = 6;
    public static final int INT10 = 3;
    public static final int INT11 = 11;
    public static final char CHAR30 = '1';
    public static final char CHAR31 = '4';
    public static final char CHAR32 = '3';
    public static final char CHAR33 = '2';
    public static final char CHAR34 = '}';
    public static final char CHAR35 = '\\';
    public static final char CHAR36 = '5';
    public static final char CHAR37 = '8';
    public static final int INT12 = 4;
    public static final char CHAR38 = '6';
    public static final char CHAR39 = '\n';
    public static final char CHAR40 = '[';
    public static final String STRING1 = "ull";
    public static final char CHAR41 = 'r';
    public static final char CHAR42 = 'u';
    public static final char CHAR43 = 'f';
    public static final char CHAR44 = 'l';
    public static final char CHAR45 = 's';
    public static final int INT13 = 18;
    public static final int INT14 = 65535;
    private final String text;
    private final int len;

    public JSONScanner(String input) {
        this(input, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONScanner(String input, int features) {
        super(features);

        text = input;
        len = text.length();
        bp = -1;

        next();
        if (ch == INT) {
            // utf-8 bom
            next();
        }
    }

    @Override
    public final char charAt(int index) {
        if (index >= len) {
            return EOI;
        }

        return text.charAt(index);
    }

    @Override
    public final char next() {
        int index = ++bp;
        return ch = (index >= this.len ?
                EOI
                : text.charAt(index));
    }

    public JSONScanner(char[] input, int inputLength) {
        this(input, inputLength, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONScanner(char[] input, int inputLength, int features) {
        this(new String(input, 0, inputLength), features);
    }

    @Override
    protected final void copyTo(int offset, int count, char[] dest) {
        text.getChars(offset, offset + count, dest, 0);
    }

    static boolean charArrayCompare(String src, int offset, char[] dest) {
        final int destLen = dest.length;
        if (destLen + offset > src.length()) {
            return false;
        }

        for (int i = 0; i < destLen; ++i) {
            if (dest[i] != src.charAt(offset + i)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public final boolean charArrayCompare(char[] chars) {
        return charArrayCompare(text, bp, chars);
    }

    @Override
    public final int indexOf(char ch, int startIndex) {
        return text.indexOf(ch, startIndex);
    }

    @Override
    public final String addSymbol(int offset, int len, int hash, final SymbolTable symbolTable) {
        return symbolTable.addSymbol(text, offset, len, hash);
    }

    @Override
    public byte[] bytesValue() {
        if (token == JSONToken.HEX) {
            int start = np + 1, len = sp;
            if (len % INT1 != 0) {
                throw new JSONException("illegal state. " + len);
            }

            byte[] bytes = new byte[len / INT1];
            for (int i = 0; i < bytes.length; ++i) {
                char c0 = text.charAt(start + i * INT1);
                char c1 = text.charAt(start + i * INT1 + 1);

                int b0 = c0 - (c0 <= 57 ? 48 : 55);
                int b1 = c1 - (c1 <= 57 ? 48 : 55);
                bytes[i] = (byte) ((b0 << INT12) | b1);
            }

            return bytes;
        }

        if (!hasSpecial) {
            return IOUtils.decodeBase64(text, np + 1, sp);
        } else {
            String escapedText = new String(sbuf, 0, sp);
            return IOUtils.decodeBase64(escapedText);
        }
    }

    /**
     * The value of a literal token, recorded as a string. For integers, leading 0x and 'l' suffixes are suppressed.
     */
    @Override
    public final String stringVal() {
        if (!hasSpecial) {
            return this.subString(np + 1, sp);
        } else {
            return new String(sbuf, 0, sp);
        }
    }

    @Override
    public final String subString(int offset, int count) {
        if (ASMUtils.IS_ANDROID) {
            if (count < sbuf.length) {
                text.getChars(offset, offset + count, sbuf, 0);
                return new String(sbuf, 0, count);
            } else {
                char[] chars = new char[count];
                text.getChars(offset, offset + count, chars, 0);
                return new String(chars);
            }
        } else {
            return text.substring(offset, offset + count);
        }
    }

    @Override
    public final char[] subChars(int offset, int count) {
        if (ASMUtils.IS_ANDROID && count < sbuf.length) {
            text.getChars(offset, offset + count, sbuf, 0);
            return sbuf;
        } else {
            char[] chars = new char[count];
            text.getChars(offset, offset + count, chars, 0);
            return chars;
        }
    }

    @Override
    public final String numberString() {
        char chLocal = charAt(np + sp - 1);

        int sp = this.sp;
        if (chLocal == CHAR || chLocal == CHAR1 || chLocal == CHAR2
                || chLocal == CHAR3 || chLocal == CHAR4) {
            sp--;
        }

        return this.subString(np, sp);
    }

    @Override
    public final BigDecimal decimalValue() {
        char chLocal = charAt(np + sp - 1);

        int sp = this.sp;
        if (chLocal == CHAR || chLocal == CHAR1 || chLocal == CHAR2 || chLocal == CHAR3 || chLocal == CHAR4) {
            sp--;
        }

        int offset = np, count = sp;
        if (count < sbuf.length) {
            text.getChars(offset, offset + count, sbuf, 0);
            return new BigDecimal(sbuf, 0, count);
        } else {
            char[] chars = new char[count];
            text.getChars(offset, offset + count, chars, 0);
            return new BigDecimal(chars);
        }
    }

    public boolean scanISO8601DateIfMatch() {
        return scanISO8601DateIfMatch(true);
    }

    public boolean scanISO8601DateIfMatch(boolean strict) {
        int rest = len - bp;
        return scanISO8601DateIfMatch(strict, rest);
    }

    private boolean scanISO8601DateIfMatch(boolean strict, int rest) {
        if (rest < INT2) {
            return false;
        }

        char c0 = charAt(bp);
        char c1 = charAt(bp + 1);
        char c2 = charAt(bp + INT1);
        char c3 = charAt(bp + INT10);
        char c4 = charAt(bp + INT12);
        char c5 = charAt(bp + 5);
        char c6 = charAt(bp + INT9);
        char c7 = charAt(bp + 7);

        if ((!strict) && rest > INT3) {
            char c_r0 = charAt(bp + rest - 1);
            char c_r1 = charAt(bp + rest - INT1);
            if (c0 == CHAR5 && c1 == CHAR4 && c2 == CHAR6
                    && c3 == CHAR7 && c4 == CHAR8 && c5 == CHAR9 && c_r0 == CHAR5
                    && c_r1 == CHAR10) {
                int plusIndex = -1;
                for (int i = INT9; i < rest; ++i) {
                    char c = charAt(bp + i);
                    if (c == CHAR29) {
                        plusIndex = i;
                    } else if (c < CHAR15 || c > CHAR16) {
                        break;
                    }
                }
                if (plusIndex == -1) {
                    return false;
                }
                int offset = bp + INT9;
                String numberText = this.subString(offset, bp + plusIndex - offset);
                long millis = Long.parseLong(numberText);

                calendar = Calendar.getInstance(timeZone, locale);
                calendar.setTimeInMillis(millis);

                token = JSONToken.LITERAL_ISO8601_DATE;
                return true;
            }
        }

        char c10;
        boolean res1 = rest == INT2
                || rest == 14
                || (rest == INT4 && ((c10 = charAt(bp + INT7)) == CHAR12 || c10 == CHAR17))
                || (rest == INT5 && charAt(bp + INT9) != CHAR11);
        if (res1) {
            if (strict) {
                return false;
            }

            char y0, y1, y2, y3, M0, M1, d0, d1;


            char c8 = charAt(bp + INT2);

            final boolean c_47 = c4 == CHAR11 && c7 == CHAR11;
            final boolean sperate16 = c_47 && rest == INT4;
            final boolean sperate17 = c_47 && rest == INT5;
            if (sperate17 || sperate16) {
                y0 = c0;
                y1 = c1;
                y2 = c2;
                y3 = c3;
                M0 = c5;
                M1 = c6;
                d0 = c8;
                d1 = charAt(bp + INT6);
            } else if (c4 == CHAR11 && c6 == CHAR11) {
                y0 = c0;
                y1 = c1;
                y2 = c2;
                y3 = c3;
                M0 = CHAR15;
                M1 = c5;
                d0 = CHAR15;
                d1 = c7;
            } else {
                y0 = c0;
                y1 = c1;
                y2 = c2;
                y3 = c3;
                M0 = c4;
                M1 = c5;
                d0 = c6;
                d1 = c7;
            }


            if (!checkDate(y0, y1, y2, y3, M0, M1, d0, d1)) {
                return false;
            }

            setCalendar(y0, y1, y2, y3, M0, M1, d0, d1);

            int hour, minute, seconds, millis;
            if (rest != INT2) {
                char c9 = charAt(bp + INT6);
                c10 = charAt(bp + INT7);
                char c11 = charAt(bp + INT11);
                char c12 = charAt(bp + 12);
                char c13 = charAt(bp + INT3);

                char h0, h1, m0, m1, s0, s1;

                boolean res2 = (sperate17 && c10 == CHAR12 && c13 == CHAR13 && charAt(bp + INT4) == CHAR14)
                        || (sperate16 && (c10 == CHAR17 || c10 == CHAR12) && c13 == CHAR13);
                if (res2) {
                    h0 = c11;
                    h1 = c12;
                    m0 = charAt(bp + 14);
                    m1 = charAt(bp + INT8);
                    s0 = CHAR15;
                    s1 = CHAR15;
                } else {
                    h0 = c8;
                    h1 = c9;
                    m0 = c10;
                    m1 = c11;
                    s0 = c12;
                    s1 = c13;
                }

                if (!checkTime(h0, h1, m0, m1, s0, s1)) {
                    return false;
                }

                if (rest == INT5 && !sperate17) {
                    char S0 = charAt(bp + 14);
                    char S1 = charAt(bp + INT8);
                    char S2 = charAt(bp + INT4);
                    if (S0 < CHAR15 || S0 > CHAR16) {
                        return false;
                    }
                    if (S1 < CHAR15 || S1 > CHAR16) {
                        return false;
                    }
                    if (S2 < CHAR15 || S2 > CHAR16) {
                        return false;
                    }

                    millis = (S0 - CHAR15) * 100 + (S1 - CHAR15) * INT7 + (S2 - CHAR15);
                } else {
                    millis = 0;
                }

                hour = (h0 - CHAR15) * INT7 + (h1 - CHAR15);
                minute = (m0 - CHAR15) * INT7 + (m1 - CHAR15);
                seconds = (s0 - CHAR15) * INT7 + (s1 - CHAR15);
            } else {
                hour = 0;
                minute = 0;
                seconds = 0;
                millis = 0;
            }

            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, seconds);
            calendar.set(Calendar.MILLISECOND, millis);

            token = JSONToken.LITERAL_ISO8601_DATE;
            return true;
        }

        if (rest < INT6) {
            return false;
        }

        char c8 = charAt(bp + INT2);
        char c9 = charAt(bp + INT6);

        int date_len = INT7;
        char y0, y1, y2, y3, M0, M1, d0, d1;
        // cn tw yyyy/mm/dd
        boolean res3 = (c4 == CHAR11 && c7 == CHAR11)
                || (c4 == CHAR5 && c7 == CHAR5);
        boolean res4 = (c2 == CHAR21 && c5 == CHAR21) || (c2 == CHAR11 && c5 == CHAR11);
        if (res3) {
            y0 = c0;
            y1 = c1;
            y2 = c2;
            y3 = c3;
            M0 = c5;
            M1 = c6;

            if (c9 == CHAR17) {
                d0 = CHAR15;
                d1 = c8;
                date_len = INT6;
            } else {
                d0 = c8;
                d1 = c9;
            }
        }
        // cn yyyy-m-dd
        else if ((c4 == CHAR11 && c6 == CHAR11)
        ) {
            y0 = c0;
            y1 = c1;
            y2 = c2;
            y3 = c3;
            M0 = CHAR15;
            M1 = c5;

            if (c8 == CHAR17) {
                d0 = CHAR15;
                d1 = c7;
                date_len = INT2;
            } else {
                d0 = c7;
                d1 = c8;
                date_len = INT6;
            }
        }
        // de dd.mm.yyyy
        // in dd-mm-yyyy
        else if (res4) {
            d0 = c0;
            d1 = c1;
            M0 = c3;
            M1 = c4;
            y0 = c6;
            y1 = c7;
            y2 = c8;
            y3 = c9;
        } else if (c8 == CHAR12) {
            y0 = c0;
            y1 = c1;
            y2 = c2;
            y3 = c3;
            M0 = c4;
            M1 = c5;
            d0 = c6;
            d1 = c7;
            date_len = INT2;
        } else {
            if (c4 == CHAR22 || c4 == CHAR23) {
                y0 = c0;
                y1 = c1;
                y2 = c2;
                y3 = c3;

                if (c7 == CHAR24 || c7 == CHAR25) {
                    M0 = c5;
                    M1 = c6;
                    if (c9 == CHAR26 || c9 == CHAR27) {
                        d0 = CHAR15;
                        d1 = c8;
                    } else if (charAt(bp + INT7) == CHAR26 || charAt(bp + INT7) == CHAR27) {
                        d0 = c8;
                        d1 = c9;
                        date_len = INT11;
                    } else {
                        return false;
                    }
                } else if (c6 == CHAR24 || c6 == CHAR25) {
                    M0 = CHAR15;
                    M1 = c5;
                    if (c8 == CHAR26 || c8 == CHAR27) {
                        d0 = CHAR15;
                        d1 = c7;
                    } else if (c9 == CHAR26 || c9 == CHAR27) {
                        d0 = c7;
                        d1 = c8;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (!checkDate(y0, y1, y2, y3, M0, M1, d0, d1)) {
            return false;
        }

        setCalendar(y0, y1, y2, y3, M0, M1, d0, d1);

        char t = charAt(bp + date_len);
        boolean res5 = t == CHAR12 || (t == CHAR17 && !strict);
        if (t == CHAR12 && rest == INT4 && date_len == INT2 && charAt(bp + INT8) == CHAR14) {
            char h0 = charAt(bp + date_len + 1);
            char h1 = charAt(bp + date_len + INT1);
            char m0 = charAt(bp + date_len + INT10);
            char m1 = charAt(bp + date_len + INT12);
            char s0 = charAt(bp + date_len + 5);
            char s1 = charAt(bp + date_len + INT9);

            if (!checkTime(h0, h1, m0, m1, s0, s1)) {
                return false;
            }

            setTime(h0, h1, m0, m1, s0, s1);
            calendar.set(Calendar.MILLISECOND, 0);

            if (calendar.getTimeZone().getRawOffset() != 0) {
                String[] timeZoneIDs = TimeZone.getAvailableIDs(0);
                if (timeZoneIDs.length > 0) {
                    TimeZone timeZone = TimeZone.getTimeZone(timeZoneIDs[0]);
                    calendar.setTimeZone(timeZone);
                }
            }

            token = JSONToken.LITERAL_ISO8601_DATE;
            return true;
        } else if (res5) {
            if (rest < date_len + INT6) {
                // "0000-00-00T00:00:00".length()
                return false;
            }
        } else if (t == CHAR28 || t == EOI || t == CHAR26 || t == CHAR27) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            ch = charAt(bp += date_len);

            token = JSONToken.LITERAL_ISO8601_DATE;
            return true;
        } else if (t == CHAR29 || t == CHAR11) {
            if (len == date_len + INT9) {
                if (charAt(bp + date_len + INT10) != CHAR13
                        || charAt(bp + date_len + INT12) != CHAR15
                        || charAt(bp + date_len + 5) != CHAR15) {
                    return false;
                }

                setTime(CHAR15, CHAR15, CHAR15, CHAR15, CHAR15, CHAR15);
                calendar.set(Calendar.MILLISECOND, 0);
                setTimeZone(t, charAt(bp + date_len + 1), charAt(bp + date_len + INT1));
                return true;
            }
            return false;
        } else {
            return false;
        }

        if (charAt(bp + date_len + INT10) != CHAR13) {
            return false;
        }
        if (charAt(bp + date_len + INT9) != CHAR13) {
            return false;
        }

        char h0 = charAt(bp + date_len + 1);
        char h1 = charAt(bp + date_len + INT1);
        char m0 = charAt(bp + date_len + INT12);
        char m1 = charAt(bp + date_len + 5);
        char s0 = charAt(bp + date_len + 7);
        char s1 = charAt(bp + date_len + INT2);

        if (!checkTime(h0, h1, m0, m1, s0, s1)) {
            return false;
        }

        setTime(h0, h1, m0, m1, s0, s1);

        char dot = charAt(bp + date_len + INT6);
        // 有可能没有毫秒区域，没有毫秒区域的时候下一个字符位置有可能是'Z'、'+'、'-'
        int millisLen = -1;
        int millis = 0;
        if (dot == CHAR21) {
            // 0000-00-00T00:00:00.000
            if (rest < date_len + INT11) {
                return false;
            }

            char S0 = charAt(bp + date_len + INT7);
            if (S0 < CHAR15 || S0 > CHAR16) {
                return false;
            }
            millis = S0 - CHAR15;
            millisLen = 1;

            if (rest > date_len + INT11) {
                char S1 = charAt(bp + date_len + INT11);
                if (S1 >= CHAR15 && S1 <= CHAR16) {
                    millis = millis * INT7 + (S1 - CHAR15);
                    millisLen = INT1;
                }
            }

            if (millisLen == INT1) {
                char S2 = charAt(bp + date_len + 12);
                if (S2 >= CHAR15 && S2 <= CHAR16) {
                    millis = millis * INT7 + (S2 - CHAR15);
                    millisLen = INT10;
                }
            }
        }
        calendar.set(Calendar.MILLISECOND, millis);

        int timzeZoneLength = 0;
        char timeZoneFlag = charAt(bp + date_len + INT7 + millisLen);

        if (timeZoneFlag == CHAR17) {
            millisLen++;
            timeZoneFlag = charAt(bp + date_len + INT7 + millisLen);
        }

        if (timeZoneFlag == CHAR29 || timeZoneFlag == CHAR11) {
            char t0 = charAt(bp + date_len + INT7 + millisLen + 1);
            if (t0 < CHAR15 || t0 > CHAR30) {
                return false;
            }

            char t1 = charAt(bp + date_len + INT7 + millisLen + INT1);
            if (t1 < CHAR15 || t1 > CHAR16) {
                return false;
            }

            char t2 = charAt(bp + date_len + INT7 + millisLen + INT10);
            char t3 = CHAR15, t4 = CHAR15;
            if (t2 == CHAR13) {
                // ThreeLetterISO8601TimeZone
                t3 = charAt(bp + date_len + INT7 + millisLen + INT12);
                t4 = charAt(bp + date_len + INT7 + millisLen + 5);

                if (t3 == CHAR31 && t4 == CHAR36) {
                    // handle some special timezones like xx:45

                    boolean r1 = t0 == CHAR30 && (t1 == CHAR33 || t1 == CHAR32);
                    boolean r2 = t0 == CHAR15 && (t1 == CHAR36 || t1 == CHAR37);
                    if (r1) {
                        // NZ-CHAT          => +12:45
                        // Pacific/Chatham  => +12:45
                        // NZ-CHAT          => +13:45 (DST)
                        // Pacific/Chatham  => +13:45 (DST)
                    } else if (r2) {
                        // Asia/Kathmandu   => +05:45
                        // Asia/Katmandu    => +05:45
                        // Australia/Eucla  => +08:45
                    } else {
                        return false;
                    }
                } else {
                    //handle normal timezone like xx:00 and xx:30
                    if (t3 != CHAR15 && t3 != CHAR32) {
                        return false;
                    }

                    if (t4 != CHAR15) {
                        return false;
                    }
                }

                timzeZoneLength = INT9;
            } else if (t2 == CHAR15) {
                // TwoLetterISO8601TimeZone
                t3 = charAt(bp + date_len + INT7 + millisLen + INT12);
                if (t3 != CHAR15 && t3 != CHAR32) {
                    return false;
                }
                timzeZoneLength = 5;
            } else if (t2 == CHAR32 && charAt(bp + date_len + INT7 + millisLen + INT12) == CHAR15) {
                t3 = CHAR32;
                t4 = CHAR15;
                timzeZoneLength = 5;
            } else if (t2 == CHAR31 && charAt(bp + date_len + INT7 + millisLen + INT12) == CHAR36) {
                t3 = CHAR31;
                t4 = CHAR36;
                timzeZoneLength = 5;
            } else {
                timzeZoneLength = INT10;
            }

            setTimeZone(timeZoneFlag, t0, t1, t3, t4);

        } else if (timeZoneFlag == CHAR14) {
            // UTC
            timzeZoneLength = 1;
            if (calendar.getTimeZone().getRawOffset() != 0) {
                String[] timeZoneIDs = TimeZone.getAvailableIDs(0);
                if (timeZoneIDs.length > 0) {
                    TimeZone timeZone = TimeZone.getTimeZone(timeZoneIDs[0]);
                    calendar.setTimeZone(timeZone);
                }
            }
        }

        char end = charAt(bp + (date_len + INT7 + millisLen + timzeZoneLength));
        if (end != EOI && end != CHAR28) {
            return false;
        }
        ch = charAt(bp += (date_len + INT7 + millisLen + timzeZoneLength));

        token = JSONToken.LITERAL_ISO8601_DATE;
        return true;
    }

    protected void setTime(char h0, char h1, char m0, char m1, char s0, char s1) {
        int hour = (h0 - CHAR15) * INT7 + (h1 - CHAR15);
        int minute = (m0 - CHAR15) * INT7 + (m1 - CHAR15);
        int seconds = (s0 - CHAR15) * INT7 + (s1 - CHAR15);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, seconds);
    }

    protected void setTimeZone(char timeZoneFlag, char t0, char t1) {
        setTimeZone(timeZoneFlag, t0, t1, CHAR15, CHAR15);
    }

    protected void setTimeZone(char timeZoneFlag, char t0, char t1, char t3, char t4) {
        int timeZoneOffset = ((t0 - CHAR15) * INT7 + (t1 - CHAR15)) * 3600 * 1000;

        timeZoneOffset += ((t3 - CHAR15) * INT7 + (t4 - CHAR15)) * 60 * 1000;

        if (timeZoneFlag == CHAR11) {
            timeZoneOffset = -timeZoneOffset;
        }

        if (calendar.getTimeZone().getRawOffset() != timeZoneOffset) {
            calendar.setTimeZone(new SimpleTimeZone(timeZoneOffset, Integer.toString(timeZoneOffset)));
        }
    }

    private boolean checkTime(char h0, char h1, char m0, char m1, char s0, char s1) {
        if (h0 == CHAR15) {
            if (h1 < CHAR15 || h1 > CHAR16) {
                return false;
            }
        } else if (h0 == CHAR30) {
            if (h1 < CHAR15 || h1 > CHAR16) {
                return false;
            }
        } else if (h0 == CHAR33) {
            if (h1 < CHAR15 || h1 > CHAR31) {
                return false;
            }
        } else {
            return false;
        }

        if (m0 >= CHAR15 && m0 <= CHAR36) {
            if (m1 < CHAR15 || m1 > CHAR16) {
                return false;
            }
        } else if (m0 == CHAR38) {
            if (m1 != CHAR15) {
                return false;
            }
        } else {
            return false;
        }

        if (s0 >= CHAR15 && s0 <= CHAR36) {
            if (s1 < CHAR15 || s1 > CHAR16) {
                return false;
            }
        } else if (s0 == CHAR38) {
            if (s1 != CHAR15) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private void setCalendar(char y0, char y1, char y2, char y3, char M0, char M1, char d0, char d1) {
        calendar = Calendar.getInstance(timeZone, locale);
        int year = (y0 - CHAR15) * 1000 + (y1 - CHAR15) * 100 + (y2 - CHAR15) * INT7 + (y3 - CHAR15);
        int month = (M0 - CHAR15) * INT7 + (M1 - CHAR15) - 1;
        int day = (d0 - CHAR15) * INT7 + (d1 - CHAR15);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
    }

    static boolean checkDate(char y0, char y1, char y2, char y3, char M0, char M1, int d0, int d1) {
        if (y0 < CHAR15 || y0 > CHAR16) {
            return false;
        }
        if (y1 < CHAR15 || y1 > CHAR16) {
            return false;
        }
        if (y2 < CHAR15 || y2 > CHAR16) {
            return false;
        }
        if (y3 < CHAR15 || y3 > CHAR16) {
            return false;
        }

        if (M0 == CHAR15) {
            if (M1 < CHAR30 || M1 > CHAR16) {
                return false;
            }
        } else if (M0 == CHAR30) {
            if (M1 != CHAR15 && M1 != CHAR30 && M1 != CHAR33) {
                return false;
            }
        } else {
            return false;
        }

        if (d0 == CHAR15) {
            if (d1 < CHAR30 || d1 > CHAR16) {
                return false;
            }
        } else if (d0 == CHAR30 || d0 == CHAR33) {
            if (d1 < CHAR15 || d1 > CHAR16) {
                return false;
            }
        } else if (d0 == CHAR32) {
            if (d1 != CHAR15 && d1 != CHAR30) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    @Override
    public boolean isEof() {
        return bp == len || (ch == EOI && bp + 1 >= len);
    }

    @Override
    public int scanFieldInt(char[] fieldName) {
        matchStat = UNKNOWN;
        int startPos = this.bp;
        char startChar = this.ch;

        if (!charArrayCompare(text, bp, fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int index = bp + fieldName.length;

        char ch = charAt(index++);

        final boolean quote = ch == CHAR28;

        if (quote) {
            ch = charAt(index++);
        }

        final boolean negative = ch == CHAR11;
        if (negative) {
            ch = charAt(index++);
        }

        int value;
        if (ch >= CHAR15 && ch <= CHAR16) {
            value = ch - CHAR15;
            for (; ; ) {
                ch = charAt(index++);
                if (ch >= CHAR15 && ch <= CHAR16) {
                    int value_10 = value * INT7;
                    if (value_10 < value) {
                        matchStat = NOT_MATCH;
                        return 0;
                    }

                    value = value_10 + (ch - CHAR15);
                } else if (ch == CHAR21) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    break;
                }
            }

            if (value < 0) {
                matchStat = NOT_MATCH;
                return 0;
            }

            if (quote) {
                if (ch != CHAR28) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    ch = charAt(index++);
                }
            }

            for (; ; ) {
                if (ch == CHAR18 || ch == CHAR34) {
                    bp = index - 1;
                    break;
                } else if (isWhitespace(ch)) {
                    ch = charAt(index++);
                    continue;
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (ch == CHAR18) {
            this.ch = charAt(++bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return negative ? -value : value;
        }

        if (ch == CHAR34) {
            bp = index - 1;
            ch = charAt(++bp);
            for (; ; ) {
                if (ch == CHAR18) {
                    token = JSONToken.COMMA;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == CHAR19) {
                    token = JSONToken.RBRACKET;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == CHAR34) {
                    token = JSONToken.RBRACE;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == EOI) {
                    token = JSONToken.EOF;
                    break;
                } else if (isWhitespace(ch)) {
                    ch = charAt(++bp);
                    continue;
                } else {
                    this.bp = startPos;
                    this.ch = startChar;
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }
            matchStat = END;
        }

        return negative ? -value : value;
    }

    @Override
    public String scanFieldString(char[] fieldName) {
        matchStat = UNKNOWN;
        int startPos = this.bp;
        char startChar = this.ch;


        for (; ; ) {
            if (!charArrayCompare(text, bp, fieldName)) {
                if (isWhitespace(ch)) {
                    next();

                    while (isWhitespace(ch)) {
                        next();
                    }
                    continue;
                }
                matchStat = NOT_MATCH_NAME;
                return stringDefaultValue();
            } else {
                break;
            }
        }

        int index = bp + fieldName.length;

        int spaceCount = 0;
        char ch = charAt(index++);
        if (ch != CHAR28) {
            while (isWhitespace(ch)) {
                spaceCount++;
                ch = charAt(index++);
            }

            if (ch != CHAR28) {
                matchStat = NOT_MATCH;

                return stringDefaultValue();
            }
        }

        final String strVal;
        {
            int startIndex = index;
            int endIndex = indexOf(CHAR28, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            String stringVal = subString(startIndex, endIndex - startIndex);
            if (stringVal.indexOf(CHAR35) != -1) {
                for (; ; ) {
                    int slashCount = 0;
                    for (int i = endIndex - 1; i >= 0; --i) {
                        if (charAt(i) == CHAR35) {
                            slashCount++;
                        } else {
                            break;
                        }
                    }
                    if (slashCount % INT1 == 0) {
                        break;
                    }
                    endIndex = indexOf(CHAR28, endIndex + 1);
                }

                int chars_len = endIndex - (bp + fieldName.length + 1 + spaceCount);
                char[] chars = subChars(bp + fieldName.length + 1 + spaceCount, chars_len);

                stringVal = readString(chars, chars_len);
            }

            ch = charAt(endIndex + 1);

            for (; ; ) {
                if (ch == CHAR18 || ch == CHAR34) {
                    bp = endIndex + 1;
                    this.ch = ch;
                    strVal = stringVal;
                    break;
                } else if (isWhitespace(ch)) {
                    endIndex++;
                    ch = charAt(endIndex + 1);
                } else {
                    matchStat = NOT_MATCH;

                    return stringDefaultValue();
                }
            }
        }

        if (ch == CHAR18) {
            this.ch = charAt(++bp);
            matchStat = VALUE;
            return strVal;
        } else {
            //condition ch == '}' is always 'true'
            ch = charAt(++bp);
            if (ch == CHAR18) {
                token = JSONToken.COMMA;
                this.ch = charAt(++bp);
            } else if (ch == CHAR19) {
                token = JSONToken.RBRACKET;
                this.ch = charAt(++bp);
            } else if (ch == CHAR34) {
                token = JSONToken.RBRACE;
                this.ch = charAt(++bp);
            } else if (ch == EOI) {
                token = JSONToken.EOF;
            } else {
                this.bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return stringDefaultValue();
            }
            matchStat = END;
        }
        return strVal;
    }

    @Override
    public java.util.Date scanFieldDate(char[] fieldName) {
        matchStat = UNKNOWN;
        int startPos = this.bp;
        char startChar = this.ch;

        if (!charArrayCompare(text, bp, fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int index = bp + fieldName.length;

        char ch = charAt(index++);

        final java.util.Date dateVal;
        boolean rr1 = ch == CHAR11 || (ch >= CHAR15 && ch <= CHAR16);
        if (ch == CHAR28) {
            int startIndex = index;
            int endIndex = indexOf(CHAR28, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            int rest = endIndex - startIndex;
            bp = index;
            if (scanISO8601DateIfMatch(false, rest)) {
                dateVal = calendar.getTime();
            } else {
                bp = startPos;
                matchStat = NOT_MATCH;
                return null;
            }
            ch = charAt(endIndex + 1);
            bp = startPos;

            for (; ; ) {
                if (ch == CHAR18 || ch == CHAR34) {
                    bp = endIndex + 1;
                    this.ch = ch;
                    break;
                } else if (isWhitespace(ch)) {
                    endIndex++;
                    ch = charAt(endIndex + 1);
                } else {
                    matchStat = NOT_MATCH;

                    return null;
                }
            }
        } else if (rr1) {
            long millis = 0;

            boolean negative = false;
            if (ch == CHAR11) {
                ch = charAt(index++);
                negative = true;
            }

            if (ch >= CHAR15 && ch <= CHAR16) {
                millis = ch - CHAR15;
                for (; ; ) {
                    ch = charAt(index++);
                    if (ch >= CHAR15 && ch <= CHAR16) {
                        millis = millis * INT7 + (ch - CHAR15);
                    } else {
                        if (ch == CHAR18 || ch == CHAR34) {
                            bp = index - 1;
                        }
                        break;
                    }
                }
            }

            if (millis < 0) {
                matchStat = NOT_MATCH;
                return null;
            }

            if (negative) {
                millis = -millis;
            }

            dateVal = new java.util.Date(millis);
        } else {
            matchStat = NOT_MATCH;

            return null;
        }

        if (ch == CHAR18) {
            this.ch = charAt(++bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return dateVal;
        } else {
            //condition ch == '}' is always 'true'
            ch = charAt(++bp);
            if (ch == CHAR18) {
                token = JSONToken.COMMA;
                this.ch = charAt(++bp);
            } else if (ch == CHAR19) {
                token = JSONToken.RBRACKET;
                this.ch = charAt(++bp);
            } else if (ch == CHAR34) {
                token = JSONToken.RBRACE;
                this.ch = charAt(++bp);
            } else if (ch == EOI) {
                token = JSONToken.EOF;
            } else {
                this.bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        }
        return dateVal;
    }

    @Override
    public long scanFieldSymbol(char[] fieldName) {
        matchStat = UNKNOWN;

        for (; ; ) {
            if (!charArrayCompare(text, bp, fieldName)) {
                if (isWhitespace(ch)) {
                    next();

                    while (isWhitespace(ch)) {
                        next();
                    }
                    continue;
                }
                matchStat = NOT_MATCH_NAME;
                return 0;
            } else {
                break;
            }
        }

        int index = bp + fieldName.length;
        int spaceCount = 0;
        char ch = charAt(index++);
        if (ch != CHAR28) {
            while (isWhitespace(ch)) {
                ch = charAt(index++);
                spaceCount++;
            }

            if (ch != CHAR28) {
                matchStat = NOT_MATCH;

                return 0;
            }
        }

        long hash = 0xcbf29ce484222325L;
        for (; ; ) {
            ch = charAt(index++);
            if (ch == '\"') {
                bp = index;
                this.ch = ch = charAt(bp);
                break;
            } else if (index > len) {
                matchStat = NOT_MATCH;
                return 0;
            }

            hash ^= ch;
            hash *= 0x100000001b3L;
        }

        for (; ; ) {
            if (ch == CHAR18) {
                this.ch = charAt(++bp);
                matchStat = VALUE;
                return hash;
            } else if (ch == CHAR34) {
                next();
                skipWhitespace();
                ch = getCurrent();
                if (ch == CHAR18) {
                    token = JSONToken.COMMA;
                    this.ch = charAt(++bp);
                } else if (ch == CHAR19) {
                    token = JSONToken.RBRACKET;
                    this.ch = charAt(++bp);
                } else if (ch == CHAR34) {
                    token = JSONToken.RBRACE;
                    this.ch = charAt(++bp);
                } else if (ch == EOI) {
                    token = JSONToken.EOF;
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
                matchStat = END;
                break;
            } else if (isWhitespace(ch)) {
                ch = charAt(++bp);
                continue;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
        }

        return hash;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<String> scanFieldStringArray(char[] fieldName, Class<?> type) {
        matchStat = UNKNOWN;

        while (ch == CHAR39 || ch == CHAR17) {
            int index = ++bp;
            ch = (index >= this.len ?
                    EOI
                    : text.charAt(index));
        }

        if (!charArrayCompare(text, bp, fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        Collection<String> list = newCollectionByType(type);

        int startPos = this.bp;
        char startChar = this.ch;

        int index = bp + fieldName.length;

        char ch = charAt(index++);

        if (ch == CHAR40) {
            ch = charAt(index++);

            for (; ; ) {
                if (ch == CHAR28) {
                    int startIndex = index;
                    int endIndex = indexOf(CHAR28, startIndex);
                    if (endIndex == -1) {
                        throw new JSONException("unclosed str");
                    }

                    String stringVal = subString(startIndex, endIndex - startIndex);
                    if (stringVal.indexOf(CHAR35) != -1) {
                        for (; ; ) {
                            int slashCount = 0;
                            for (int i = endIndex - 1; i >= 0; --i) {
                                if (charAt(i) == CHAR35) {
                                    slashCount++;
                                } else {
                                    break;
                                }
                            }
                            if (slashCount % INT1 == 0) {
                                break;
                            }
                            endIndex = indexOf(CHAR28, endIndex + 1);
                        }

                        int chars_len = endIndex - startIndex;
                        char[] chars = subChars(startIndex, chars_len);

                        stringVal = readString(chars, chars_len);
                    }

                    index = endIndex + 1;
                    ch = charAt(index++);

                    list.add(stringVal);
                } else if (ch == CHAR20 && text.startsWith(STRING1, index)) {
                    index += INT10;
                    ch = charAt(index++);
                    list.add(null);
                } else if (ch == CHAR19 && list.size() == 0) {
                    ch = charAt(index++);
                    break;
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }

                if (ch == CHAR18) {
                    ch = charAt(index++);
                    continue;
                }

                if (ch == CHAR19) {
                    ch = charAt(index++);
                    while (isWhitespace(ch)) {
                        ch = charAt(index++);
                    }
                    break;
                }

                matchStat = NOT_MATCH;
                return null;
            }
        } else if (text.startsWith(STRING1, index)) {
            index += INT10;
            ch = charAt(index++);
            list = null;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        bp = index;
        if (ch == CHAR18) {
            this.ch = charAt(bp);
            matchStat = VALUE;
            return list;
        } else if (ch == CHAR34) {
            ch = charAt(bp);
            for (; ; ) {
                if (ch == CHAR18) {
                    token = JSONToken.COMMA;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == CHAR19) {
                    token = JSONToken.RBRACKET;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == CHAR34) {
                    token = JSONToken.RBRACE;
                    this.ch = charAt(++bp);
                    break;
                } else if (ch == EOI) {
                    token = JSONToken.EOF;
                    this.ch = ch;
                    break;
                } else {
                    boolean space = false;
                    while (isWhitespace(ch)) {
                        ch = charAt(index++);
                        bp = index;
                        space = true;
                    }
                    if (space) {
                        continue;
                    }

                    matchStat = NOT_MATCH;
                    return null;
                }
            }

            matchStat = END;
        } else {
            this.ch = startChar;
            bp = startPos;
            matchStat = NOT_MATCH;
            return null;
        }

        return list;
    }

    @Override
    public long scanFieldLong(char[] fieldName) {
        matchStat = UNKNOWN;
        int startPos = this.bp;
        char startChar = this.ch;

        if (!charArrayCompare(text, bp, fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int index = bp + fieldName.length;

        char ch = charAt(index++);

        final boolean quote = ch == CHAR28;
        if (quote) {
            ch = charAt(index++);
        }

        boolean negative = false;
        if (ch == CHAR11) {
            ch = charAt(index++);
            negative = true;
        }

        long value;
        if (ch >= CHAR15 && ch <= CHAR16) {
            value = ch - CHAR15;
            for (; ; ) {
                ch = charAt(index++);
                if (ch >= CHAR15 && ch <= CHAR16) {
                    value = value * INT7 + (ch - CHAR15);
                } else if (ch == CHAR21) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    if (quote) {
                        if (ch != CHAR28) {
                            matchStat = NOT_MATCH;
                            return 0;
                        } else {
                            ch = charAt(index++);
                        }
                    }

                    if (ch == CHAR18 || ch == CHAR34) {
                        bp = index - 1;
                    }
                    break;
                }
            }

            boolean valid = value >= 0 || (value == -9223372036854775808L && negative);
            if (!valid) {
                this.bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return 0;
            }
        } else {
            this.bp = startPos;
            this.ch = startChar;
            matchStat = NOT_MATCH;
            return 0;
        }

        for (; ; ) {
            if (ch == CHAR18) {
                this.ch = charAt(++bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return negative ? -value : value;
            } else if (ch == CHAR34) {
                ch = charAt(++bp);
                for (; ; ) {
                    if (ch == CHAR18) {
                        token = JSONToken.COMMA;
                        this.ch = charAt(++bp);
                        break;
                    } else if (ch == CHAR19) {
                        token = JSONToken.RBRACKET;
                        this.ch = charAt(++bp);
                        break;
                    } else if (ch == CHAR34) {
                        token = JSONToken.RBRACE;
                        this.ch = charAt(++bp);
                        break;
                    } else if (ch == EOI) {
                        token = JSONToken.EOF;
                        break;
                    } else if (isWhitespace(ch)) {
                        ch = charAt(++bp);
                    } else {
                        this.bp = startPos;
                        this.ch = startChar;
                        matchStat = NOT_MATCH;
                        return 0;
                    }
                }
                matchStat = END;
                break;
            } else if (isWhitespace(ch)) {
                bp = index;
                ch = charAt(index++);
                continue;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
        }

        return negative ? -value : value;
    }

    @Override
    public boolean scanFieldBoolean(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(text, bp, fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return false;
        }

        int startPos = bp;
        int index = bp + fieldName.length;

        char ch = charAt(index++);

        final boolean quote = ch == CHAR28;
        if (quote) {
            ch = charAt(index++);
        }

        boolean value;
        if (ch == CHAR7) {
            if (charAt(index++) != CHAR41) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(index++) != CHAR42) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(index++) != CHAR8) {
                matchStat = NOT_MATCH;
                return false;
            }

            if (quote && charAt(index++) != CHAR28) {
                matchStat = NOT_MATCH;
                return false;
            }

            bp = index;
            ch = charAt(bp);
            value = true;
        } else if (ch == CHAR43) {
            if (charAt(index++) != CHAR6) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(index++) != CHAR44) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(index++) != CHAR45) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(index++) != CHAR8) {
                matchStat = NOT_MATCH;
                return false;
            }

            if (quote && charAt(index++) != CHAR28) {
                matchStat = NOT_MATCH;
                return false;
            }

            bp = index;
            ch = charAt(bp);
            value = false;
        } else if (ch == CHAR30) {
            if (quote && charAt(index++) != CHAR28) {
                matchStat = NOT_MATCH;
                return false;
            }

            bp = index;
            ch = charAt(bp);
            value = true;
        } else if (ch == CHAR15) {
            if (quote && charAt(index++) != CHAR28) {
                matchStat = NOT_MATCH;
                return false;
            }

            bp = index;
            ch = charAt(bp);
            value = false;
        } else {
            matchStat = NOT_MATCH;
            return false;
        }

        for (; ; ) {
            if (ch == CHAR18) {
                this.ch = charAt(++bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                break;
            } else if (ch == CHAR34) {
                ch = charAt(++bp);
                for (; ; ) {
                    if (ch == CHAR18) {
                        token = JSONToken.COMMA;
                        this.ch = charAt(++bp);
                    } else if (ch == CHAR19) {
                        token = JSONToken.RBRACKET;
                        this.ch = charAt(++bp);
                    } else if (ch == CHAR34) {
                        token = JSONToken.RBRACE;
                        this.ch = charAt(++bp);
                    } else if (ch == EOI) {
                        token = JSONToken.EOF;
                    } else if (isWhitespace(ch)) {
                        ch = charAt(++bp);
                        continue;
                    } else {
                        matchStat = NOT_MATCH;
                        return false;
                    }
                    break;
                }
                matchStat = END;
                break;
            } else if (isWhitespace(ch)) {
                ch = charAt(++bp);
            } else {
                bp = startPos;
                ch = charAt(bp);
                matchStat = NOT_MATCH;
                return false;
            }
        }

        return value;
    }

    @Override
    public final int scanInt(char expectNext) {
        matchStat = UNKNOWN;

        final int mark = bp;
        int offset = bp;
        char chLocal = charAt(offset++);

        while (isWhitespace(chLocal)) {
            chLocal = charAt(offset++);
        }

        final boolean quote = chLocal == CHAR28;

        if (quote) {
            chLocal = charAt(offset++);
        }

        final boolean negative = chLocal == CHAR11;
        if (negative) {
            chLocal = charAt(offset++);
        }

        int value;
        if (chLocal >= CHAR15 && chLocal <= CHAR16) {
            value = chLocal - CHAR15;
            for (; ; ) {
                chLocal = charAt(offset++);
                if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                    int value_10 = value * INT7;
                    if (value_10 < value) {
                        throw new JSONException("parseInt error : "
                                + subString(mark, offset - 1));
                    }
                    value = value_10 + (chLocal - CHAR15);
                } else if (chLocal == CHAR21) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    if (quote) {
                        if (chLocal != CHAR28) {
                            matchStat = NOT_MATCH;
                            return 0;
                        } else {
                            chLocal = charAt(offset++);
                        }
                    }
                    break;
                }
            }
            if (value < 0) {
                matchStat = NOT_MATCH;
                return 0;
            }
        } else if (chLocal == CHAR20
                && charAt(offset++) == CHAR42
                && charAt(offset++) == CHAR44
                && charAt(offset++) == CHAR44) {
            matchStat = VALUE_NULL;
            value = 0;
            chLocal = charAt(offset++);

            if (quote && chLocal == CHAR28) {
                chLocal = charAt(offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR18) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR19) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        for (; ; ) {
            if (chLocal == expectNext) {
                bp = offset;
                this.ch = charAt(bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return negative ? -value : value;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(offset++);
                    continue;
                }
                matchStat = NOT_MATCH;
                return negative ? -value : value;
            }
        }
    }

    @Override
    public double scanDouble(char seperator) {
        matchStat = UNKNOWN;

        int offset = bp;
        char chLocal = charAt(offset++);
        final boolean quote = chLocal == CHAR28;
        if (quote) {
            chLocal = charAt(offset++);
        }

        boolean negative = chLocal == CHAR11;
        if (negative) {
            chLocal = charAt(offset++);
        }

        double value;
        if (chLocal >= CHAR15 && chLocal <= CHAR16) {
            long intVal = chLocal - CHAR15;
            for (; ; ) {
                chLocal = charAt(offset++);
                if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                    intVal = intVal * INT7 + (chLocal - CHAR15);
                    continue;
                } else {
                    break;
                }
            }

            long power = 1;
            boolean small = (chLocal == CHAR21);
            if (small) {
                chLocal = charAt(offset++);
                if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                    intVal = intVal * INT7 + (chLocal - CHAR15);
                    power = INT7;
                    for (; ; ) {
                        chLocal = charAt(offset++);
                        if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                            intVal = intVal * INT7 + (chLocal - CHAR15);
                            power *= INT7;
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }

            boolean exp = chLocal == CHAR8 || chLocal == 'E';
            if (exp) {
                chLocal = charAt(offset++);
                if (chLocal == CHAR29 || chLocal == CHAR11) {
                    chLocal = charAt(offset++);
                }
                for (; ; ) {
                    if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                        chLocal = charAt(offset++);
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR28) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    chLocal = charAt(offset++);
                }
                start = bp + 1;
                count = offset - start - INT1;
            } else {
                start = bp;
                count = offset - start - 1;
            }

            if (!exp && count < INT13) {
                value = ((double) intVal) / power;
                if (negative) {
                    value = -value;
                }
            } else {
                String text = this.subString(start, count);
                value = Double.parseDouble(text);
            }
        } else if (chLocal == CHAR20
                && charAt(offset++) == CHAR42
                && charAt(offset++) == CHAR44
                && charAt(offset++) == CHAR44) {
            matchStat = VALUE_NULL;
            value = 0;
            chLocal = charAt(offset++);

            if (quote && chLocal == CHAR28) {
                chLocal = charAt(offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR18) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR19) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == seperator) {
            bp = offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        } else {
            matchStat = NOT_MATCH;
            return value;
        }
    }

    @Override
    public long scanLong(char seperator) {
        matchStat = UNKNOWN;

        int offset = bp;
        char chLocal = charAt(offset++);
        final boolean quote = chLocal == CHAR28;

        if (quote) {
            chLocal = charAt(offset++);
        }

        final boolean negative = chLocal == CHAR11;
        if (negative) {
            chLocal = charAt(offset++);
        }

        long value;
        if (chLocal >= CHAR15 && chLocal <= CHAR16) {
            value = chLocal - CHAR15;
            for (; ; ) {
                chLocal = charAt(offset++);
                if (chLocal >= CHAR15 && chLocal <= CHAR16) {
                    value = value * INT7 + (chLocal - CHAR15);
                } else if (chLocal == CHAR21) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    if (quote) {
                        if (chLocal != CHAR28) {
                            matchStat = NOT_MATCH;
                            return 0;
                        } else {
                            chLocal = charAt(offset++);
                        }
                    }
                    break;
                }
            }

            boolean valid = value >= 0 || (value == -9223372036854775808L && negative);
            if (!valid) {
                matchStat = NOT_MATCH;
                return 0;
            }
        } else if (chLocal == CHAR20
                && charAt(offset++) == CHAR42
                && charAt(offset++) == CHAR44
                && charAt(offset++) == CHAR44) {
            matchStat = VALUE_NULL;
            value = 0;
            chLocal = charAt(offset++);

            if (quote && chLocal == CHAR28) {
                chLocal = charAt(offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR18) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR19) {
                    bp = offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        for (; ; ) {
            if (chLocal == seperator) {
                bp = offset;
                this.ch = charAt(bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return negative ? -value : value;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(offset++);
                    continue;
                }

                matchStat = NOT_MATCH;
                return value;
            }
        }
    }

    @Override
    public java.util.Date scanDate(char seperator) {
        matchStat = UNKNOWN;
        int startPos = this.bp;
        char startChar = this.ch;

        int index = bp;

        char ch = charAt(index++);

        final java.util.Date dateVal;
        boolean res1 = ch == CHAR11 || (ch >= CHAR15 && ch <= CHAR16);
        if (ch == CHAR28) {
            int startIndex = index;
            int endIndex = indexOf(CHAR28, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            int rest = endIndex - startIndex;
            bp = index;
            if (scanISO8601DateIfMatch(false, rest)) {
                dateVal = calendar.getTime();
            } else {
                bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return null;
            }
            ch = charAt(endIndex + 1);
            bp = startPos;

            for (; ; ) {
                if (ch == CHAR18 || ch == CHAR19) {
                    bp = endIndex + 1;
                    this.ch = ch;
                    break;
                } else if (isWhitespace(ch)) {
                    endIndex++;
                    ch = charAt(endIndex + 1);
                } else {
                    this.bp = startPos;
                    this.ch = startChar;
                    matchStat = NOT_MATCH;

                    return null;
                }
            }
        } else if (res1) {
            long millis = 0;

            boolean negative = false;
            if (ch == CHAR11) {
                ch = charAt(index++);
                negative = true;
            }

            if (ch >= CHAR15 && ch <= CHAR16) {
                millis = ch - CHAR15;
                for (; ; ) {
                    ch = charAt(index++);
                    if (ch >= CHAR15 && ch <= CHAR16) {
                        millis = millis * INT7 + (ch - CHAR15);
                    } else {
                        if (ch == CHAR18 || ch == CHAR19) {
                            bp = index - 1;
                        }
                        break;
                    }
                }
            }

            if (millis < 0) {
                this.bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return null;
            }

            if (negative) {
                millis = -millis;
            }

            dateVal = new java.util.Date(millis);
        } else if (ch == CHAR20
                && charAt(index++) == CHAR42
                && charAt(index++) == CHAR44
                && charAt(index++) == CHAR44) {
            dateVal = null;
            ch = charAt(index);
            bp = index;
        } else {
            this.bp = startPos;
            this.ch = startChar;
            matchStat = NOT_MATCH;

            return null;
        }

        if (ch == CHAR18) {
            this.ch = charAt(++bp);
            matchStat = VALUE;
            return dateVal;
        } else {
            //condition ch == '}' is always 'true'
            ch = charAt(++bp);
            if (ch == CHAR18) {
                token = JSONToken.COMMA;
                this.ch = charAt(++bp);
            } else if (ch == CHAR19) {
                token = JSONToken.RBRACKET;
                this.ch = charAt(++bp);
            } else if (ch == CHAR34) {
                token = JSONToken.RBRACE;
                this.ch = charAt(++bp);
            } else if (ch == EOI) {
                this.ch = EOI;
                token = JSONToken.EOF;
            } else {
                this.bp = startPos;
                this.ch = startChar;
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        }
        return dateVal;
    }

    @Override
    protected final void arrayCopy(int srcPos, char[] dest, int destPos, int length) {
        text.getChars(srcPos, srcPos + length, dest, destPos);
    }

    @Override
    public String info() {
        StringBuilder buf = new StringBuilder();


        int line = 1;
        int column = 1;
        for (int i = 0; i < bp; ++i, column++) {
            char ch = text.charAt(i);
            if (ch == CHAR39) {
                column = 1;
                line++;
            }
        }

        buf.append("pos ").append(bp)
                .append(", line ").append(line)
                .append(", column ").append(column);

        if (text.length() < INT14) {
            buf.append(text);
        } else {
            buf.append(text.substring(0, INT14));
        }

        return buf.toString();
    }

    /**for hsf support */
    @Override
    public String[] scanFieldStringArray(char[] fieldName, int argTypesCount, SymbolTable typeSymbolTable) {
        int startPos = bp;
        char starChar = ch;

        while (isWhitespace(ch)) {
            next();
        }

        int offset;
        char ch;
        if (fieldName != null) {
            matchStat = UNKNOWN;
            if (!charArrayCompare(fieldName)) {
                matchStat = NOT_MATCH_NAME;
                return null;
            }

            offset = bp + fieldName.length;
            ch = text.charAt(offset++);
            while (isWhitespace(ch)) {
                ch = text.charAt(offset++);
            }

            if (ch == CHAR13) {
                ch = text.charAt(offset++);
            } else {
                matchStat = NOT_MATCH;
                return null;
            }

            while (isWhitespace(ch)) {
                ch = text.charAt(offset++);
            }
        } else {
            offset = bp + 1;
            ch = this.ch;
        }

        if (ch == CHAR40) {
            bp = offset;
            this.ch = text.charAt(bp);
        } else if (ch == CHAR20 && text.startsWith(STRING1, bp + 1)) {
            bp += INT12;
            this.ch = text.charAt(bp);
            return null;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        String[] types = argTypesCount >= 0 ? new String[argTypesCount] : new String[INT12];
        int typeIndex = 0;
        for (; ; ) {
            while (isWhitespace(this.ch)) {
                next();
            }

            if (this.ch != '\"') {
                this.bp = startPos;
                this.ch = starChar;
                matchStat = NOT_MATCH;
                return null;
            }

            String type = scanSymbol(typeSymbolTable, CHAR28);
            if (typeIndex == types.length) {
                int newCapacity = types.length + (types.length >> 1) + 1;
                String[] array = new String[newCapacity];
                System.arraycopy(types, 0, array, 0, types.length);
                types = array;
            }
            types[typeIndex++] = type;
            while (isWhitespace(this.ch)) {
                next();
            }
            if (this.ch == CHAR18) {
                next();
                continue;
            }
            break;
        }
        if (types.length != typeIndex) {
            String[] array = new String[typeIndex];
            System.arraycopy(types, 0, array, 0, typeIndex);
            types = array;
        }

        while (isWhitespace(this.ch)) {
            next();
        }

        if (this.ch == CHAR19) {
            next();
        } else {
            this.bp = startPos;
            this.ch = starChar;
            matchStat = NOT_MATCH;
            return null;
        }

        return types;
    }

    @Override
    public boolean matchField2(char[] fieldName) {
        while (isWhitespace(ch)) {
            next();
        }

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return false;
        }

        int offset = bp + fieldName.length;
        char ch = text.charAt(offset++);
        while (isWhitespace(ch)) {
            ch = text.charAt(offset++);
        }

        if (ch == CHAR13) {
            this.bp = offset;
            this.ch = charAt(bp);
            return true;
        } else {
            matchStat = NOT_MATCH_NAME;
            return false;
        }
    }

    @Override
    public final void skipObject() {
        skipObject(false);
    }

    @Override
    public final void skipObject(boolean valid) {
        boolean quote = false;
        int braceCnt = 0;
        int i = bp;
        for (; i < text.length(); ++i) {
            final char ch = text.charAt(i);
            if (ch == CHAR35) {
                if (i < len - 1) {
                    ++i;
                    continue;
                } else {
                    this.ch = ch;
                    this.bp = i;
                    throw new JSONException("illegal str, " + info());
                }
            } else if (ch == CHAR28) {
                quote = !quote;
            } else if (ch == '{') {
                if (quote) {
                    continue;
                }
                braceCnt++;
            } else if (ch == CHAR34) {
                if (quote) {
                    continue;
                } else {
                    braceCnt--;
                }
                if (braceCnt == -1) {
                    this.bp = i + 1;
                    if (this.bp == text.length()) {
                        this.ch = EOI;
                        this.token = JSONToken.EOF;
                        return;
                    }
                    this.ch = text.charAt(this.bp);
                    if (this.ch == CHAR18) {
                        token = JSONToken.COMMA;
                        int index = ++bp;
                        this.ch = (index >= text.length()
                                ? EOI
                                : text.charAt(index));
                        return;
                    } else if (this.ch == CHAR34) {
                        token = JSONToken.RBRACE;
                        next();
                        return;
                    } else if (this.ch == CHAR19) {
                        token = JSONToken.RBRACKET;
                        next();
                        return;
                    } else {
                        nextToken(JSONToken.COMMA);
                    }
                    return;
                }
            }
        }

        for (int j = 0; j < bp; j++) {
            if (j < text.length() && text.charAt(j) == CHAR17) {
                i++;
            }
        }

        if (i == text.length()) {
            throw new JSONException("illegal str, " + info());
        }
    }

    @Override
    public final void skipArray() {
        skipArray(false);
    }

    public final void skipArray(boolean valid) {
        boolean quote = false;
        int bracketCnt = 0;
        int i = bp;
        for (; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if (ch == CHAR35) {
                if (i < len - 1) {
                    ++i;
                    continue;
                } else {
                    this.ch = ch;
                    this.bp = i;
                    throw new JSONException("illegal str, " + info());
                }
            } else if (ch == CHAR28) {
                quote = !quote;
            } else if (ch == CHAR40) {
                if (quote) {
                    continue;
                }
                bracketCnt++;
            } else if (ch == '{' && valid) {
                {
                    int index = ++bp;
                    this.ch = (index >= text.length()
                            ? EOI
                            : text.charAt(index));
                }

                skipObject(valid);
            } else if (ch == CHAR19) {
                if (quote) {
                    continue;
                } else {
                    bracketCnt--;
                }
                if (bracketCnt == -1) {
                    this.bp = i + 1;
                    if (this.bp == text.length()) {
                        this.ch = EOI;
                        token = JSONToken.EOF;
                        return;
                    }
                    this.ch = text.charAt(this.bp);
                    nextToken(JSONToken.COMMA);
                    return;
                }
            }
        }

        if (i == text.length()) {
            throw new JSONException("illegal str, " + info());
        }
    }

    public final void skipString() {
        if (ch == CHAR28) {
            for (int i = bp + 1; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (c == CHAR35) {
                    if (i < len - 1) {
                        ++i;
                        continue;
                    }
                } else if (c == CHAR28) {
                    this.ch = text.charAt(bp = i + 1);
                    return;
                }
            }
            throw new JSONException("unclosed str");
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public boolean seekArrayToItem(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("index must > 0, but " + index);
        }

        if (token == JSONToken.EOF) {
            return false;
        }

        if (token != JSONToken.LBRACKET) {
            throw new UnsupportedOperationException();
        }
//        nextToken();

        for (int i = 0; i < index; ++i) {
            skipWhitespace();
            if (ch == CHAR28 || ch == '\'') {
                skipString();
                if (ch == CHAR18) {
                    next();
                    continue;
                } else if (ch == CHAR19) {
                    next();
                    nextToken(JSONToken.COMMA);
                    return false;
                } else {
                    throw new JSONException("illegal json.");
                }
            } else if (ch == '{') {
                next();
                token = JSONToken.LBRACE;
                skipObject(false);
            } else if (ch == CHAR40) {
                next();
                token = JSONToken.LBRACKET;
                skipArray(false);
            } else {
                boolean match = false;
                for (int j = bp + 1; j < text.length(); ++j) {
                    char c = text.charAt(j);
                    if (c == CHAR18) {
                        match = true;
                        bp = j + 1;
                        ch = charAt(bp);
                        break;
                    } else if (c == CHAR19) {
                        bp = j + 1;
                        ch = charAt(bp);
                        nextToken();
                        return false;
                    }
                }

                if (!match) {
                    throw new JSONException("illegal json.");
                }

                continue;
            }

            if (token == JSONToken.COMMA) {
                continue;
            } else if (token == JSONToken.RBRACKET) {
                return false;
            } else {
                throw new UnsupportedOperationException();
            }

        }

        nextToken();
        return true;
    }

    @Override
    public int seekObjectToField(long fieldNameHash, boolean deepScan) {
        if (token == JSONToken.EOF) {
            return JSONLexer.NOT_MATCH;
        }

        if (token == JSONToken.RBRACE || token == JSONToken.RBRACKET) {
            nextToken();
            return JSONLexer.NOT_MATCH;
        }

        if (token != JSONToken.LBRACE && token != JSONToken.COMMA) {
            throw new UnsupportedOperationException(JSONToken.name(token));
        }

        for (; ; ) {
            if (ch == CHAR34) {
                next();
                nextToken();
                return JSONLexer.NOT_MATCH;
            }
            if (ch == EOI) {
                return JSONLexer.NOT_MATCH;
            }

            if (ch != CHAR28) {
                skipWhitespace();
            }

            long hash;
            if (ch == CHAR28) {
                hash = 0xcbf29ce484222325L;

                for (int i = bp + 1; i < text.length(); ++i) {
                    char c = text.charAt(i);
                    if (c == CHAR35) {
                        ++i;
                        if (i == text.length()) {
                            throw new JSONException("unclosed str, " + info());
                        }
                        c = text.charAt(i);
                    }

                    if (c == CHAR28) {
                        bp = i + 1;
                        ch = (bp >= text.length()
                                ? EOI
                                : text.charAt(bp));
                        break;
                    }

                    hash ^= c;
                    hash *= 0x100000001b3L;
                }
            } else {
                throw new UnsupportedOperationException();
            }

            if (hash == fieldNameHash) {
                if (ch != CHAR13) {
                    skipWhitespace();
                }
                if (ch == CHAR13) {
                    {
                        int index = ++bp;
                        ch = (index >= text.length()
                                ? EOI
                                : text.charAt(index));
                    }
                    if (ch == CHAR18) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.COMMA;
                    } else if (ch == CHAR19) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.RBRACKET;
                    } else if (ch == CHAR34) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.RBRACE;
                    } else if (ch >= CHAR15 && ch <= CHAR16) {
                        sp = 0;
                        pos = bp;
                        scanNumber();
                    } else {
                        nextToken(JSONToken.LITERAL_INT);
                    }
                }
                return VALUE;
            }

            if (ch != CHAR13) {
                skipWhitespace();
            }

            if (ch == CHAR13) {
                int index = ++bp;
                ch = (index >= text.length()
                        ? EOI
                        : text.charAt(index));
            } else {
                throw new JSONException("illegal json, " + info());
            }

            if (ch != CHAR28
                    && ch != '\''
                    && ch != '{'
                    && ch != CHAR40
                    && ch != CHAR15
                    && ch != CHAR30
                    && ch != CHAR33
                    && ch != CHAR32
                    && ch != CHAR31
                    && ch != CHAR36
                    && ch != CHAR38
                    && ch != '7'
                    && ch != CHAR37
                    && ch != CHAR16
                    && ch != CHAR29
                    && ch != CHAR11) {
                skipWhitespace();
            }

            // skip fieldValues
            boolean res2 = ch == CHAR11 || ch == CHAR29 || (ch >= CHAR15 && ch <= CHAR16);
            if (res2) {
                next();
                while (ch >= CHAR15 && ch <= CHAR16) {
                    next();
                }

                // scale
                if (ch == CHAR21) {
                    next();
                    while (ch >= CHAR15 && ch <= CHAR16) {
                        next();
                    }
                }

                // exp
                if (ch == 'E' || ch == CHAR8) {
                    next();
                    if (ch == CHAR11 || ch == CHAR29) {
                        next();
                    }
                    while (ch >= CHAR15 && ch <= CHAR16) {
                        next();
                    }
                }

                if (ch != CHAR18) {
                    skipWhitespace();
                }
                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == CHAR28) {
                skipString();

                if (ch != CHAR18 && ch != CHAR34) {
                    skipWhitespace();
                }

                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == CHAR7) {
                next();
                if (ch == CHAR41) {
                    next();
                    if (ch == CHAR42) {
                        next();
                        if (ch == CHAR8) {
                            next();
                        }
                    }
                }

                if (ch != CHAR18 && ch != CHAR34) {
                    skipWhitespace();
                }

                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == CHAR20) {
                next();
                if (ch == CHAR42) {
                    next();
                    if (ch == CHAR44) {
                        next();
                        if (ch == CHAR44) {
                            next();
                        }
                    }
                }

                if (ch != CHAR18 && ch != CHAR34) {
                    skipWhitespace();
                }

                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == CHAR43) {
                next();
                if (ch == CHAR6) {
                    next();
                    if (ch == CHAR44) {
                        next();
                        if (ch == CHAR45) {
                            next();
                            if (ch == CHAR8) {
                                next();
                            }
                        }
                    }
                }

                if (ch != CHAR18 && ch != CHAR34) {
                    skipWhitespace();
                }

                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == '{') {
                {
                    int index = ++bp;
                    ch = (index >= text.length()
                            ? EOI
                            : text.charAt(index));
                }
                if (deepScan) {
                    token = JSONToken.LBRACE;
                    return OBJECT;
                }

                skipObject(false);
                if (token == JSONToken.RBRACE) {
                    return JSONLexer.NOT_MATCH;
                }
            } else if (ch == CHAR40) {
                next();
                if (deepScan) {
                    token = JSONToken.LBRACKET;
                    return ARRAY;
                }
                skipArray(false);
                if (token == JSONToken.RBRACE) {
                    return JSONLexer.NOT_MATCH;
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public int seekObjectToField(long[] fieldNameHash) {
        if (token != JSONToken.LBRACE && token != JSONToken.COMMA) {
            throw new UnsupportedOperationException();
        }

        for (; ; ) {
            if (ch == CHAR34) {
                next();
                nextToken();
                this.matchStat = JSONLexer.NOT_MATCH;
                return -1;
            }
            if (ch == EOI) {
                this.matchStat = JSONLexer.NOT_MATCH;
                return -1;
            }

            if (ch != CHAR28) {
                skipWhitespace();
            }

            long hash;
            if (ch == CHAR28) {
                hash = 0xcbf29ce484222325L;

                for (int i = bp + 1; i < text.length(); ++i) {
                    char c = text.charAt(i);
                    if (c == CHAR35) {
                        ++i;
                        if (i == text.length()) {
                            throw new JSONException("unclosed str, " + info());
                        }
                        c = text.charAt(i);
                    }

                    if (c == CHAR28) {
                        bp = i + 1;
                        ch = (bp >= text.length()
                                ? EOI
                                : text.charAt(bp));
                        break;
                    }

                    hash ^= c;
                    hash *= 0x100000001b3L;
                }
            } else {
                throw new UnsupportedOperationException();
            }

            int matchIndex = -1;
            for (int i = 0; i < fieldNameHash.length; i++) {
                if (hash == fieldNameHash[i]) {
                    matchIndex = i;
                    break;
                }
            }

            if (matchIndex != -1) {
                if (ch != CHAR13) {
                    skipWhitespace();
                }
                if (ch == CHAR13) {
                    {
                        int index = ++bp;
                        ch = (index >= text.length()
                                ? EOI
                                : text.charAt(index));
                    }
                    if (ch == CHAR18) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.COMMA;
                    } else if (ch == CHAR19) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.RBRACKET;
                    } else if (ch == CHAR34) {
                        {
                            int index = ++bp;
                            ch = (index >= text.length()
                                    ? EOI
                                    : text.charAt(index));
                        }
                        token = JSONToken.RBRACE;
                    } else if (ch >= CHAR15 && ch <= CHAR16) {
                        sp = 0;
                        pos = bp;
                        scanNumber();
                    } else {
                        nextToken(JSONToken.LITERAL_INT);
                    }
                }

                matchStat = VALUE;
                return matchIndex;
            }

            if (ch != CHAR13) {
                skipWhitespace();
            }

            if (ch == CHAR13) {
                int index = ++bp;
                ch = (index >= text.length()
                        ? EOI
                        : text.charAt(index));
            } else {
                throw new JSONException("illegal json, " + info());
            }

            if (ch != CHAR28
                    && ch != '\''
                    && ch != '{'
                    && ch != CHAR40
                    && ch != CHAR15
                    && ch != CHAR30
                    && ch != CHAR33
                    && ch != CHAR32
                    && ch != CHAR31
                    && ch != CHAR36
                    && ch != CHAR38
                    && ch != '7'
                    && ch != CHAR37
                    && ch != CHAR16
                    && ch != CHAR29
                    && ch != CHAR11) {
                skipWhitespace();
            }

            // skip fieldValues
            boolean res3 = ch == CHAR11 || ch == CHAR29 || (ch >= CHAR15 && ch <= CHAR16);
            if (res3) {
                next();
                while (ch >= CHAR15 && ch <= CHAR16) {
                    next();
                }

                // scale
                if (ch == CHAR21) {
                    next();
                    while (ch >= CHAR15 && ch <= CHAR16) {
                        next();
                    }
                }

                // exp
                if (ch == 'E' || ch == CHAR8) {
                    next();
                    if (ch == CHAR11 || ch == CHAR29) {
                        next();
                    }
                    while (ch >= CHAR15 && ch <= CHAR16) {
                        next();
                    }
                }

                if (ch != CHAR18) {
                    skipWhitespace();
                }
                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == CHAR28) {
                skipString();

                if (ch != CHAR18 && ch != CHAR34) {
                    skipWhitespace();
                }

                if (ch == CHAR18) {
                    next();
                }
            } else if (ch == '{') {
                {
                    int index = ++bp;
                    ch = (index >= text.length()
                            ? EOI
                            : text.charAt(index));
                }

                skipObject(false);
            } else if (ch == CHAR40) {
                next();

                skipArray(false);
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public String scanTypeName(SymbolTable symbolTable) {
        if (text.startsWith(STRING, bp)) {
            int p = text.indexOf(CHAR28, bp + INT6);
            if (p != -1) {
                bp += INT6;
                int h = 0;
                for (int i = bp; i < p; i++) {
                    h = 31 * h + text.charAt(i);
                }
                String typeName = addSymbol(bp, p - bp, h, symbolTable);
                char separator = text.charAt(p + 1);
                if (separator != CHAR18 && separator != CHAR19) {
                    return null;
                }
                bp = p + INT1;
                ch = text.charAt(bp);
                return typeName;
            }
        }

        return null;
    }
}
