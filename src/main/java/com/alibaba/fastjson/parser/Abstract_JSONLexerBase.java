/*
 * Copyright 1999-2019 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.fastjson.parser;

import java.io.Closeable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.IOUtils;

import static com.alibaba.fastjson.parser.JSONToken.*;

/**
 * @author wenshao[szujobs@hotmail.com]
 */
public abstract class Abstract_JSONLexerBase implements JSONLexer, Closeable {

    public static final char CHAR = '_';
    public static final char CHAR1 = '$';
    public static final char CHAR2 = '-';
    public static final char CHAR3 = 'L';
    public static final char CHAR4 = 'S';
    public static final char CHAR5 = 'B';
    public static final char CHAR6 = '0';
    public static final char CHAR7 = '9';
    public static final char CHAR8 = '/';
    public static final char CHAR9 = '*';
    public static final char CHAR10 = '"';
    public static final char CHAR11 = '\'';
    public static final char CHAR12 = '}';
    public static final char CHAR13 = ',';
    public static final int INT = 4;
    public static final char CHAR14 = 'n';
    public static final char CHAR15 = 'u';
    public static final char CHAR16 = 'l';
    public static final int INT1 = 2;
    public static final int INT2 = 1024;
    public static final int INT3 = 8;
    public static final char CHAR17 = ']';

    protected void lexError(String key, Object... args) {
        token = ERROR;
    }

    protected int token;
    protected int pos;
    protected int features;

    protected char ch;
    protected int bp;

    protected int eofPos;

    /**
     * A character buffer for literals.
     */
    protected char[] sbuf;
    protected int sp;

    /**
     * number start position
     */
    protected int np;

    protected boolean hasSpecial;

    protected Calendar calendar = null;
    protected TimeZone timeZone = JSON.defaultTimeZone;
    protected Locale locale = JSON.defaultLocale;

    public int matchStat = UNKNOWN;

    private final static ThreadLocal<char[]> SBUF_LOCAL = new ThreadLocal<char[]>();

    protected String stringDefaultValue = null;
    protected int nanos = 0;

    public Abstract_JSONLexerBase(int features) {
        this.features = features;

        if ((features & Feature.InitStringFieldAsEmpty.mask) != 0) {
            stringDefaultValue = "";
        }

        sbuf = SBUF_LOCAL.get();


        boolean a=false;
        while (a){
            SBUF_LOCAL.remove();
        }
        if (sbuf == null) {
            sbuf = new char[512];
        }
    }

    public final int matchStat() {
        return matchStat;
    }

    /**
     * internal method, don't invoke
     *
     * @param token
     */
    public void setToken(int token) {
        this.token = token;
    }

    @Override
    public final void nextToken() {
        sp = 0;

        for (; ; ) {
            pos = bp;

            if (ch == CHAR8) {
                skipComment();
                continue;
            }

            if (ch == CHAR10) {
                scanString();
                return;
            }

            if (ch == CHAR13) {
                next();
                token = COMMA;
                return;
            }

            if (ch >= CHAR6 && ch <= CHAR7) {
                scanNumber();
                return;
            }

            if (ch == CHAR2) {
                scanNumber();
                return;
            }

            switch (ch) {
                case CHAR11:
                    if (!isEnabled(Feature.AllowSingleQuotes)) {
                        throw new JSONException("Feature.AllowSingleQuotes is false");
                    }
                    scanStringSingleQuote();
                    return;
                case ' ':
                case '\t':
                case '\b':
                case '\f':
                case '\n':
                case '\r':
                    next();
                    break;
                case 't':
                    // true
                    scanTrue();
                    return;
                case 'f':
                    // false
                    scanFalse();
                    return;
                case CHAR14:
                    // new,null
                    scanNullOrNew();
                    return;
                case 'T':
                case 'N':
                case CHAR4:
                case CHAR15:
                    // undefined
                    scanIdent();
                    return;
                case '(':
                    next();
                    token = LPAREN;
                    return;
                case ')':
                    next();
                    token = RPAREN;
                    return;
                case '[':
                    next();
                    token = LBRACKET;
                    return;
                case CHAR17:
                    next();
                    token = RBRACKET;
                    return;
                case '{':
                    next();
                    token = LBRACE;
                    return;
                case CHAR12:
                    next();
                    token = RBRACE;
                    return;
                case ':':
                    next();
                    token = COLON;
                    return;
                case ';':
                    next();
                    token = SEMI;
                    return;
                case '.':
                    next();
                    token = DOT;
                    return;
                case '+':
                    next();
                    scanNumber();
                    return;
                case 'x':
                    scanHex();
                    return;
                default:
                    if (isEof()) {
                        // JLS
                        if (token == EOF) {
                            throw new JSONException("EOF error");
                        }

                        token = EOF;
                        eofPos = pos = bp;
                    } else {
                        if (ch <= 31 || ch == 127) {
                            next();
                            break;
                        }

                        lexError("illegal.char", String.valueOf((int) ch));
                        next();
                    }

                    return;
            }
        }

    }

    @Override
    public final void nextToken(int expect) {
        sp = 0;

        for (; ; ) {
            switch (expect) {
                case JSONToken.LBRACE:
                    if (ch == '{') {
                        token = JSONToken.LBRACE;
                        next();
                        return;
                    }
                    if (ch == '[') {
                        token = JSONToken.LBRACKET;
                        next();
                        return;
                    }
                    break;
                case JSONToken.COMMA:
                    if (ch == CHAR13) {
                        token = JSONToken.COMMA;
                        next();
                        return;
                    }

                    if (ch == CHAR12) {
                        token = JSONToken.RBRACE;
                        next();
                        return;
                    }

                    if (ch == CHAR17) {
                        token = JSONToken.RBRACKET;
                        next();
                        return;
                    }

                    if (ch == EOI) {
                        token = JSONToken.EOF;
                        return;
                    }

                    if (ch == CHAR14) {
                        scanNullOrNew(false);
                        return;
                    }
                    break;
                case JSONToken.LITERAL_INT:
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        pos = bp;
                        scanNumber();
                        return;
                    }

                    if (ch == CHAR10) {
                        pos = bp;
                        scanString();
                        return;
                    }

                    if (ch == '[') {
                        token = JSONToken.LBRACKET;
                        next();
                        return;
                    }

                    if (ch == '{') {
                        token = JSONToken.LBRACE;
                        next();
                        return;
                    }

                    break;
                case JSONToken.LITERAL_STRING:
                    if (ch == CHAR10) {
                        pos = bp;
                        scanString();
                        return;
                    }

                    if (ch >= CHAR6 && ch <= CHAR7) {
                        pos = bp;
                        scanNumber();
                        return;
                    }

                    if (ch == '[') {
                        token = JSONToken.LBRACKET;
                        next();
                        return;
                    }

                    if (ch == '{') {
                        token = JSONToken.LBRACE;
                        next();
                        return;
                    }
                    break;
                case JSONToken.LBRACKET:
                    if (ch == '[') {
                        token = JSONToken.LBRACKET;
                        next();
                        return;
                    }

                    if (ch == '{') {
                        token = JSONToken.LBRACE;
                        next();
                        return;
                    }
                    break;
                case JSONToken.RBRACKET:
                    if (ch == CHAR17) {
                        token = JSONToken.RBRACKET;
                        next();
                        return;
                    }
                case JSONToken.EOF:
                    if (ch == EOI) {
                        token = JSONToken.EOF;
                        return;
                    }
                    break;
                case JSONToken.IDENTIFIER:
                    nextIdent();
                    return;
                default:
                    break;
            }

            if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b') {
                next();
                continue;
            }

            nextToken();
            break;
        }
    }

    public final void nextIdent() {
        while (isWhitespace(ch)) {
            next();
        }
        if (ch == CHAR || ch == CHAR1 || Character.isLetter(ch)) {
            scanIdent();
        } else {
            nextToken();
        }
    }

    @Override
    public final void nextTokenWithColon() {
        nextTokenWithChar(':');
    }

    public final void nextTokenWithChar(char expect) {
        sp = 0;

        for (; ; ) {
            if (ch == expect) {
                next();
                nextToken();
                return;
            }

            if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b') {
                next();
                continue;
            }

            throw new JSONException("not match " + expect + " - " + ch + ", info : " + this.info());
        }
    }

    @Override
    public final int token() {
        return token;
    }

    @Override
    public final String tokenName() {
        return JSONToken.name(token);
    }

    @Override
    public final int pos() {
        return pos;
    }

    public final String stringDefaultValue() {
        return stringDefaultValue;
    }

    @Override
    public final Number integerValue() throws NumberFormatException {
        long result = 0;
        boolean negative = false;
        if (np == -1) {
            np = 0;
        }
        int i = np, max = np + sp;
        long limit;
        long multmin;
        int digit;

        char type = ' ';

        switch (charAt(max - 1)) {
            case CHAR3:
                max--;
                type = CHAR3;
                break;
            case CHAR4:
                max--;
                type = CHAR4;
                break;
            case CHAR5:
                max--;
                type = CHAR5;
                break;
            default:
                break;
        }

        if (charAt(np) == CHAR2) {
            negative = true;
            limit = Long.MIN_VALUE;
            i++;
        } else {
            limit = -Long.MAX_VALUE;
        }
        multmin = MULTMIN_RADIX_TEN;
        if (i < max) {
            digit = charAt(i++) - CHAR6;
            result = -digit;
        }
        while (i < max) {
            // Accumulating negatively avoids surprises near MAX_VALUE
            digit = charAt(i++) - CHAR6;
            if (result < multmin) {
                return new BigInteger(numberString());
            }
            result *= 10;
            if (result < limit + digit) {
                return new BigInteger(numberString());
            }
            result -= digit;
        }

        if (negative) {
            if (i > np + 1) {
                if (result >= Integer.MIN_VALUE && type != CHAR3) {
                    if (type == CHAR4) {
                        return (short) result;
                    }

                    if (type == CHAR5) {
                        return (byte) result;
                    }

                    return (int) result;
                }
                return result;
            } else {
                /* Only got "-" */
                throw new NumberFormatException(numberString());
            }
        } else {
            result = -result;
            if (result <= Integer.MAX_VALUE && type != CHAR3) {
                if (type == CHAR4) {
                    return (short) result;
                }

                if (type == CHAR5) {
                    return (byte) result;
                }

                return (int) result;
            }
            return result;
        }
    }

    @Override
    public final void nextTokenWithColon(int expect) {
        nextTokenWithChar(':');
    }

    @Override
    public float floatValue() {
        String strVal = numberString();
        float floatValue = Float.parseFloat(strVal);
        if (floatValue == 0 || floatValue == Float.POSITIVE_INFINITY) {
            char c0 = strVal.charAt(0);
            if (c0 > CHAR6 && c0 <= CHAR7) {
                throw new JSONException("float overflow : " + strVal);
            }
        }
        return floatValue;
    }

    public double doubleValue() {
        return Double.parseDouble(numberString());
    }

    @Override
    public void config(Feature feature, boolean state) {
        features = Feature.config(features, feature, state);

        if ((features & Feature.InitStringFieldAsEmpty.mask) != 0) {
            stringDefaultValue = "";
        }
    }

    @Override
    public final boolean isEnabled(Feature feature) {
        return isEnabled(feature.mask);
    }

    @Override
    public final boolean isEnabled(int feature) {
        return (this.features & feature) != 0;
    }

    public final boolean isEnabled(int features, int feature) {
        return (this.features & feature) != 0 || (features & feature) != 0;
    }

    @Override
    public abstract String numberString();

    public abstract boolean isEof();

    @Override
    public final char getCurrent() {
        return ch;
    }

    public abstract char charAt(int index);

    // public final char next() {
    // ch = doNext();
    //// if (ch == '/' && (this.features & Feature.AllowComment.mask) != 0) {
    //// skipComment();
    //// }
    // return ch;
    // }

    @Override
    public abstract char next();

    protected void skipComment() {
        next();
        if (ch == CHAR8) {
            for (; ; ) {
                next();
                if (ch == '\n') {
                    next();
                    return;
                } else if (ch == EOI) {
                    return;
                }
            }
        } else if (ch == CHAR9) {
            next();

            for (; ch != EOI; ) {
                if (ch == CHAR9) {
                    next();
                    if (ch == CHAR8) {
                        next();
                        return;
                    } else {
                        continue;
                    }
                }
                next();
            }
        } else {
            throw new JSONException("invalid comment");
        }
    }

    @Override
    public final String scanSymbol(final SymbolTable symbolTable) {
        skipWhitespace();

        if (ch == CHAR10) {
            return scanSymbol(symbolTable, CHAR10);
        }

        if (ch == CHAR11) {
            if (!isEnabled(Feature.AllowSingleQuotes)) {
                throw new JSONException("syntax error");
            }

            return scanSymbol(symbolTable, CHAR11);
        }

        if (ch == CHAR12) {
            next();
            token = JSONToken.RBRACE;
            return null;
        }

        if (ch == CHAR13) {
            next();
            token = JSONToken.COMMA;
            return null;
        }

        if (ch == EOI) {
            token = JSONToken.EOF;
            return null;
        }

        if (!isEnabled(Feature.AllowUnQuotedFieldNames)) {
            throw new JSONException("syntax error");
        }

        return scanSymbolUnQuoted(symbolTable);
    }

    // public abstract String scanSymbol(final SymbolTable symbolTable, final char quote);

    protected abstract void arrayCopy(int srcPos, char[] dest, int destPos, int length);

    @Override
    public final String scanSymbol(final SymbolTable symbolTable, final char quote) {
        int hash = 0;

        np = bp;
        sp = 0;
        boolean hasSpecial = false;
        char chLocal;
        for (; ; ) {
            chLocal = next();

            if (chLocal == quote) {
                break;
            }

            if (chLocal == EOI) {
                throw new JSONException("unclosed.str");
            }

            if (chLocal == '\\') {
                if (!hasSpecial) {
                    hasSpecial = true;

                    if (sp >= sbuf.length) {
                        int newCapcity = sbuf.length * INT1;
                        if (sp > newCapcity) {
                            newCapcity = sp;
                        }
                        char[] newsbuf = new char[newCapcity];
                        System.arraycopy(sbuf, 0, newsbuf, 0, sbuf.length);
                        sbuf = newsbuf;
                    }
                    arrayCopy(np + 1, sbuf, 0, sp);
                }

                chLocal = next();

                switch (chLocal) {
                    case CHAR6:
                        hash = 31 * hash + (int) chLocal;
                        putChar('\0');
                        break;
                    case '1':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\1');
                        break;
                    case '2':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\2');
                        break;
                    case '3':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\3');
                        break;
                    case '4':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\4');
                        break;
                    case '5':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\5');
                        break;
                    case '6':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\6');
                        break;
                    case '7':
                        hash = 31 * hash + (int) chLocal;
                        putChar('\7');
                        break;
                    case 'b':
                        // 8
                        hash = 31 * hash + (int) '\b';
                        putChar('\b');
                        break;
                    case 't':
                        // 9
                        hash = 31 * hash + (int) '\t';
                        putChar('\t');
                        break;
                    case CHAR14:
                        // 10
                        hash = 31 * hash + (int) '\n';
                        putChar('\n');
                        break;
                    case 'v':
                        // 11
                        hash = 31 * hash + (int) '\u000B';
                        putChar('\u000B');
                        break;
                    case 'f':
                    case 'F':
                        // 12
                        hash = 31 * hash + (int) '\f';
                        putChar('\f');
                        break;
                    case 'r':
                        // 13
                        hash = 31 * hash + (int) '\r';
                        putChar('\r');
                        break;
                    case CHAR10:
                        // 34
                        hash = 31 * hash + (int) CHAR10;
                        putChar(CHAR10);
                        break;
                    case CHAR11:
                        // 39
                        hash = 31 * hash + (int) CHAR11;
                        putChar(CHAR11);
                        break;
                    case CHAR8:
                        // 47
                        hash = 31 * hash + (int) CHAR8;
                        putChar(CHAR8);
                        break;
                    case '\\':
                        // 92
                        hash = 31 * hash + (int) '\\';
                        putChar('\\');
                        break;
                    case 'x':
                        char x1 = ch = next();
                        char x2 = ch = next();

                        int xVal = DIGITS[x1] * 16 + DIGITS[x2];
                        char xChar = (char) xVal;
                        hash = 31 * hash + (int) xChar;
                        putChar(xChar);
                        break;
                    case CHAR15:
                        char c1 = chLocal = next();
                        char c2 = chLocal = next();
                        char c3 = chLocal = next();
                        char c4 = chLocal = next();
                        int val = Integer.parseInt(new String(new char[]{c1, c2, c3, c4}), 16);
                        hash = 31 * hash + val;
                        putChar((char) val);
                        break;
                    default:
                        this.ch = chLocal;
                        throw new JSONException("unclosed.str.lit");
                }
                continue;
            }

            hash = 31 * hash + chLocal;

            if (!hasSpecial) {
                sp++;
                continue;
            }

            if (sp == sbuf.length) {
                putChar(chLocal);
            } else {
                sbuf[sp++] = chLocal;
            }
        }

        token = LITERAL_STRING;

        String value;
        if (!hasSpecial) {
            int offset;
            if (np == -1) {
                offset = 0;
            } else {
                offset = np + 1;
            }
            value = addSymbol(offset, sp, hash, symbolTable);
        } else {
            value = symbolTable.addSymbol(sbuf, 0, sp, hash);
        }

        sp = 0;
        this.next();

        return value;
    }

    @Override
    public final void resetStringPosition() {
        this.sp = 0;
    }

    @Override
    public String info() {
        return "";
    }

    @Override
    public final String scanSymbolUnQuoted(final SymbolTable symbolTable) {
        if (token == JSONToken.ERROR && pos == 0 && bp == 1) {
            bp = 0;
        }
        final boolean[] firstIdentifierFlags = IOUtils.FIRST_IDENTIFIER_FLAGS;
        final char first = ch;

        final boolean firstFlag = ch >= firstIdentifierFlags.length || firstIdentifierFlags[first];
        if (!firstFlag) {
            throw new JSONException("illegal identifier : " + ch
                    + info());
        }

        final boolean[] identifierFlags = IOUtils.IDENTIFIER_FLAGS;

        int hash = first;

        np = bp;
        sp = 1;
        char chLocal;
        for (; ; ) {
            chLocal = next();

            if (chLocal < identifierFlags.length) {
                if (!identifierFlags[chLocal]) {
                    break;
                }
            }

            hash = 31 * hash + chLocal;

            sp++;
            continue;
        }

        this.ch = charAt(bp);
        token = JSONToken.IDENTIFIER;

        final int nullHash = 3392903;
        if (sp == INT && hash == nullHash && charAt(np) == CHAR14
                && charAt(np + 1) == CHAR15 && charAt(np + INT1) == CHAR16
                && charAt(np + 3) == CHAR16) {
            return null;
        }


        if (symbolTable == null) {
            return subString(np, sp);
        }

        return this.addSymbol(np, sp, hash, symbolTable);
        // return symbolTable.addSymbol(buf, np, sp, hash);
    }

    protected abstract void copyTo(int offset, int count, char[] dest);

    @Override
    public final void scanString() {
        np = bp;
        hasSpecial = false;
        char ch;
        for (; ; ) {
            ch = next();

            if (ch == '\"') {
                break;
            }

            if (ch == EOI) {
                if (!isEof()) {
                    putChar((char) EOI);
                    continue;
                }
                throw new JSONException("unclosed string : " + ch);
            }

            if (ch == '\\') {
                if (!hasSpecial) {
                    hasSpecial = true;

                    if (sp >= sbuf.length) {
                        int newCapcity = sbuf.length * INT1;
                        if (sp > newCapcity) {
                            newCapcity = sp;
                        }
                        char[] newsbuf = new char[newCapcity];
                        System.arraycopy(sbuf, 0, newsbuf, 0, sbuf.length);
                        sbuf = newsbuf;
                    }

                    copyTo(np + 1, sp, sbuf);
                }

                ch = next();

                switch (ch) {
                    case CHAR6:
                        putChar('\0');
                        break;
                    case '1':
                        putChar('\1');
                        break;
                    case '2':
                        putChar('\2');
                        break;
                    case '3':
                        putChar('\3');
                        break;
                    case '4':
                        putChar('\4');
                        break;
                    case '5':
                        putChar('\5');
                        break;
                    case '6':
                        putChar('\6');
                        break;
                    case '7':
                        putChar('\7');
                        break;
                    case 'b':
                        // 8
                        putChar('\b');
                        break;
                    case 't':
                        // 9
                        putChar('\t');
                        break;
                    case CHAR14:
                        // 10
                        putChar('\n');
                        break;
                    case 'v':
                        // 11
                        putChar('\u000B');
                        break;
                    case 'f':
                    case 'F':
                        // 12
                        putChar('\f');
                        break;
                    case 'r':
                        // 13
                        putChar('\r');
                        break;
                    case CHAR10:
                        // 34
                        putChar(CHAR10);
                        break;
                    case CHAR11:
                        // 39
                        putChar(CHAR11);
                        break;
                    case CHAR8:
                        // 47
                        putChar(CHAR8);
                        break;
                    case '\\':
                        // 92
                        putChar('\\');
                        break;
                    case 'x':
                        char x1 = next();
                        char x2 = next();

                        boolean hex1 = (x1 >= CHAR6 && x1 <= CHAR7)
                                || (x1 >= 'a' && x1 <= 'f')
                                || (x1 >= 'A' && x1 <= 'F');
                        boolean hex2 = (x2 >= CHAR6 && x2 <= CHAR7)
                                || (x2 >= 'a' && x2 <= 'f')
                                || (x2 >= 'A' && x2 <= 'F');
                        if (!hex1 || !hex2) {
                            throw new JSONException("invalid escape character \\x" + x1 + x2);
                        }

                        char xChar = (char) (DIGITS[x1] * 16 + DIGITS[x2]);
                        putChar(xChar);
                        break;
                    case CHAR15:
                        char u1 = next();
                        char u2 = next();
                        char u3 = next();
                        char u4 = next();
                        int val = Integer.parseInt(new String(new char[]{u1, u2, u3, u4}), 16);
                        putChar((char) val);
                        break;
                    default:
                        this.ch = ch;
                        throw new JSONException("unclosed string : " + ch);
                }
                continue;
            }

            if (!hasSpecial) {
                sp++;
                continue;
            }

            if (sp == sbuf.length) {
                putChar(ch);
            } else {
                sbuf[sp++] = ch;
            }
        }

        token = JSONToken.LITERAL_STRING;
        this.ch = next();
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    @Override
    public TimeZone getTimeZone() {
        return timeZone;
    }

    @Override
    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public final int intValue() {
        if (np == -1) {
            np = 0;
        }

        int result = 0;
        boolean negative = false;
        int i = np, max = np + sp;
        int limit;
        int digit;

        if (charAt(np) == CHAR2) {
            negative = true;
            limit = Integer.MIN_VALUE;
            i++;
        } else {
            limit = -Integer.MAX_VALUE;
        }
        long multmin = INT_MULTMIN_RADIX_TEN;
        if (i < max) {
            digit = charAt(i++) - CHAR6;
            result = -digit;
        }
        while (i < max) {
            // Accumulating negatively avoids surprises near MAX_VALUE
            char chLocal = charAt(i++);

            if (chLocal == CHAR3 || chLocal == CHAR4 || chLocal == CHAR5) {
                break;
            }

            digit = chLocal - CHAR6;

            if (result < multmin) {
                throw new NumberFormatException(numberString());
            }
            result *= 10;
            if (result < limit + digit) {
                throw new NumberFormatException(numberString());
            }
            result -= digit;
        }

        if (negative) {
            if (i > np + 1) {
                return result;
            } else {
                /* Only got "-" */
                throw new NumberFormatException(numberString());
            }
        } else {
            return -result;
        }
    }

    @Override
    public abstract byte[] bytesValue();

    @Override
    public void close() {
        if (sbuf.length <= INT2 * INT3) {
            SBUF_LOCAL.set(sbuf);
        }
        this.sbuf = null;
    }

    @Override
    public final boolean isRef() {
        if (sp != INT) {
            return false;
        }

        return charAt(np + 1) == CHAR1
                && charAt(np + INT1) == 'r'
                && charAt(np + 3) == 'e'
                && charAt(np + INT) == 'f';
    }

    @Override
    public String scanTypeName(SymbolTable symbolTable) {
        return null;
    }

    protected final static char[] TYPE_FIELD_NAME = ("\"" + JSON.DEFAULT_TYPE_KEY + "\":\"").toCharArray();

    public final int scanType(String type) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(TYPE_FIELD_NAME)) {
            return NOT_MATCH_NAME;
        }

        int bpLocal = this.bp + TYPE_FIELD_NAME.length;

        final int typeLength = type.length();
        for (int i = 0; i < typeLength; ++i) {
            if (type.charAt(i) != charAt(bpLocal + i)) {
                return NOT_MATCH;
            }
        }
        bpLocal += typeLength;
        if (charAt(bpLocal) != CHAR10) {
            return NOT_MATCH;
        }

        this.ch = charAt(++bpLocal);

        if (ch == CHAR13) {
            this.ch = charAt(++bpLocal);
            this.bp = bpLocal;
            token = JSONToken.COMMA;
            return VALUE;
        } else if (ch == CHAR12) {
            ch = charAt(++bpLocal);
            if (ch == CHAR13) {
                token = JSONToken.COMMA;
                this.ch = charAt(++bpLocal);
            } else if (ch == CHAR17) {
                token = JSONToken.RBRACKET;
                this.ch = charAt(++bpLocal);
            } else if (ch == CHAR12) {
                token = JSONToken.RBRACE;
                this.ch = charAt(++bpLocal);
            } else if (ch == EOI) {
                token = JSONToken.EOF;
            } else {
                return NOT_MATCH;
            }
            matchStat = END;
        }

        this.bp = bpLocal;
        return matchStat;
    }

    public final boolean matchField(char[] fieldName) {
        for (; ; ) {
            if (!charArrayCompare(fieldName)) {
                if (isWhitespace(ch)) {
                    next();
                    continue;
                }
                return false;
            } else {
                break;
            }
        }

        bp = bp + fieldName.length;
        ch = charAt(bp);

        if (ch == '{') {
            next();
            token = JSONToken.LBRACE;
        } else if (ch == '[') {
            next();
            token = JSONToken.LBRACKET;
        } else if (ch == CHAR4 && charAt(bp + 1) == 'e' && charAt(bp + INT1) == 't' && charAt(bp + 3) == '[') {
            bp += 3;
            ch = charAt(bp);
            token = JSONToken.SET;
        } else {
            nextToken();
        }

        return true;
    }

    public int matchField(long fieldNameHash) {
        throw new UnsupportedOperationException();
    }

    public boolean seekArrayToItem(int index) {
        throw new UnsupportedOperationException();
    }

    public int seekObjectToField(long fieldNameHash, boolean deepScan) {
        throw new UnsupportedOperationException();
    }

    public int seekObjectToField(long[] fieldNameHash) {
        throw new UnsupportedOperationException();
    }

    public int seekObjectToFieldDeepScan(long fieldNameHash) {
        throw new UnsupportedOperationException();
    }

    public void skipObject() {
        throw new UnsupportedOperationException();
    }

    public void skipObject(boolean valid) {
        throw new UnsupportedOperationException();
    }

    public void skipArray() {
        throw new UnsupportedOperationException();
    }

    public abstract int indexOf(char ch, int startIndex);

    public abstract String addSymbol(int offset, int len, int hash, final SymbolTable symbolTable);

    public String scanFieldString(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return stringDefaultValue();
        }

        // int index = bp + fieldName.length;

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != CHAR10) {
            matchStat = NOT_MATCH;

            return stringDefaultValue();
        }

        final String strVal;
        {
            int startIndex = bp + fieldName.length + 1;
            int endIndex = indexOf(CHAR10, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            int startIndex2 = bp + fieldName.length + 1;
            String stringVal = subString(startIndex2, endIndex - startIndex2);
            if (stringVal.indexOf('\\') != -1) {
                for (; ; ) {
                    int slashCount = 0;
                    for (int i = endIndex - 1; i >= 0; --i) {
                        if (charAt(i) == '\\') {
                            slashCount++;
                        } else {
                            break;
                        }
                    }
                    if (slashCount % INT1 == 0) {
                        break;
                    }
                    endIndex = indexOf(CHAR10, endIndex + 1);
                }

                int charsLen = endIndex - (bp + fieldName.length + 1);
                char[] chars = subChars(bp + fieldName.length + 1, charsLen);

                stringVal = readString(chars, charsLen);
            }

            offset += (endIndex - (bp + fieldName.length + 1) + 1);
            chLocal = charAt(bp + (offset++));
            strVal = stringVal;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return strVal;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return stringDefaultValue();
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return stringDefaultValue();
        }

        return strVal;
    }

    @Override
    public String scanString(char expectNextChar) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        if (chLocal == CHAR14) {
            if (charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
                offset += 3;
                chLocal = charAt(bp + (offset++));
            } else {
                matchStat = NOT_MATCH;
                return null;
            }

            if (chLocal == expectNextChar) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                return null;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
        }

        final String strVal;
        for (; ; ) {
            if (chLocal == CHAR10) {
                int startIndex = bp + offset;
                int endIndex = indexOf(CHAR10, startIndex);
                if (endIndex == -1) {
                    throw new JSONException("unclosed str");
                }

                String stringVal = subString(bp + offset, endIndex - startIndex);
                if (stringVal.indexOf('\\') != -1) {
                    for (; ; ) {
                        int slashCount = 0;
                        for (int i = endIndex - 1; i >= 0; --i) {
                            if (charAt(i) == '\\') {
                                slashCount++;
                            } else {
                                break;
                            }
                        }
                        if (slashCount % INT1 == 0) {
                            break;
                        }
                        endIndex = indexOf(CHAR10, endIndex + 1);
                    }

                    int charsLen = endIndex - startIndex;
                    char[] chars = subChars(bp + 1, charsLen);

                    stringVal = readString(chars, charsLen);
                }

                offset += (endIndex - startIndex + 1);
                chLocal = charAt(bp + (offset++));
                strVal = stringVal;
                break;
            } else if (isWhitespace(chLocal)) {
                chLocal = charAt(bp + (offset++));
                continue;
            } else {
                matchStat = NOT_MATCH;

                return stringDefaultValue();
            }
        }

        for (; ; ) {
            if (chLocal == expectNextChar) {
                bp += offset;
                this.ch = charAt(bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return strVal;
            } else if (isWhitespace(chLocal)) {
                chLocal = charAt(bp + (offset++));
                continue;
            } else {
                if (chLocal == CHAR17) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = NOT_MATCH;
                }
                return strVal;
            }
        }
    }

    public long scanFieldSymbol(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != CHAR10) {
            matchStat = NOT_MATCH;
            return 0;
        }

        long hash = 0xcbf29ce484222325L;
        for (; ; ) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == '\"') {
                chLocal = charAt(bp + (offset++));
                break;
            }

            hash ^= chLocal;
            hash *= 0x100000001b3L;

            if (chLocal == '\\') {
                matchStat = NOT_MATCH;
                return 0;
            }
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return hash;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return hash;
    }

    public long scanEnumSymbol(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != CHAR10) {
            matchStat = NOT_MATCH;
            return 0;
        }

        long hash = 0xcbf29ce484222325L;
        for (; ; ) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == '\"') {
                chLocal = charAt(bp + (offset++));
                break;
            }

            hash ^= ((chLocal >= 'A' && chLocal <= 'Z') ? (chLocal + 32) : chLocal);
            hash *= 0x100000001b3L;

            if (chLocal == '\\') {
                matchStat = NOT_MATCH;
                return 0;
            }
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return hash;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return hash;
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Enum<?> scanEnum(Class<?> enumClass, final SymbolTable symbolTable, char serperator) {
        String name = scanSymbolWithSeperator(symbolTable, serperator);
        if (name == null) {
            return null;
        }
        return Enum.valueOf((Class<? extends Enum>) enumClass, name);
    }

    @Override
    public String scanSymbolWithSeperator(final SymbolTable symbolTable, char serperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        if (chLocal == CHAR14) {
            if (charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
                offset += 3;
                chLocal = charAt(bp + (offset++));
            } else {
                matchStat = NOT_MATCH;
                return null;
            }

            if (chLocal == serperator) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                return null;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
        }

        if (chLocal != CHAR10) {
            matchStat = NOT_MATCH;
            return null;
        }

        String strVal;
        // int start = index;
        int hash = 0;
        for (; ; ) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == '\"') {
                // bp = index;
                // this.ch = chLocal = charAt(bp);
                int start = bp + 0 + 1;
                int len = bp + offset - start - 1;
                strVal = addSymbol(start, len, hash, symbolTable);
                chLocal = charAt(bp + (offset++));
                break;
            }

            hash = 31 * hash + chLocal;

            if (chLocal == '\\') {
                matchStat = NOT_MATCH;
                return null;
            }
        }

        for (; ; ) {
            if (chLocal == serperator) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                return strVal;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + (offset++));
                    continue;
                }

                matchStat = NOT_MATCH;
                return strVal;
            }
        }
    }

    public Collection<String> newCollectionByType(Class<?> type) {
        if (type.isAssignableFrom(HashSet.class)) {
            return new HashSet<String>();
        } else if (type.isAssignableFrom(ArrayList.class)) {
            return new ArrayList<String>();
        } else if (type.isAssignableFrom(LinkedList.class)) {
            return new LinkedList<String>();
        } else {
            try {
                return (Collection<String>) type.getDeclaredConstructor().newInstance();
            } catch (Exception e) {
                throw new JSONException(e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Collection<String> scanFieldStringArray(char[] fieldName, Class<?> type) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        Collection<String> list = newCollectionByType(type);

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != '[') {
            matchStat = NOT_MATCH;
            return null;
        }

        chLocal = charAt(bp + (offset++));

        for (; ; ) {
            // int start = index;
            if (chLocal == CHAR10) {
                int startIndex = bp + offset;
                int endIndex = indexOf(CHAR10, startIndex);
                if (endIndex == -1) {
                    throw new JSONException("unclosed str");
                }

                // must re compute
                int startIndex2 = bp + offset;
                String stringVal = subString(startIndex2, endIndex - startIndex2);
                if (stringVal.indexOf('\\') != -1) {
                    for (; ; ) {
                        int slashCount = 0;
                        for (int i = endIndex - 1; i >= 0; --i) {
                            if (charAt(i) == '\\') {
                                slashCount++;
                            } else {
                                break;
                            }
                        }
                        if (slashCount % INT1 == 0) {
                            break;
                        }
                        endIndex = indexOf(CHAR10, endIndex + 1);
                    }

                    int charsLen = endIndex - (bp + offset);
                    char[] chars = subChars(bp + offset, charsLen);

                    stringVal = readString(chars, charsLen);
                }

                offset += (endIndex - (bp + offset) + 1);
                chLocal = charAt(bp + (offset++));

                list.add(stringVal);
            } else if (chLocal == CHAR14
                    && charAt(bp + offset) == CHAR15
                    && charAt(bp + offset + 1) == CHAR16
                    && charAt(bp + offset + INT1) == CHAR16) {
                offset += 3;
                chLocal = charAt(bp + (offset++));
                list.add(null);
            } else if (chLocal == CHAR17 && list.size() == 0) {
                chLocal = charAt(bp + (offset++));
                break;
            } else {
                throw new JSONException("illega str");
            }

            if (chLocal == CHAR13) {
                chLocal = charAt(bp + (offset++));
                continue;
            }

            if (chLocal == CHAR17) {
                chLocal = charAt(bp + (offset++));
                break;
            }

            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return list;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                bp += (offset - 1);
                token = JSONToken.EOF;
                this.ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return list;
    }

    @Override
    public void scanStringArray(Collection<String> list, char seperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        if (chLocal == CHAR14
                && charAt(bp + offset) == CHAR15
                && charAt(bp + offset + 1) == CHAR16
                && charAt(bp + offset + INT1) == CHAR16
                && charAt(bp + offset + 3) == seperator
        ) {
            bp += 5;
            ch = charAt(bp);
            matchStat = VALUE_NULL;
            return;
        }

        if (chLocal != '[') {
            matchStat = NOT_MATCH;
            return;
        }

        chLocal = charAt(bp + (offset++));

        for (; ; ) {
            if (chLocal == CHAR14
                    && charAt(bp + offset) == CHAR15
                    && charAt(bp + offset + 1) == CHAR16
                    && charAt(bp + offset + INT1) == CHAR16) {
                offset += 3;
                chLocal = charAt(bp + (offset++));
                list.add(null);
            } else if (chLocal == CHAR17 && list.size() == 0) {
                chLocal = charAt(bp + (offset++));
                break;
            } else if (chLocal != CHAR10) {
                matchStat = NOT_MATCH;
                return;
            } else {
                int startIndex = bp + offset;
                int endIndex = indexOf(CHAR10, startIndex);
                if (endIndex == -1) {
                    throw new JSONException("unclosed str");
                }

                String stringVal = subString(bp + offset, endIndex - startIndex);
                if (stringVal.indexOf('\\') != -1) {
                    for (; ; ) {
                        int slashCount = 0;
                        for (int i = endIndex - 1; i >= 0; --i) {
                            if (charAt(i) == '\\') {
                                slashCount++;
                            } else {
                                break;
                            }
                        }
                        if (slashCount % INT1 == 0) {
                            break;
                        }
                        endIndex = indexOf(CHAR10, endIndex + 1);
                    }

                    int charsLen = endIndex - startIndex;
                    char[] chars = subChars(bp + offset, charsLen);

                    stringVal = readString(chars, charsLen);
                }

                offset += (endIndex - (bp + offset) + 1);
                chLocal = charAt(bp + (offset++));
                list.add(stringVal);
            }

            if (chLocal == CHAR13) {
                chLocal = charAt(bp + (offset++));
                continue;
            }

            if (chLocal == CHAR17) {
                chLocal = charAt(bp + (offset++));
                break;
            }

            matchStat = NOT_MATCH;
            return;
        }

        if (chLocal == seperator) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return;
        } else {
            matchStat = NOT_MATCH;
            return;
        }
    }

    public int scanFieldInt(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        final boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        int value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            value = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    value = value * 10 + (chLocal - CHAR6);
                } else if (chLocal == '.') {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    break;
                }
            }
            if (value < 0
                    || offset > 11 + 3 + fieldName.length) {
                if (value != Integer.MIN_VALUE
                        || offset != 17
                        || !negative) {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return negative ? -value : value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return negative ? -value : value;
    }

    public final int[] scanFieldIntArray(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != '[') {
            matchStat = NOT_MATCH_NAME;
            return null;
        }
        chLocal = charAt(bp + (offset++));

        int[] array = new int[16];
        int arrayIndex = 0;

        if (chLocal == CHAR17) {
            chLocal = charAt(bp + (offset++));
        } else {
            for (; ; ) {
                boolean nagative = false;
                if (chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                    nagative = true;
                }
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    int value = chLocal - CHAR6;
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));

                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            value = value * 10 + (chLocal - CHAR6);
                        } else {
                            break;
                        }
                    }

                    if (arrayIndex >= array.length) {
                        int[] tmp = new int[array.length * 3 / INT1];
                        System.arraycopy(array, 0, tmp, 0, arrayIndex);
                        array = tmp;
                    }
                    array[arrayIndex++] = nagative ? -value : value;

                    if (chLocal == CHAR13) {
                        chLocal = charAt(bp + (offset++));
                    } else if (chLocal == CHAR17) {
                        chLocal = charAt(bp + (offset++));
                        break;
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }
            }
        }


        if (arrayIndex != array.length) {
            int[] tmp = new int[arrayIndex];
            System.arraycopy(array, 0, tmp, 0, arrayIndex);
            array = tmp;
        }

        if (chLocal == CHAR13) {
            bp += (offset - 1);
            this.next();
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return array;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == EOI) {
                bp += (offset - 1);
                token = JSONToken.EOF;
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return array;
    }

    @Override
    public boolean scanBoolean(char expectNext) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        boolean value = false;
        if (chLocal == 't') {
            if (charAt(bp + offset) == 'r'
                    && charAt(bp + offset + 1) == CHAR15
                    && charAt(bp + offset + INT1) == 'e') {
                offset += 3;
                chLocal = charAt(bp + (offset++));
                value = true;
            } else {
                matchStat = NOT_MATCH;
                return false;
            }
        } else if (chLocal == 'f') {
            if (charAt(bp + offset) == 'a'
                    && charAt(bp + offset + 1) == CHAR16
                    && charAt(bp + offset + INT1) == 's'
                    && charAt(bp + offset + 3) == 'e') {
                offset += INT;
                chLocal = charAt(bp + (offset++));
                value = false;
            } else {
                matchStat = NOT_MATCH;
                return false;
            }
        } else if (chLocal == '1') {
            chLocal = charAt(bp + (offset++));
            value = true;
        } else if (chLocal == CHAR6) {
            chLocal = charAt(bp + (offset++));
            value = false;
        }

        for (; ; ) {
            if (chLocal == expectNext) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                return value;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + (offset++));
                    continue;
                }
                matchStat = NOT_MATCH;
                return value;
            }
        }
    }

    @Override
    public int scanInt(char expectNext) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        final boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        int value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            value = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    value = value * 10 + (chLocal - CHAR6);
                } else if (chLocal == '.') {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    break;
                }
            }
            if (value < 0) {
                matchStat = NOT_MATCH;
                return 0;
            }
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR17) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        for (; ; ) {
            if (chLocal == expectNext) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return negative ? -value : value;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + (offset++));
                    continue;
                }
                matchStat = NOT_MATCH;
                return negative ? -value : value;
            }
        }
    }

    public boolean scanFieldBoolean(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return false;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        boolean value;
        if (chLocal == 't') {
            if (charAt(bp + (offset++)) != 'r') {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(bp + (offset++)) != CHAR15) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(bp + (offset++)) != 'e') {
                matchStat = NOT_MATCH;
                return false;
            }

            value = true;
        } else if (chLocal == 'f') {
            if (charAt(bp + (offset++)) != 'a') {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(bp + (offset++)) != CHAR16) {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(bp + (offset++)) != 's') {
                matchStat = NOT_MATCH;
                return false;
            }
            if (charAt(bp + (offset++)) != 'e') {
                matchStat = NOT_MATCH;
                return false;
            }

            value = false;
        } else {
            matchStat = NOT_MATCH;
            return false;
        }

        chLocal = charAt(bp + offset++);
        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;

            return value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return false;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return false;
        }

        return value;
    }

    public long scanFieldLong(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        boolean negative = false;
        if (chLocal == CHAR2) {
            chLocal = charAt(bp + (offset++));
            negative = true;
        }

        long value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            value = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    value = value * 10 + (chLocal - CHAR6);
                } else if (chLocal == '.') {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    break;
                }
            }

            boolean valid = offset - fieldName.length < 21
                    && (value >= 0 || (value == -9223372036854775808L && negative));
            if (!valid) {
                matchStat = NOT_MATCH;
                return 0;
            }
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return negative ? -value : value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return negative ? -value : value;
    }

    @Override
    public long scanLong(char expectNextChar) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        final boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        long value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            value = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    value = value * 10 + (chLocal - CHAR6);
                } else if (chLocal == '.') {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    break;
                }
            }
            boolean valid = value >= 0 || (value == -9223372036854775808L && negative);
            if (!valid) {
                String val = subString(bp, offset - 1);
                throw new NumberFormatException(val);
            }
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR17) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (quote) {
            if (chLocal != CHAR10) {
                matchStat = NOT_MATCH;
                return 0;
            } else {
                chLocal = charAt(bp + (offset++));
            }
        }

        for (; ; ) {
            if (chLocal == expectNextChar) {
                bp += offset;
                this.ch = this.charAt(bp);
                matchStat = VALUE;
                token = JSONToken.COMMA;
                return negative ? -value : value;
            } else {
                if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + (offset++));
                    continue;
                }

                matchStat = NOT_MATCH;
                return value;
            }
        }
    }

    public final float scanFieldFloat(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        float value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            long intVal = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    continue;
                } else {
                    break;
                }
            }

            long power = 1;
            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    power = 10;
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            intVal = intVal * 10 + (chLocal - CHAR6);
                            power *= 10;
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + fieldName.length + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp + fieldName.length;
                count = bp + offset - start - 1;
            }

            if ((!exp) && count < 17) {
                value = (float) (((double) intVal) / power);
                if (negative) {
                    value = -value;
                }
            } else {
                String text = this.subString(start, count);
                value = Float.parseFloat(text);
            }
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR12) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACE;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                bp += (offset - 1);
                token = JSONToken.EOF;
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return value;
    }

    @Override
    public final float scanFloat(char seperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        float value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            long intVal = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    continue;
                } else {
                    break;
                }
            }

            long power = 1;
            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    power = 10;
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            intVal = intVal * 10 + (chLocal - CHAR6);
                            power *= 10;
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }
            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp;
                count = bp + offset - start - 1;
            }

            if ((!exp) && count < 17) {
                value = (float) (((double) intVal) / power);
                if (negative) {
                    value = -value;
                }
            } else {
                String text = this.subString(start, count);
                value = Float.parseFloat(text);
            }
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR17) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == seperator) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        } else {
            matchStat = NOT_MATCH;
            return value;
        }
    }

    @Override
    public double scanDouble(char seperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        double value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            long intVal = chLocal - CHAR6;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    continue;
                } else {
                    break;
                }
            }

            long power = 1;
            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    power = 10;
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            intVal = intVal * 10 + (chLocal - CHAR6);
                            power *= 10;
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp;
                count = bp + offset - start - 1;
            }

            if (!exp && count < 17) {
                value = ((double) intVal) / power;
                if (negative) {
                    value = -value;
                }
            } else {
                String text = this.subString(start, count);
                value = Double.parseDouble(text);
            }
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR17) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACKET;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == seperator) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        } else {
            matchStat = NOT_MATCH;
            return value;
        }
    }

    @Override
    public BigDecimal scanDecimal(char seperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        BigDecimal value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    continue;
                } else {
                    break;
                }
            }

            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return null;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp;
                count = bp + offset - start - 1;
            }

            char[] chars = this.subChars(start, count);
            value = new BigDecimal(chars);
        } else if (chLocal == CHAR14 && charAt(bp + offset) == CHAR15 && charAt(bp + offset + 1) == CHAR16 && charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = null;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR12) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACE;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return null;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        }

        if (chLocal == CHAR17) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return value;
    }

    public final float[] scanFieldFloatArray(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));
        if (chLocal != '[') {
            matchStat = NOT_MATCH_NAME;
            return null;
        }
        chLocal = charAt(bp + (offset++));

        float[] array = new float[16];
        int arrayIndex = 0;

        for (; ; ) {
            int start = bp + offset - 1;

            boolean negative = chLocal == CHAR2;
            if (negative) {
                chLocal = charAt(bp + (offset++));
            }

            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                int intVal = chLocal - CHAR6;
                for (; ; ) {
                    chLocal = charAt(bp + (offset++));
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        intVal = intVal * 10 + (chLocal - CHAR6);
                        continue;
                    } else {
                        break;
                    }
                }

                int power = 1;
                boolean small = (chLocal == '.');
                if (small) {
                    chLocal = charAt(bp + (offset++));
                    power = 10;
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        intVal = intVal * 10 + (chLocal - CHAR6);
                        for (; ; ) {
                            chLocal = charAt(bp + (offset++));

                            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                                intVal = intVal * 10 + (chLocal - CHAR6);
                                power *= 10;
                                continue;
                            } else {
                                break;
                            }
                        }
                    } else {
                        matchStat = NOT_MATCH;
                        return null;
                    }
                }

                boolean exp = chLocal == 'e' || chLocal == 'E';
                if (exp) {
                    chLocal = charAt(bp + (offset++));
                    if (chLocal == '+' || chLocal == CHAR2) {
                        chLocal = charAt(bp + (offset++));
                    }
                    for (; ; ) {
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            chLocal = charAt(bp + (offset++));
                        } else {
                            break;
                        }
                    }
                }

                int count = bp + offset - start - 1;

                float value;
                if (!exp && count < 10) {
                    value = ((float) intVal) / power;
                    if (negative) {
                        value = -value;
                    }
                } else {
                    String text = this.subString(start, count);
                    value = Float.parseFloat(text);
                }

                if (arrayIndex >= array.length) {
                    float[] tmp = new float[array.length * 3 / INT1];
                    System.arraycopy(array, 0, tmp, 0, arrayIndex);
                    array = tmp;
                }
                array[arrayIndex++] = value;

                if (chLocal == CHAR13) {
                    chLocal = charAt(bp + (offset++));
                } else if (chLocal == CHAR17) {
                    chLocal = charAt(bp + (offset++));
                    break;
                }
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
        }


        if (arrayIndex != array.length) {
            float[] tmp = new float[arrayIndex];
            System.arraycopy(array, 0, tmp, 0, arrayIndex);
            array = tmp;
        }

        if (chLocal == CHAR13) {
            bp += (offset - 1);
            this.next();
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return array;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == EOI) {
                bp += (offset - 1);
                token = JSONToken.EOF;
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return array;
    }

    public final float[][] scanFieldFloatArray2(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        if (chLocal != '[') {
            matchStat = NOT_MATCH_NAME;
            return null;
        }
        chLocal = charAt(bp + (offset++));

        float[][] arrayarray = new float[16][];
        int arrayarrayIndex = 0;

        for (; ; ) {
            if (chLocal == '[') {
                chLocal = charAt(bp + (offset++));

                float[] array = new float[16];
                int arrayIndex = 0;

                for (; ; ) {
                    int start = bp + offset - 1;
                    boolean negative = chLocal == CHAR2;
                    if (negative) {
                        chLocal = charAt(bp + (offset++));
                    }

                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        int intVal = chLocal - CHAR6;
                        for (; ; ) {
                            chLocal = charAt(bp + (offset++));

                            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                                intVal = intVal * 10 + (chLocal - CHAR6);
                                continue;
                            } else {
                                break;
                            }
                        }

                        int power = 1;
                        if (chLocal == '.') {
                            chLocal = charAt(bp + (offset++));

                            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                                intVal = intVal * 10 + (chLocal - CHAR6);
                                power = 10;
                                for (; ; ) {
                                    chLocal = charAt(bp + (offset++));

                                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                                        intVal = intVal * 10 + (chLocal - CHAR6);
                                        power *= 10;
                                        continue;
                                    } else {
                                        break;
                                    }
                                }
                            } else {
                                matchStat = NOT_MATCH;
                                return null;
                            }
                        }

                        boolean exp = chLocal == 'e' || chLocal == 'E';
                        if (exp) {
                            chLocal = charAt(bp + (offset++));
                            if (chLocal == '+' || chLocal == CHAR2) {
                                chLocal = charAt(bp + (offset++));
                            }
                            for (; ; ) {
                                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                                    chLocal = charAt(bp + (offset++));
                                } else {
                                    break;
                                }
                            }
                        }

                        int count = bp + offset - start - 1;
                        float value;
                        if (!exp && count < 10) {
                            value = ((float) intVal) / power;
                            if (negative) {
                                value = -value;
                            }
                        } else {
                            String text = this.subString(start, count);
                            value = Float.parseFloat(text);
                        }

                        if (arrayIndex >= array.length) {
                            float[] tmp = new float[array.length * 3 / INT1];
                            System.arraycopy(array, 0, tmp, 0, arrayIndex);
                            array = tmp;
                        }
                        array[arrayIndex++] = value;

                        if (chLocal == CHAR13) {
                            chLocal = charAt(bp + (offset++));
                        } else if (chLocal == CHAR17) {
                            chLocal = charAt(bp + (offset++));
                            break;
                        }
                    } else {
                        matchStat = NOT_MATCH;
                        return null;
                    }
                }

                // compact
                if (arrayIndex != array.length) {
                    float[] tmp = new float[arrayIndex];
                    System.arraycopy(array, 0, tmp, 0, arrayIndex);
                    array = tmp;
                }

                if (arrayarrayIndex >= arrayarray.length) {
                    float[][] tmp = new float[arrayarray.length * 3 / INT1][];
                    System.arraycopy(array, 0, tmp, 0, arrayIndex);
                    arrayarray = tmp;
                }
                arrayarray[arrayarrayIndex++] = array;

                if (chLocal == CHAR13) {
                    chLocal = charAt(bp + (offset++));
                } else if (chLocal == CHAR17) {
                    chLocal = charAt(bp + (offset++));
                    break;
                }
            } else {
                break;
            }
        }

        // compact
        if (arrayarrayIndex != arrayarray.length) {
            float[][] tmp = new float[arrayarrayIndex][];
            System.arraycopy(arrayarray, 0, tmp, 0, arrayarrayIndex);
            arrayarray = tmp;
        }

        if (chLocal == CHAR13) {
            bp += (offset - 1);
            this.next();
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return arrayarray;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += (offset - 1);
                this.next();
            } else if (chLocal == EOI) {
                bp += (offset - 1);
                token = JSONToken.EOF;
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return arrayarray;
    }

    public final double scanFieldDouble(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return 0;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        double value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            long intVal = chLocal - CHAR6;

            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    continue;
                } else {
                    break;
                }
            }

            long power = 1;
            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    intVal = intVal * 10 + (chLocal - CHAR6);
                    power = 10;
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            intVal = intVal * 10 + (chLocal - CHAR6);
                            power *= 10;
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return 0;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return 0;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + fieldName.length + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp + fieldName.length;
                count = bp + offset - start - 1;
            }

            if (!exp && count < 17) {
                value = ((double) intVal) / power;
                if (negative) {
                    value = -value;
                }
            } else {
                String text = this.subString(start, count);
                value = Double.parseDouble(text);
            }
        } else if (chLocal == CHAR14 &&
                charAt(bp + offset) == CHAR15 &&
                charAt(bp + offset + 1) == CHAR16 &&
                charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = 0;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR12) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACE;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return 0;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return 0;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return 0;
        }

        return value;
    }

    public BigDecimal scanFieldDecimal(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        BigDecimal value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    continue;
                } else {
                    break;
                }
            }

            boolean small = (chLocal == '.');
            if (small) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    for (; ; ) {
                        chLocal = charAt(bp + (offset++));
                        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }
            }

            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                chLocal = charAt(bp + (offset++));
                if (chLocal == '+' || chLocal == CHAR2) {
                    chLocal = charAt(bp + (offset++));
                }
                for (; ; ) {
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        chLocal = charAt(bp + (offset++));
                    } else {
                        break;
                    }
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return null;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + fieldName.length + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp + fieldName.length;
                count = bp + offset - start - 1;
            }

            char[] chars = this.subChars(start, count);
            value = new BigDecimal(chars);
        } else if (chLocal == CHAR14 &&
                charAt(bp + offset) == CHAR15 &&
                charAt(bp + offset + 1) == CHAR16 &&
                charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = null;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR12) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACE;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return null;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return value;
    }

    public BigInteger scanFieldBigInteger(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));
        final boolean quote = chLocal == CHAR10;
        if (quote) {
            chLocal = charAt(bp + (offset++));
        }

        boolean negative = chLocal == CHAR2;
        if (negative) {
            chLocal = charAt(bp + (offset++));
        }

        BigInteger value;
        if (chLocal >= CHAR6 && chLocal <= CHAR7) {
            long intVal = chLocal - CHAR6;
            boolean overflow = false;
            long temp;
            for (; ; ) {
                chLocal = charAt(bp + (offset++));
                if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                    temp = intVal * 10 + (chLocal - CHAR6);
                    if (temp < intVal) {
                        overflow = true;
                        break;
                    }
                    intVal = temp;
                    continue;
                } else {
                    break;
                }
            }

            int start, count;
            if (quote) {
                if (chLocal != CHAR10) {
                    matchStat = NOT_MATCH;
                    return null;
                } else {
                    chLocal = charAt(bp + (offset++));
                }
                start = bp + fieldName.length + 1;
                count = bp + offset - start - INT1;
            } else {
                start = bp + fieldName.length;
                count = bp + offset - start - 1;
            }

            boolean r1 = !overflow && (count < 20 || (negative && count < 21));
            if (r1) {
                value = BigInteger.valueOf(negative ? -intVal : intVal);
            } else {

                String strVal = this.subString(start, count);
                value = new BigInteger(strVal);
            }
        } else if (chLocal == CHAR14 &&
                charAt(bp + offset) == CHAR15 &&
                charAt(bp + offset + 1) == CHAR16 &&
                charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            value = null;
            offset += 3;
            chLocal = charAt(bp + offset++);

            if (quote && chLocal == CHAR10) {
                chLocal = charAt(bp + offset++);
            }

            for (; ; ) {
                if (chLocal == CHAR13) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.COMMA;
                    return value;
                } else if (chLocal == CHAR12) {
                    bp += offset;
                    this.ch = charAt(bp);
                    matchStat = VALUE_NULL;
                    token = JSONToken.RBRACE;
                    return value;
                } else if (isWhitespace(chLocal)) {
                    chLocal = charAt(bp + offset++);
                    continue;
                }
                break;
            }
            matchStat = NOT_MATCH;
            return null;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return value;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return value;
    }

    public java.util.Date scanFieldDate(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        // int index = bp + fieldName.length;

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        final java.util.Date dateVal;
        boolean r2 = chLocal == CHAR2 || (chLocal >= CHAR6 && chLocal <= CHAR7);
        if (chLocal == CHAR10) {
            int startIndex = bp + fieldName.length + 1;
            int endIndex = indexOf(CHAR10, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            // must re compute
            int startIndex2 = bp + fieldName.length + 1;
            String stringVal = subString(startIndex2, endIndex - startIndex2);
            if (stringVal.indexOf('\\') != -1) {
                for (; ; ) {
                    int slashCount = 0;
                    for (int i = endIndex - 1; i >= 0; --i) {
                        if (charAt(i) == '\\') {
                            slashCount++;
                        } else {
                            break;
                        }
                    }
                    if (slashCount % INT1 == 0) {
                        break;
                    }
                    endIndex = indexOf(CHAR10, endIndex + 1);
                }

                int charsLen = endIndex - (bp + fieldName.length + 1);
                char[] chars = subChars(bp + fieldName.length + 1, charsLen);

                stringVal = readString(chars, charsLen);
            }

            offset += (endIndex - (bp + fieldName.length + 1) + 1);
            chLocal = charAt(bp + (offset++));

            JSONScanner dateLexer = new JSONScanner(stringVal);
            try {
                if (dateLexer.scanISO8601DateIfMatch(false)) {
                    Calendar calendar = dateLexer.getCalendar();
                    dateVal = calendar.getTime();
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }
            } finally {
                dateLexer.close();
            }
        } else if (r2) {
            long millis = 0;

            boolean negative = false;
            if (chLocal == CHAR2) {
                chLocal = charAt(bp + (offset++));
                negative = true;
            }

            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                millis = chLocal - CHAR6;
                for (; ; ) {
                    chLocal = charAt(bp + (offset++));
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        millis = millis * 10 + (chLocal - CHAR6);
                    } else {
                        break;
                    }
                }
            }

            if (millis < 0) {
                matchStat = NOT_MATCH;
                return null;
            }

            if (negative) {
                millis = -millis;
            }

            dateVal = new java.util.Date(millis);
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return dateVal;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return dateVal;
    }

    public java.util.Date scanDate(char seperator) {
        matchStat = UNKNOWN;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        final java.util.Date dateVal;
        boolean r3 = chLocal == CHAR2 || (chLocal >= CHAR6 && chLocal <= CHAR7);
        if (chLocal == CHAR10) {
            int startIndex = bp + 1;
            int endIndex = indexOf(CHAR10, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            // must re compute
            int startIndex2 = bp + 1;
            String stringVal = subString(startIndex2, endIndex - startIndex2);
            if (stringVal.indexOf('\\') != -1) {
                for (; ; ) {
                    int slashCount = 0;
                    for (int i = endIndex - 1; i >= 0; --i) {
                        if (charAt(i) == '\\') {
                            slashCount++;
                        } else {
                            break;
                        }
                    }
                    if (slashCount % INT1 == 0) {
                        break;
                    }
                    endIndex = indexOf(CHAR10, endIndex + 1);
                }

                int charsLen = endIndex - (bp + 1);
                char[] chars = subChars(bp + 1, charsLen);

                stringVal = readString(chars, charsLen);
            }

            offset += (endIndex - (bp + 1) + 1);
            chLocal = charAt(bp + (offset++));

            JSONScanner dateLexer = new JSONScanner(stringVal);
            try {
                if (dateLexer.scanISO8601DateIfMatch(false)) {
                    Calendar calendar = dateLexer.getCalendar();
                    dateVal = calendar.getTime();
                } else {
                    matchStat = NOT_MATCH;
                    return null;
                }
            } finally {
                dateLexer.close();
            }
        } else if (r3) {
            long millis = 0;

            boolean negative = false;
            if (chLocal == CHAR2) {
                chLocal = charAt(bp + (offset++));
                negative = true;
            }

            if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                millis = chLocal - CHAR6;
                for (; ; ) {
                    chLocal = charAt(bp + (offset++));
                    if (chLocal >= CHAR6 && chLocal <= CHAR7) {
                        millis = millis * 10 + (chLocal - CHAR6);
                    } else {
                        break;
                    }
                }
            }

            if (millis < 0) {
                matchStat = NOT_MATCH;
                return null;
            }

            if (negative) {
                millis = -millis;
            }

            dateVal = new java.util.Date(millis);
        } else if (chLocal == CHAR14 &&
                charAt(bp + offset) == CHAR15 &&
                charAt(bp + offset + 1) == CHAR16 &&
                charAt(bp + offset + INT1) == CHAR16) {
            matchStat = VALUE_NULL;
            dateVal = null;
            offset += 3;
            chLocal = charAt(bp + offset++);
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            token = JSONToken.COMMA;
            return dateVal;
        }

        if (chLocal == CHAR17) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return dateVal;
    }

    public java.util.UUID scanFieldUUID(char[] fieldName) {
        matchStat = UNKNOWN;

        if (!charArrayCompare(fieldName)) {
            matchStat = NOT_MATCH_NAME;
            return null;
        }

        // int index = bp + fieldName.length;

        int offset = fieldName.length;
        char chLocal = charAt(bp + (offset++));

        final java.util.UUID uuid;
        if (chLocal == CHAR10) {
            int startIndex = bp + fieldName.length + 1;
            int endIndex = indexOf(CHAR10, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            // must re compute
            int startIndex2 = bp + fieldName.length + 1;
            int len = endIndex - startIndex2;
            if (len == 36) {
                long mostSigBits = 0, leastSigBits = 0;
                for (int i = 0; i < INT3; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 9; i < 13; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 14; i < 18; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 19; i < 23; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }
                for (int i = 24; i < 36; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }
                uuid = new UUID(mostSigBits, leastSigBits);

                offset += (endIndex - (bp + fieldName.length + 1) + 1);
                chLocal = charAt(bp + (offset++));
            } else if (len == 32) {
                long mostSigBits = 0, leastSigBits = 0;
                for (int i = 0; i < 16; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 16; i < 32; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }

                uuid = new UUID(mostSigBits, leastSigBits);

                offset += (endIndex - (bp + fieldName.length + 1) + 1);
                chLocal = charAt(bp + (offset++));
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
        } else if (chLocal == CHAR14
                && charAt(bp + (offset++)) == CHAR15
                && charAt(bp + (offset++)) == CHAR16
                && charAt(bp + (offset++)) == CHAR16) {
            uuid = null;
            chLocal = charAt(bp + (offset++));
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return uuid;
        }

        if (chLocal == CHAR12) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return uuid;
    }

    public java.util.UUID scanUuid(char seperator) {
        matchStat = UNKNOWN;

        // int index = bp + fieldName.length;

        int offset = 0;
        char chLocal = charAt(bp + (offset++));

        final java.util.UUID uuid;
        if (chLocal == CHAR10) {
            int startIndex = bp + 1;
            int endIndex = indexOf(CHAR10, startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }

            // must re compute
            int startIndex2 = bp + 1;
            int len = endIndex - startIndex2;
            if (len == 36) {
                long mostSigBits = 0, leastSigBits = 0;
                for (int i = 0; i < INT3; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 9; i < 13; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 14; i < 18; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 19; i < 23; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }
                for (int i = 24; i < 36; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }
                uuid = new UUID(mostSigBits, leastSigBits);

                offset += (endIndex - (bp + 1) + 1);
                chLocal = charAt(bp + (offset++));
            } else if (len == 32) {
                long mostSigBits = 0, leastSigBits = 0;
                for (int i = 0; i < 16; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    mostSigBits <<= INT;
                    mostSigBits |= num;
                }
                for (int i = 16; i < 32; ++i) {
                    char ch = charAt(startIndex2 + i);
                    int num;
                    if (ch >= CHAR6 && ch <= CHAR7) {
                        num = ch - CHAR6;
                    } else if (ch >= 'a' && ch <= 'f') {
                        num = 10 + (ch - 'a');
                    } else if (ch >= 'A' && ch <= 'F') {
                        num = 10 + (ch - 'A');
                    } else {
                        matchStat = NOT_MATCH_NAME;
                        return null;
                    }

                    leastSigBits <<= INT;
                    leastSigBits |= num;
                }

                uuid = new UUID(mostSigBits, leastSigBits);

                offset += (endIndex - (bp + 1) + 1);
                chLocal = charAt(bp + (offset++));
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
        } else if (chLocal == CHAR14
                && charAt(bp + (offset++)) == CHAR15
                && charAt(bp + (offset++)) == CHAR16
                && charAt(bp + (offset++)) == CHAR16) {
            uuid = null;
            chLocal = charAt(bp + (offset++));
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        if (chLocal == CHAR13) {
            bp += offset;
            this.ch = this.charAt(bp);
            matchStat = VALUE;
            return uuid;
        }

        if (chLocal == CHAR17) {
            chLocal = charAt(bp + (offset++));
            if (chLocal == CHAR13) {
                token = JSONToken.COMMA;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR17) {
                token = JSONToken.RBRACKET;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == CHAR12) {
                token = JSONToken.RBRACE;
                bp += offset;
                this.ch = this.charAt(bp);
            } else if (chLocal == EOI) {
                token = JSONToken.EOF;
                bp += (offset - 1);
                ch = EOI;
            } else {
                matchStat = NOT_MATCH;
                return null;
            }
            matchStat = END;
        } else {
            matchStat = NOT_MATCH;
            return null;
        }

        return uuid;
    }

    public final void scanTrue() {
        if (ch != 't') {
            throw new JSONException("error parse true");
        }
        next();

        if (ch != 'r') {
            throw new JSONException("error parse true");
        }
        next();

        if (ch != CHAR15) {
            throw new JSONException("error parse true");
        }
        next();

        if (ch != 'e') {
            throw new JSONException("error parse true");
        }
        next();

        if (ch == ' ' ||
                ch == CHAR13 ||
                ch == CHAR12 ||
                ch == CHAR17 ||
                ch == '\n' ||
                ch == '\r' ||
                ch == '\t' ||
                ch == EOI ||
                ch == '\f' ||
                ch == '\b' ||
                ch == ':' ||
                ch == CHAR8) {
            token = JSONToken.TRUE;
        } else {
            throw new JSONException("scan true error");
        }
    }

    public final void scanNullOrNew() {
        scanNullOrNew(true);
    }

    public final void scanNullOrNew(boolean acceptColon) {
        if (ch != CHAR14) {
            throw new JSONException("error parse null or new");
        }
        next();

        if (ch == CHAR15) {
            next();
            if (ch != CHAR16) {
                throw new JSONException("error parse null");
            }
            next();

            if (ch != CHAR16) {
                throw new JSONException("error parse null");
            }
            next();


            boolean res = ch == ' '
                    || ch == CHAR13
                    || ch == CHAR12
                    || ch == CHAR17
                    || ch == '\n'
                    || ch == '\r'
                    || ch == '\t'
                    || ch == EOI
                    || (ch == ':' && acceptColon)
                    || ch == '\f'
                    || ch == '\b';
            if (res) {
                token = JSONToken.NULL;
            } else {
                throw new JSONException("scan null error");
            }
            return;
        }

        if (ch != 'e') {
            throw new JSONException("error parse new");
        }
        next();

        if (ch != 'w') {
            throw new JSONException("error parse new");
        }
        next();

        if (ch == ' ' ||
                ch == CHAR13 ||
                ch == CHAR12 ||
                ch == CHAR17 ||
                ch == '\n' ||
                ch == '\r' ||
                ch == '\t' ||
                ch == EOI ||
                ch == '\f' ||
                ch == '\b') {
            token = JSONToken.NEW;
        } else {
            throw new JSONException("scan new error");
        }
    }

    public final void scanFalse() {
        if (ch != 'f') {
            throw new JSONException("error parse false");
        }
        next();

        if (ch != 'a') {
            throw new JSONException("error parse false");
        }
        next();

        if (ch != CHAR16) {
            throw new JSONException("error parse false");
        }
        next();

        if (ch != 's') {
            throw new JSONException("error parse false");
        }
        next();

        if (ch != 'e') {
            throw new JSONException("error parse false");
        }
        next();

        if (ch == ' ' ||
                ch == CHAR13 ||
                ch == CHAR12 ||
                ch == CHAR17 ||
                ch == '\n' ||
                ch == '\r' ||
                ch == '\t' ||
                ch == EOI ||
                ch == '\f' ||
                ch == '\b' ||
                ch == ':' ||
                ch == CHAR8) {
            token = JSONToken.FALSE;
        } else {
            throw new JSONException("scan false error");
        }
    }

    public final void scanIdent() {
        np = bp - 1;
        hasSpecial = false;

        for (; ; ) {
            sp++;

            next();
            if (Character.isLetterOrDigit(ch)) {
                continue;
            }

            String ident = stringVal();

            if ("null".equalsIgnoreCase(ident)) {
                token = JSONToken.NULL;
            } else if ("new".equals(ident)) {
                token = JSONToken.NEW;
            } else if ("true".equals(ident)) {
                token = JSONToken.TRUE;
            } else if ("false".equals(ident)) {
                token = JSONToken.FALSE;
            } else if ("undefined".equals(ident)) {
                token = JSONToken.UNDEFINED;
            } else if ("Set".equals(ident)) {
                token = JSONToken.SET;
            } else if ("TreeSet".equals(ident)) {
                token = JSONToken.TREE_SET;
            } else {
                token = JSONToken.IDENTIFIER;
            }
            return;
        }
    }

    @Override
    public abstract String stringVal();

    public abstract String subString(int offset, int count);

    protected abstract char[] subChars(int offset, int count);

    public static String readString(char[] chars, int charsLen) {
        char[] sbuf = new char[charsLen];
        int len = 0;
        for (int i = 0; i < charsLen; ++i) {
            char ch = chars[i];

            if (ch != '\\') {
                sbuf[len++] = ch;
                continue;
            }
            ch = chars[++i];

            switch (ch) {
                case CHAR6:
                    sbuf[len++] = '\0';
                    break;
                case '1':
                    sbuf[len++] = '\1';
                    break;
                case '2':
                    sbuf[len++] = '\2';
                    break;
                case '3':
                    sbuf[len++] = '\3';
                    break;
                case '4':
                    sbuf[len++] = '\4';
                    break;
                case '5':
                    sbuf[len++] = '\5';
                    break;
                case '6':
                    sbuf[len++] = '\6';
                    break;
                case '7':
                    sbuf[len++] = '\7';
                    break;
                case 'b':
                    // 8
                    sbuf[len++] = '\b';
                    break;
                case 't':
                    // 9
                    sbuf[len++] = '\t';
                    break;
                case CHAR14:
                    // 10
                    sbuf[len++] = '\n';
                    break;
                case 'v':
                    // 11
                    sbuf[len++] = '\u000B';
                    break;
                case 'f':
                case 'F':
                    // 12
                    sbuf[len++] = '\f';
                    break;
                case 'r':
                    // 13
                    sbuf[len++] = '\r';
                    break;
                case CHAR10:
                    // 34
                    sbuf[len++] = CHAR10;
                    break;
                case CHAR11:
                    // 39
                    sbuf[len++] = CHAR11;
                    break;
                case CHAR8:
                    // 47
                    sbuf[len++] = CHAR8;
                    break;
                case '\\':
                    // 92
                    sbuf[len++] = '\\';
                    break;
                case 'x':
                    sbuf[len++] = (char) (DIGITS[chars[++i]] * 16 + DIGITS[chars[++i]]);
                    break;
                case CHAR15:
                    sbuf[len++] = (char) Integer.parseInt(new String(new char[]{chars[++i],
                                    chars[++i],
                                    chars[++i],
                                    chars[++i]}),
                            16);
                    break;
                default:
                    throw new JSONException("unclosed.str.lit");
            }
        }
        return new String(sbuf, 0, len);
    }

    protected abstract boolean charArrayCompare(char[] chars);

    @Override
    public boolean isBlankInput() {
        for (int i = 0; ; ++i) {
            char chLocal = charAt(i);
            if (chLocal == EOI) {
                token = JSONToken.EOF;
                break;
            }

            if (!isWhitespace(chLocal)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public final void skipWhitespace() {
        for (; ; ) {
            if (ch <= CHAR8) {
                if (ch == ' ' ||
                        ch == '\r' ||
                        ch == '\n' ||
                        ch == '\t' ||
                        ch == '\f' ||
                        ch == '\b') {
                    next();
                    continue;
                } else if (ch == CHAR8) {
                    skipComment();
                    continue;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }

    private void scanStringSingleQuote() {
        np = bp;
        hasSpecial = false;
        char chLocal;
        for (; ; ) {
            chLocal = next();

            if (chLocal == CHAR11) {
                break;
            }

            if (chLocal == EOI) {
                if (!isEof()) {
                    putChar((char) EOI);
                    continue;
                }
                throw new JSONException("unclosed single-quote string");
            }

            if (chLocal == '\\') {
                if (!hasSpecial) {
                    hasSpecial = true;

                    if (sp > sbuf.length) {
                        char[] newsbuf = new char[sp * INT1];
                        System.arraycopy(sbuf, 0, newsbuf, 0, sbuf.length);
                        sbuf = newsbuf;
                    }

                    this.copyTo(np + 1, sp, sbuf);
                }

                chLocal = next();

                switch (chLocal) {
                    case CHAR6:
                        putChar('\0');
                        break;
                    case '1':
                        putChar('\1');
                        break;
                    case '2':
                        putChar('\2');
                        break;
                    case '3':
                        putChar('\3');
                        break;
                    case '4':
                        putChar('\4');
                        break;
                    case '5':
                        putChar('\5');
                        break;
                    case '6':
                        putChar('\6');
                        break;
                    case '7':
                        putChar('\7');
                        break;
                    case 'b':
                        // 8
                        putChar('\b');
                        break;
                    case 't':
                        // 9
                        putChar('\t');
                        break;
                    case CHAR14:
                        // 10
                        putChar('\n');
                        break;
                    case 'v':
                        // 11
                        putChar('\u000B');
                        break;
                    case 'f':
                    case 'F':
                        // 12
                        putChar('\f');
                        break;
                    case 'r':
                        // 13
                        putChar('\r');
                        break;
                    case CHAR10:
                        // 34
                        putChar(CHAR10);
                        break;
                    case CHAR11:
                        // 39
                        putChar(CHAR11);
                        break;
                    case CHAR8:
                        // 47
                        putChar(CHAR8);
                        break;
                    case '\\':
                        // 92
                        putChar('\\');
                        break;
                    case 'x':
                        char x1 = next();
                        char x2 = next();

                        boolean hex1 = (x1 >= CHAR6 && x1 <= CHAR7)
                                || (x1 >= 'a' && x1 <= 'f')
                                || (x1 >= 'A' && x1 <= 'F');
                        boolean hex2 = (x2 >= CHAR6 && x2 <= CHAR7)
                                || (x2 >= 'a' && x2 <= 'f')
                                || (x2 >= 'A' && x2 <= 'F');
                        if (!hex1 || !hex2) {
                            throw new JSONException("invalid escape character \\x" + x1 + x2);
                        }

                        putChar((char) (DIGITS[x1] * 16 + DIGITS[x2]));
                        break;
                    case CHAR15:
                        putChar((char) Integer.parseInt(new String(new char[]{next(), next(), next(), next()}), 16));
                        break;
                    default:
                        this.ch = chLocal;
                        throw new JSONException("unclosed single-quote string");
                }
                continue;
            }

            if (!hasSpecial) {
                sp++;
                continue;
            }

            if (sp == sbuf.length) {
                putChar(chLocal);
            } else {
                sbuf[sp++] = chLocal;
            }
        }

        token = LITERAL_STRING;
        this.next();
    }

    /**
     * Append a character to sbuf.
     */
    protected final void putChar(char ch) {
        if (sp == sbuf.length) {
            char[] newsbuf = new char[sbuf.length * INT1];
            System.arraycopy(sbuf, 0, newsbuf, 0, sbuf.length);
            sbuf = newsbuf;
        }
        sbuf[sp++] = ch;
    }

    public final void scanHex() {
        if (ch != 'x') {
            throw new JSONException("illegal state. " + ch);
        }
        next();
        if (ch != CHAR11) {
            throw new JSONException("illegal state. " + ch);
        }

        np = bp;
        next();

        if (ch == CHAR11) {
            next();
            token = JSONToken.HEX;
            return;
        }

        for (int i = 0; ; ++i) {
            char ch = next();
            boolean res = (ch >= CHAR6 && ch <= CHAR7) || (ch >= 'A' && ch <= 'F');
            if (res) {
                sp++;
                continue;
            } else if (ch == CHAR11) {
                sp++;
                next();
                break;
            } else {
                throw new JSONException("illegal state. " + ch);
            }
        }
        token = JSONToken.HEX;
    }

    @Override
    public final void scanNumber() {
        np = bp;

        if (ch == CHAR2) {
            sp++;
            next();
        }

        for (; ; ) {
            if (ch >= CHAR6 && ch <= CHAR7) {
                sp++;
            } else {
                break;
            }
            next();
        }

        boolean isDouble = false;

        if (ch == '.') {
            sp++;
            next();
            isDouble = true;

            for (; ; ) {
                if (ch >= CHAR6 && ch <= CHAR7) {
                    sp++;
                } else {
                    break;
                }
                next();
            }
        }

        if (ch == CHAR3) {
            sp++;
            next();
        } else if (ch == CHAR4) {
            sp++;
            next();
        } else if (ch == CHAR5) {
            sp++;
            next();
        } else if (ch == 'F') {
            sp++;
            next();
            isDouble = true;
        } else if (ch == 'D') {
            sp++;
            next();
            isDouble = true;
        } else if (ch == 'e' || ch == 'E') {
            sp++;
            next();

            if (ch == '+' || ch == CHAR2) {
                sp++;
                next();
            }

            for (; ; ) {
                if (ch >= CHAR6 && ch <= CHAR7) {
                    sp++;
                } else {
                    break;
                }
                next();
            }

            if (ch == 'D' || ch == 'F') {
                sp++;
                next();
            }

            isDouble = true;
        }

        if (isDouble) {
            token = JSONToken.LITERAL_FLOAT;
        } else {
            token = JSONToken.LITERAL_INT;
        }
    }

    @Override
    public final long longValue() throws NumberFormatException {
        long result = 0;
        boolean negative = false;
        long limit;
        int digit;

        if (np == -1) {
            np = 0;
        }

        int i = np, max = np + sp;

        if (charAt(np) == CHAR2) {
            negative = true;
            limit = Long.MIN_VALUE;
            i++;
        } else {
            limit = -Long.MAX_VALUE;
        }
        long multmin = MULTMIN_RADIX_TEN;
        if (i < max) {
            digit = charAt(i++) - CHAR6;
            result = -digit;
        }
        while (i < max) {
            // Accumulating negatively avoids surprises near MAX_VALUE
            char chLocal = charAt(i++);

            if (chLocal == CHAR3 || chLocal == CHAR4 || chLocal == CHAR5) {
                break;
            }

            digit = chLocal - CHAR6;
            if (result < multmin) {
                throw new NumberFormatException(numberString());
            }
            result *= 10;
            if (result < limit + digit) {
                throw new NumberFormatException(numberString());
            }
            result -= digit;
        }

        if (negative) {
            if (i > np + 1) {
                return result;
            } else { /* Only got "-" */
                throw new NumberFormatException(numberString());
            }
        } else {
            return -result;
        }
    }

    @Override
    public final Number decimalValue(boolean decimal) {
        char chLocal = charAt(np + sp - 1);
        try {
            if (chLocal == 'F') {
                return Float.parseFloat(numberString());
            }

            if (chLocal == 'D') {
                return Double.parseDouble(numberString());
            }

            if (decimal) {
                return decimalValue();
            } else {
                return doubleValue();
            }
        } catch (NumberFormatException ex) {
            throw new JSONException(ex.getMessage() + ", " + info());
        }
    }

    @Override
    public abstract BigDecimal decimalValue();

    public static boolean isWhitespace(char ch) {
        // 专门调整了判断顺序
        return ch <= ' ' &&
                (ch == ' ' ||
                        ch == '\n' ||
                        ch == '\r' ||
                        ch == '\t' ||
                        ch == '\f' ||
                        ch == '\b');
    }

    protected static final long MULTMIN_RADIX_TEN = Long.MIN_VALUE / 10;
    protected static final int INT_MULTMIN_RADIX_TEN = Integer.MIN_VALUE / 10;

    protected final static int[] DIGITS = new int[(int) 'f' + 1];

    static {
        for (int i = CHAR6; i <= CHAR7; ++i) {
            DIGITS[i] = i - CHAR6;
        }

        for (int i = 'a'; i <= 'f'; ++i) {
            DIGITS[i] = (i - 'a') + 10;
        }
        for (int i = 'A'; i <= 'F'; ++i) {
            DIGITS[i] = (i - 'A') + 10;
        }
    }

    /**
     * hsf support
     *
     * @param fieldName
     * @param argTypesCount
     * @param typeSymbolTable
     * @return
     */
    public String[] scanFieldStringArray(char[] fieldName, int argTypesCount, SymbolTable typeSymbolTable) {
        throw new UnsupportedOperationException();
    }

    public boolean matchField2(char[] fieldName) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getFeatures() {
        return this.features;
    }
}
