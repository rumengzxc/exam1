package com.alibaba.fastjson.support.geo;

import com.alibaba.fastjson.annotation.JSONType;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author xwm
 * @since 1.2.68
 */
@JSONType(typeName = "Feature", orders = {"type", "id", "bbox", "coordinates", "properties"})
public class Feature
        extends Abstract_Geometry {
    private String id;
    private Abstract_Geometry abstractGeometry;
    private Map<String, String> properties = new LinkedHashMap<String, String>();

    public Feature() {
        super("Feature");
    }

    public Abstract_Geometry getGeometry() {
        return abstractGeometry;
    }

    public void setGeometry(Abstract_Geometry abstractGeometry) {
        this.abstractGeometry = abstractGeometry;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
