package com.alibaba.fastjson.support.geo;

import com.alibaba.fastjson.annotation.JSONType;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 1.2.68
 * @author xwm
 */
@JSONType(typeName = "GeometryCollection", orders = {"type", "bbox", "geometries"})
public class AbstractGeometryCollection extends Abstract_Geometry {
    private List<Abstract_Geometry> geometries = new ArrayList<Abstract_Geometry>();

    public AbstractGeometryCollection() {
        super("GeometryCollection");
    }

    public List<Abstract_Geometry> getGeometries() {
        return geometries;
    }
}
