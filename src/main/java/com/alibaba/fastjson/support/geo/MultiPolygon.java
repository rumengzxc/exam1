package com.alibaba.fastjson.support.geo;

import com.alibaba.fastjson.annotation.JSONType;

/**
 * @author xwm
 * @since 1.2.68
 */
@JSONType(typeName = "MultiPolygon", orders = {"type", "bbox", "coordinates"})
public class MultiPolygon
        extends Abstract_Geometry {
    private double[][][][] coordinates;

    public MultiPolygon() {
        super("MultiPolygon");
    }

    public double[][][][] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[][][][] coordinates) {
        this.coordinates = coordinates;
    }
}
