package com.alibaba.fastjson.support.hsf;

import java.lang.reflect.Method;

/**
 * @author xwm
 */
public interface MethodLocator {
    Method findMethod(String[] types);
}
