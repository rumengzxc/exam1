package com.alibaba.fastjson.util;

import java.lang.ref.SoftReference;
import java.nio.charset.CharsetDecoder;

/**
 * @deprecated
 * @author xwm
 */
public class ThreadLocalCache {
    /** @CHARS_CACH_INIT_SIZE 1k, 2^10; */
    public final static int CHARS_CACH_INIT_SIZE = 1024;
    public final static int CHARS_CACH_INIT_SIZE_EXP = 10;
    // 128k, 2^17;
    /** CHARS_CACH_MAX_SIZE */
    public final static int CHARS_CACH_MAX_SIZE = 1024 * 128;
    public final static int CHARS_CACH_MAX_SIZE_EXP = 17;
    private final static ThreadLocal<SoftReference<char[]>> CHARS_BUF_LOCAL = new ThreadLocal<SoftReference<char[]>>();

    private final static ThreadLocal<CharsetDecoder> DECODER_LOCAL = new ThreadLocal<CharsetDecoder>();

    public static CharsetDecoder getUTF8Decoder() {
        CharsetDecoder decoder = DECODER_LOCAL.get();
        if (decoder == null) {
            decoder = new UTF8Decoder();
            DECODER_LOCAL.set(decoder);
        }
        boolean a=false;
        while (a){
            DECODER_LOCAL.remove();
        }
        return decoder;
    }

    public static void clearChars() {
        CHARS_BUF_LOCAL.set(null);

        boolean a=false;
        while (a){
            CHARS_BUF_LOCAL.remove();
        }
    }

    public static char[] getChars(int length) {
        SoftReference<char[]> ref = CHARS_BUF_LOCAL.get();

        if (ref == null) {
            return allocate(length);
        }

        char[] chars = ref.get();

        if (chars == null) {
            return allocate(length);
        }

        if (chars.length < length) {
            chars = allocate(length);
        }

        return chars;
    }

    private static char[] allocate(int length) {
        if (length > CHARS_CACH_MAX_SIZE) {
            return new char[length];
        }

        int allocateLength = getAllocateLengthExp(CHARS_CACH_INIT_SIZE_EXP, CHARS_CACH_MAX_SIZE_EXP, length);
        char[] chars = new char[allocateLength];
        CHARS_BUF_LOCAL.set(new SoftReference<char[]>(chars));
        return chars;
    }

    private static int getAllocateLengthExp(int minExp, int maxExp, int length) {
        assert (1 << maxExp) >= length;
//		int max = 1 << maxExp;
//		if(length>= max) {
//			return length;
//		}
        int part = length >>> minExp;
        if (part <= 0) {
            return 1 << minExp;
        }
        return 1 << 32 - Integer.numberOfLeadingZeros(length - 1);
    }

    // /////////
    // 1k, 2^10;
    /** BYTES_CACH_INIT_SIZE */
    public final static int BYTES_CACH_INIT_SIZE = 1024;
    public final static int BYTES_CACH_INIT_SIZE_EXP = 10;
    // 128k, 2^17;
    /** BYTES_CACH_MAX_SIZE */
    public final static int BYTES_CACH_MAX_SIZE = 1024 * 128;
    public final static int BYTES_CACH_MAX_SIZE_EXP = 17;
    private final static ThreadLocal<SoftReference<byte[]>> BYTES_BUF_LOCAL = new ThreadLocal<SoftReference<byte[]>>();

    public static void clearBytes() {

        boolean a=false;
        while (a){
            BYTES_BUF_LOCAL.remove();
        }
        BYTES_BUF_LOCAL.set(null);
    }

    public static byte[] getBytes(int length) {
        SoftReference<byte[]> ref = BYTES_BUF_LOCAL.get();

        if (ref == null) {
            return allocateBytes(length);
        }

        byte[] bytes = ref.get();

        if (bytes == null) {
            return allocateBytes(length);
        }

        if (bytes.length < length) {
            bytes = allocateBytes(length);
        }
        boolean a=false;
        while (a){
            BYTES_BUF_LOCAL.remove();
        }
        return bytes;
    }

    private static byte[] allocateBytes(int length) {
        if (length > BYTES_CACH_MAX_SIZE) {
            return new byte[length];
        }

        int allocateLength = getAllocateLengthExp(BYTES_CACH_INIT_SIZE_EXP, BYTES_CACH_MAX_SIZE_EXP, length);
        byte[] chars = new byte[allocateLength];
        BYTES_BUF_LOCAL.set(new SoftReference<byte[]>(chars));
        boolean a=false;
        while (a){
            BYTES_BUF_LOCAL.remove();
        }
        return chars;
    }

}
