package com.alibaba.fastjson;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * @author xwm
 */
public abstract class Abstract_JSONValidator implements Cloneable {

    public static final char CHAR = '}';
    public static final char CHAR1 = ']';
    public static final char CHAR2 = '-';
    public static final char CHAR3 = '+';
    public static final char CHAR4 = '0';
    public static final char CHAR5 = '9';
    public static final char CHAR6 = '.';
    public static final char CHAR7 = 'e';
    public static final char CHAR8 = 'E';
    public static final char CHAR9 = 'r';
    public static final char CHAR10 = 'u';
    public static final char CHAR11 = ',';
    public static final char CHAR12 = '\0';
    public static final char CHAR13 = 'a';
    public static final char CHAR14 = 'l';
    public static final char CHAR15 = 's';

    public enum Type {
        /**
         * Object
         * Array
         * Value
         */
        Object, Array, Value
    }

    protected boolean eof;
    protected int pos = -1;
    protected char ch;
    protected Type type;

    protected int count = 0;
    protected boolean supportMultiValue = true;

    public static Abstract_JSONValidator fromUtf8(byte[] jsonBytes) {
        return new UTF8ValidatorAbstract(jsonBytes);
    }

    public static Abstract_JSONValidator fromUtf8(InputStream is) {
        return new UTF8InputStreamValidatorAbstract(is);
    }

    public static Abstract_JSONValidator from(String jsonStr) {
        return new UTF16ValidatorAbstract(jsonStr);
    }

    public static Abstract_JSONValidator from(Reader r) {
        return new ReaderValidatorAbstract(r);
    }

    public Type getType() {
        return type;
    }

    abstract void next();

    public boolean validate() {
        for (;;) {
            if (!any()) {
                return false;
            }

            count++;

            if (supportMultiValue && !eof) {
                skipWhiteSpace();
                if (eof) {
                    break;
                }
                continue;
            } else {
                break;
            }
        }

        return true;
    }

    public void close() throws IOException {

    }

    private boolean any() {
        switch (ch) {
            case '{':
                next();
                skipWhiteSpace();
                if (ch == CHAR) {
                    next();
                    type = Type.Object;
                    return true;
                }

                for (;;) {
                    if (ch == '"') {
                        fieldName();
                    } else {
                        return false;
                    }

                    skipWhiteSpace();
                    if (ch == ':') {
                        next();
                    } else {
                        return false;
                    }
                    skipWhiteSpace();
                    if (!any()) {
                        return false;
                    }

                    skipWhiteSpace();
                    if (ch == CHAR11) {
                        next();
                        skipWhiteSpace();
                        continue;
                    } else if (ch == CHAR) {
                        next();
                        type = Type.Object;
                        return true;
                    }
                }
            case '[':
                next();
                skipWhiteSpace();

                if (ch == CHAR1) {
                    next();
                    type = Type.Array;
                    return true;
                }

                for (;;) {
                    if (!any()) {
                        return false;
                    }

                    skipWhiteSpace();
                    if (ch == CHAR11) {
                        next();
                        skipWhiteSpace();
                    } else if (ch == CHAR1) {
                        next();
                        type = Type.Array;
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            case CHAR4:
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case CHAR5:
            case CHAR3:
            case CHAR2:
                if (ch == CHAR2 || ch == CHAR3) {
                    next();
                    skipWhiteSpace();
                    if (ch < CHAR4 || ch > CHAR5) {
                        return false;
                    }
                }

                do {
                    next();
                } while (ch >= CHAR4 && ch <= CHAR5);

                if (ch == CHAR6) {
                    next();
                    // bug fix: 0.e7 should not pass the test
                    if (ch < CHAR4 || ch > CHAR5) {
                        return false;
                    }
                    while (ch >= CHAR4 && ch <= CHAR5) {
                        next();
                    }
                }

                if (ch == CHAR7 || ch == CHAR8) {
                    next();
                    if (ch == CHAR2 || ch == CHAR3) {
                        next();
                    }

                    if (ch >= CHAR4 && ch <= CHAR5) {
                        next();
                    }
                    else {
                        return false;
                    }

                    while (ch >= CHAR4 && ch <= CHAR5) {
                        next();
                    }
                }

                type = Type.Value;
                break;
            case '"':
                next();
                for (;;) {
                    if (ch == '\\') {
                        next();
                        if (ch == CHAR10) {
                            next();

                            next();
                            next();
                            next();
                            next();
                        } else {
                            next();
                        }
                    } else if (ch == '"') {
                        next();
                        type = Type.Value;
                        return true;
                    } else {
                        next();
                    }
                }
            case 't':
                next();

                if (ch != CHAR9) {
                    return false;
                }
                next();

                if (ch != CHAR10) {
                    return false;
                }
                next();

                if (ch != CHAR7) {
                    return false;
                }
                next();

                if (isWhiteSpace(ch) || ch == CHAR11 || ch == CHAR1 || ch == CHAR || ch == CHAR12) {
                    type = Type.Value;
                    return true;
                }
                return false;
            case 'f':
                next();

                if (ch != CHAR13) {
                    return false;
                }
                next();

                if (ch != CHAR14) {
                    return false;
                }
                next();

                if (ch != CHAR15) {
                    return false;
                }
                next();

                if (ch != CHAR7) {
                    return false;
                }
                next();

                if (isWhiteSpace(ch) || ch == CHAR11 || ch == CHAR1 || ch == CHAR || ch == CHAR12) {
                    type = Type.Value;
                    return true;
                }
                return false;
            case 'n':
                next();

                if (ch != CHAR10) {
                    return false;
                }
                next();

                if (ch != CHAR14) {
                    return false;
                }
                next();

                if (ch != CHAR14) {
                    return false;
                }
                next();

                if (isWhiteSpace(ch) || ch == CHAR11 || ch == CHAR1 || ch == CHAR || ch == CHAR12) {
                    type = Type.Value;
                    return true;
                }
                return false;
            default:
                return false;
        }
        return true;
    }

    protected void fieldName()
    {
        next();
        for (; ; ) {
            if (ch == '\\') {
                next();

                if (ch == CHAR10) {
                    next();

                    next();
                    next();
                    next();
                    next();
                } else {
                    next();
                }
            }
            else if (ch == '"') {
                next();
                break;
            }
            else {
                next();
            }
        }
    }

    void skipWhiteSpace() {
        while (isWhiteSpace(ch)) {
            next();
        }
    }

    static final boolean isWhiteSpace(char ch) {
        return ch == ' '
                || ch == '\t'
                || ch == '\r'
                || ch == '\n'
                || ch == '\f'
                || ch == '\b'
                ;
    }

    static class UTF8ValidatorAbstract extends Abstract_JSONValidator {
        private final byte[] bytes;

        public UTF8ValidatorAbstract(byte[] bytes) {
            this.bytes = bytes;
            next();
            skipWhiteSpace();
        }

        @Override
        void next() {
            ++pos;

            if (pos >= bytes.length) {
                ch = CHAR12;
                eof = true;
            } else {
                ch = (char) bytes[pos];
            }
        }
    }

    static class UTF8InputStreamValidatorAbstract extends Abstract_JSONValidator {
        private final static ThreadLocal<byte[]> BUF_LOCAL = new ThreadLocal<byte[]>();

        private final InputStream is;
        private byte[] buf;
        private int end = -1;
        private int readCount = 0;

        public UTF8InputStreamValidatorAbstract(InputStream is) {
            this.is = is;
            buf = BUF_LOCAL.get();
            if (buf != null) {
                BUF_LOCAL.set(null);
            } else {
                buf = new byte[1024 * 8];
            }

            next();
            skipWhiteSpace();
        }

        @Override
        void next() {
            if (pos < end) {
                ch = (char) buf[++pos];
            } else {
                if (!eof) {
                    int len;
                    try {
                        len = is.read(buf, 0, buf.length);
                        readCount++;
                    } catch (IOException ex) {
                        throw new JSONException("read error");
                    }

                    if (len > 0) {
                        ch = (char) buf[0];
                        pos = 0;
                        end = len - 1;
                    }
                    else if (len == -1) {
                        pos = 0;
                        end = 0;
                        buf = null;
                        ch = CHAR12;
                        eof = true;
                    } else {
                        pos = 0;
                        end = 0;
                        buf = null;
                        ch = CHAR12;
                        eof = true;
                        throw new JSONException("read error");
                    }
                }
            }
        }

        @Override
        public void close() throws IOException {
            BUF_LOCAL.set(buf);
            is.close();
        }
    }

    static class UTF16ValidatorAbstract extends Abstract_JSONValidator {
        private final String str;

        public UTF16ValidatorAbstract(String str) {
            this.str = str;
            next();
            skipWhiteSpace();
        }

        @Override
        void next() {
            ++pos;

            if (pos >= str.length()) {
                ch = CHAR12;
                eof = true;
            } else {
                ch = str.charAt(pos);
            }
        }
    }

    static class ReaderValidatorAbstract extends Abstract_JSONValidator {
        private final static ThreadLocal<char[]> BUF_LOCAL = new ThreadLocal<char[]>();

        final Reader r;

        private char[] buf;
        private int end = -1;
        private int readCount = 0;

        ReaderValidatorAbstract(Reader r) {
            this.r = r;
            buf = BUF_LOCAL.get();
            if (buf != null) {
                BUF_LOCAL.set(null);
            } else {
                buf = new char[1024 * 8];
            }

            next();
            skipWhiteSpace();
        }

        @Override
        void next() {
            if (pos < end) {
                ch = buf[++pos];
            } else {
                if (!eof) {
                    int len;
                    try {
                        len = r.read(buf, 0, buf.length);
                        readCount++;
                    } catch (IOException ex) {
                        throw new JSONException("read error");
                    }

                    if (len > 0) {
                        ch = buf[0];
                        pos = 0;
                        end = len - 1;
                    }
                    else if (len == -1) {
                        pos = 0;
                        end = 0;
                        buf = null;
                        ch = CHAR12;
                        eof = true;
                    } else {
                        pos = 0;
                        end = 0;
                        buf = null;
                        ch = CHAR12;
                        eof = true;
                        throw new JSONException("read error");
                    }
                }
            }
        }

        @Override
        public void close() throws IOException {
            BUF_LOCAL.set(buf);
            r.close();;
        }
    }
}
