package com.alibaba.json.bvt.geo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.geo.Abstract_Geometry;
import com.alibaba.fastjson.support.geo.Point;
import junit.framework.TestCase;

public class PointTest
        extends TestCase {
    public void test_geo() throws Exception {
        String str = "{\n" +
                "    \"type\": \"Point\",\n" +
                "    \"coordinates\": [100.0, 0.0]\n" +
                "}";

        Abstract_Geometry abstractGeometry = JSON.parseObject(str, Abstract_Geometry.class);
        assertEquals(Point.class, abstractGeometry.getClass());

        assertEquals("{\"type\":\"Point\",\"coordinates\":[100.0,0.0]}", JSON.toJSONString(abstractGeometry));

        String str2 = JSON.toJSONString(abstractGeometry);
        assertEquals(str2, JSON.toJSONString(JSON.parseObject(str2, Abstract_Geometry.class)));
    }
}
