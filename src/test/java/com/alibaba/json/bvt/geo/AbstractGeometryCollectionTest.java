package com.alibaba.json.bvt.geo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.geo.Abstract_Geometry;
import com.alibaba.fastjson.support.geo.AbstractGeometryCollection;
import junit.framework.TestCase;

public class AbstractGeometryCollectionTest
        extends TestCase {
    public void test_geo() throws Exception {
        String str = "{\n" +
                "    \"type\": \"GeometryCollection\",\n" +
                "    \"geometries\": [{\n" +
                "        \"type\": \"Point\",\n" +
                "        \"coordinates\": [100.0, 0.0]\n" +
                "    }, {\n" +
                "    \"type\": \"LineString\",\n" +
                "    \"coordinates\": [\n" +
                "        [101.0, 0.0],\n" +
                "        [102.0, 1.0]\n" +
                "    ]\n" +
                "    }]\n" +
                "}";

        Abstract_Geometry abstractGeometry = JSON.parseObject(str, Abstract_Geometry.class);
        assertEquals(AbstractGeometryCollection.class, abstractGeometry.getClass());

        assertEquals(
                "{\"type\":\"GeometryCollection\",\"geometries\":[{\"type\":\"Point\",\"coordinates\":[100.0,0.0]},{\"type\":\"LineString\",\"coordinates\":[[101.0,0.0],[102.0,1.0]]}]}"
                , JSON.toJSONString(abstractGeometry));

        String str2 = JSON.toJSONString(abstractGeometry);
        assertEquals(str2, JSON.toJSONString(JSON.parseObject(str2, Abstract_Geometry.class)));
    }
}
