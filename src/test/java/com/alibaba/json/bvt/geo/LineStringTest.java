package com.alibaba.json.bvt.geo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.geo.Abstract_Geometry;
import com.alibaba.fastjson.support.geo.LineString;
import junit.framework.TestCase;

public class LineStringTest extends TestCase {
    public void test_geo() throws Exception {
        String str = "{\n" +
                "    \"type\": \"LineString\",\n" +
                "    \"coordinates\": [\n" +
                "        [100.0, 0.0],\n" +
                "        [101.0, 1.0]\n" +
                "    ]\n" +
                "}";

        Abstract_Geometry abstractGeometry = JSON.parseObject(str, Abstract_Geometry.class);
        assertEquals(LineString.class, abstractGeometry.getClass());

        assertEquals("{\"type\":\"LineString\",\"coordinates\":[[100.0,0.0],[101.0,1.0]]}", JSON.toJSONString(abstractGeometry));

        String str2 = JSON.toJSONString(abstractGeometry);
        assertEquals(str2, JSON.toJSONString(JSON.parseObject(str2, Abstract_Geometry.class)));
    }
}
