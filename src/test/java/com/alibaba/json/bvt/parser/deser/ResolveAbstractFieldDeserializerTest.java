package com.alibaba.json.bvt.parser.deser;

import com.alibaba.fastjson.parser.deserializer.ResolveAbstractFieldDeserializer;

import junit.framework.TestCase;


public class ResolveAbstractFieldDeserializerTest extends TestCase {
    public void test_0 () throws Exception {
        new ResolveAbstractFieldDeserializer(null, null).parseField(null, null, null, null);
        new ResolveAbstractFieldDeserializer(null, null, 0).parseField(null, null, null, null);
    }
}
