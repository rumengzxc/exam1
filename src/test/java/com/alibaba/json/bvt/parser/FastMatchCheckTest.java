package com.alibaba.json.bvt.parser;

import org.junit.Assert;

import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.NumberDeserializer;
import com.alibaba.fastjson.parser.deserializer.SqlDateDeserializerAbstract;
import com.alibaba.fastjson.serializer.AtomicCodec;
import com.alibaba.fastjson.serializer.CharacterCodec;
import com.alibaba.fastjson.serializer.MiscCodec;
import com.alibaba.fastjson.serializer.ObjectArrayCodec;

import junit.framework.TestCase;

public class FastMatchCheckTest extends TestCase {
    public void test_match() throws Exception {
        Assert.assertEquals(JSONToken.LBRACKET, AtomicCodec.INSTANCE.getFastMatchToken());
        Assert.assertEquals(JSONToken.LITERAL_STRING, MiscCodec.INSTANCE.getFastMatchToken());
        Assert.assertEquals(JSONToken.LITERAL_INT, NumberDeserializer.INSTANCE.getFastMatchToken());
        Assert.assertEquals(JSONToken.LITERAL_INT, SqlDateDeserializerAbstract.INSTANCE.getFastMatchToken());
        Assert.assertEquals(JSONToken.LBRACKET, ObjectArrayCodec.INSTANCE.getFastMatchToken());
        Assert.assertEquals(JSONToken.LITERAL_STRING, CharacterCodec.INSTANCE.getFastMatchToken());
    }
}
